<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SCWD WordPress Theme
 */

?>
				<?php scwd_hook_main_bottom(); ?>

			</div><!-- #main-content -->

			<?php scwd_hook_main_after(); ?>

			<?php scwd_hook_wrap_bottom(); ?>

		</div><!-- #wrap -->

		<?php scwd_hook_wrap_after(); ?>

	</div><!-- #outer-wrap -->

	<?php scwd_outer_wrap_after(); ?>

<?php wp_footer(); ?>

</body>
</html>
