<?php
/**
 * The template for editing templatera templates via the front-end editor.
 *
 * @package SCWD WordPress Theme
 * @subpackage Templates
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// This file is only used for the front-end editor.
if ( ! scwd_vc_is_inline() ) {
	exit;
}

get_header(); ?>

	<div id="content-wrap" class="container clr">

		<?php scwd_hook_primary_before(); ?>

		<div id="primary" class="content-area clr">

			<?php scwd_hook_content_before(); ?>

			<div id="content" class="site-content clr">

				<?php scwd_hook_content_top(); ?>

				<div class="single-page-content entry clr">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; ?>

				</div>

				<?php scwd_hook_content_bottom(); ?>

			</div><!-- #content -->

			<?php scwd_hook_content_after(); ?>

		</div><!-- #primary -->

		<?php scwd_hook_primary_after(); ?>

	</div><!-- .container -->

<?php get_footer(); ?>