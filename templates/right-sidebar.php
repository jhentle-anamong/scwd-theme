<?php
/**
 * Template Name: Right Sidebar
 *
 * @package SCWD WordPress Theme
 * @subpackage Templates
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header(); ?>

	<div id="content-wrap" class="container clr">

		<?php scwd_hook_primary_before(); ?>

		<div id="primary" class="content-area clr">

			<?php scwd_hook_content_before(); ?>

			<div id="content" class="site-content clr">

				<?php scwd_hook_content_top(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php scwd_get_template_part( 'page_single_blocks' ); ?>

				<?php endwhile; ?>

				<?php scwd_hook_content_bottom(); ?>

			</div><!-- #content -->

			<?php scwd_hook_content_after(); ?>

		</div><!-- #primary -->

		<?php scwd_hook_primary_after(); ?>

	</div><!-- .container -->

<?php get_footer(); ?>