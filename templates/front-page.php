<?php
/**
 * Template Name: Front Page
 *
 * @package SCWD WordPress Theme
 * @subpackage Templates
 */

// Get site header
get_header(); ?>

    <div id="content-wrap" class="container clr">

        <?php scwd_hook_primary_before(); ?>

        <div id="primary" class="content-area clr">

            <?php scwd_hook_content_before(); ?>

            <div id="content" class="clr site-content" role="main">

                <?php scwd_hook_content_top(); ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <article class="entry-content entry clr">

                        <?php the_content(); ?>

                    </article><!-- #post -->

                 <?php endwhile; ?>

                <?php get_template_part( 'partials/post-edit' ); ?>

                <?php scwd_hook_content_bottom(); ?>

            </div><!-- #content -->

            <?php scwd_hook_content_after(); ?>

        </div><!-- #primary -->

        <?php scwd_hook_primary_after(); ?>

    </div><!-- #content-wrap -->

<?php
// Get site footer
get_footer(); ?>