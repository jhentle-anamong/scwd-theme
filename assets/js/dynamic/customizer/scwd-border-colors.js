/**
 * Customizer border color live preview
 *
 * @version 1.0
 */

;( function( api, $, window, document, undefined ) {

	"use strict"

	if ( ! wp || ! wp.customize ) {
		console.log( 'wp or wp.customize objects not found.' );
		return;
	}

	if ( typeof scwdBorderColorElements === 'undefined' ) {
		console.log( 'no border color elements defined.' );
		return;
	}

	wp.customize( 'main_border_color', function( value ) {

			value.bind( function( newval ) {

				var style = '';

				if ( newval ) {

					var style = '<style id="scwd-borders-css" type="text/css">';

					style += scwdBorderColorElements.join( ',' ) + '{border-color:' + newval + ';}';

					style += '</style>';

					if ( $( '#scwd-borders-css' ).length !== 0 ) {
						$( '#scwd-borders-css' ).replaceWith( style );
					} else {
						$( style ).appendTo( $( 'head' ) );
					}

				} else if ( $( '#scwd-borders-css' ).length !== 0 ) {
					$( '#scwd-borders-css' ).remove();
				}

			} );

		} );

} ( wp.customize, jQuery, window, document ) );