<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package SCWD WordPress Theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
?>

	<div id="content-wrap" class="container clr">

		<?php scwd_hook_primary_before(); ?>

		<div id="primary" class="content-area clr">

			<?php scwd_hook_content_before(); ?>

			<main id="content" class="clr site-content" role="main">

				<?php scwd_hook_content_top(); ?>

				<section class="error-404 not-found">
					<header class="page-header">
						<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'scwd' ); ?></h1>
					</header><!-- .page-header -->

					<div class="page-content">
						<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'scwd' ); ?></p>

						<?php
						get_search_form();

						the_widget( 'WP_Widget_Recent_Posts' );
						?>

						<div class="widget widget_categories">
							<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'scwd' ); ?></h2>
							<ul>
								<?php
								wp_list_categories( array(
									'orderby'    => 'count',
									'order'      => 'DESC',
									'show_count' => 1,
									'title_li'   => '',
									'number'     => 10,
								) );
								?>
							</ul>
						</div><!-- .widget -->

						<?php
						/* translators: %1$s: smiley */
						$scwd_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'scwd' ), convert_smilies( ':)' ) ) . '</p>';
						the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$scwd_archive_content" );

						the_widget( 'WP_Widget_Tag_Cloud' );
						?>

					</div><!-- .page-content -->

				</section><!-- .error-404 -->

				<?php scwd_hook_content_bottom(); ?>

			</main><!-- #content -->

			<?php scwd_hook_content_after(); ?>

		</div><!-- #primary -->

		<?php scwd_hook_primary_after(); ?>

	</div><!-- #content-wrap -->

<?php get_footer(); ?>
