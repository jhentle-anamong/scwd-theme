<?php
/**
 * Site header search dropdown HTML
 *
 * @package Total WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div id="searchform-dropdown" class="header-searchform-wrap clr" data-placeholder="<?php echo esc_attr( scwd_get_header_menu_search_form_placeholder() ); ?>" data-disable-autocomplete="true">
	<?php echo scwd_get_header_menu_search_form(); ?>
</div>