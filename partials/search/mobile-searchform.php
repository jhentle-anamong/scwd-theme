<?php
/**
 * Searchform for the mobile sidebar menu
 *
 * @package Total WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$placeholder = apply_filters( 'scwd_mobile_searchform_placeholder', esc_html__( 'Search', 'scwd' ), 'mobile' );
$action      = apply_filters( 'scwd_search_action', esc_url( home_url( '/' ) ), 'mobile' ); ?>

<div id="mobile-menu-search" class="clr scwd-hidden">
	<form method="get" action="<?php echo esc_attr( $action ); ?>" class="mobile-menu-searchform">
		<input type="search" name="s" autocomplete="off" aria-label="<?php echo esc_attr_x( 'Search', 'aria-label', 'scwd' ); ?>" placeholder="<?php echo esc_attr( $placeholder ); ?>" />
		<?php if ( defined( 'ICL_LANGUAGE_CODE' ) ) { ?>
			<input type="hidden" name="lang" value="<?php echo( ICL_LANGUAGE_CODE ); ?>"/>
		<?php } ?>
		<?php if ( SCWD_WOOCOMMERCE_ACTIVE && scwd_get_mod( 'woo_header_product_searchform', false ) ) { ?>
			<input type="hidden" name="post_type" value="product" />
		<?php } ?>
		<button type="submit" class="searchform-submit" aria-label="<?php echo esc_attr_x( 'Submit search', 'aria-label', 'scwd' ); ?>"><span class="ticon ticon-search"></span></button>
	</form>
</div>