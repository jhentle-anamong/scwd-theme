<?php
/**
 * Skip To Content
 *
 * @package Total WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get correct content ID
$id = esc_attr( scwd_get_mod( 'skip_to_content_id' ) );
$id = $id ? $id : 'content'; ?>

<a href="#<?php echo str_replace( '#', '', $id ); ?>" class="skip-to-content"<?php scwd_aria_landmark( 'skip_to_content' ); ?>><?php echo esc_html__( 'skip to Main Content', 'scwd' ); ?></a>