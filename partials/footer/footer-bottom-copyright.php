<?php
/**
 * Footer bottom content
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get copyright info
$copyright = scwd_get_mod( 'footer_copyright_text', 'Copyright <a href="#">Your Business LLC.</a> [current_year] - All Rights Reserved' );

// Translate the theme option
$copyright = scwd_translate_theme_mod( 'footer_copyright_text', $copyright );

// Return if there isn't any copyright content to display
if ( ! $copyright ) {
	return;
} ?>

<div id="copyright" class="clr"<?php scwd_aria_landmark( 'copyright' ); ?>>
	<?php echo do_shortcode( wp_kses_post( $copyright ) ); ?>
</div><!-- #copyright -->