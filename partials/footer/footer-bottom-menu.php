<?php
/**
 * Footer bottom content
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get footer menu location and apply filters for child theming
$menu_location = apply_filters( 'scwd_footer_menu_location', 'footer_menu' );

// Menu is required
if ( ! has_nav_menu( $menu_location ) ) {
	return;
} ?>

<div id="footer-bottom-menu" class="clr"<?php scwd_aria_landmark( 'footer_bottom_menu' ); ?> aria-label="<?php echo scwd_get_mod( 'footer_menu_aria_label', esc_attr_x( 'Footer menu', 'aria-label', 'scwd' ) ); ?>"><?php

	// Display footer menu
	wp_nav_menu( array(
		'theme_location' => $menu_location,
		'sort_column'    => 'menu_order',
		'fallback_cb'    => false,
	) );

?></div><!-- #footer-bottom-menu -->