<?php
/**
 * Footer bottom content
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Classes
$classes = 'clr';
if ( $align = scwd_get_mod( 'bottom_footer_text_align' ) ) {
	$classes .= ' text' . $align;
} ?>

<?php scwd_hook_footer_bottom_before(); ?>
<div id="footer-bottom" class="<?php echo esc_attr( $classes ); ?>"<?php scwd_schema_markup( 'footer_bottom' ); ?>>
	<?php scwd_hook_footer_bottom_top(); ?>
	<div id="footer-bottom-inner" class="container clr">
		<?php scwd_hook_footer_bottom_inner(); ?>
	</div><!-- #footer-bottom-inner -->
	<?php scwd_hook_footer_bottom_bottom(); ?>
</div><!-- #footer-bottom -->
<?php scwd_hook_footer_bottom_after(); ?>