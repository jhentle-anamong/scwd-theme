<?php
/**
 * Footer Layout
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} ?>

<?php scwd_hook_footer_before(); ?>

<?php if ( scwd_footer_has_widgets() ) : ?>

    <footer id="footer" class="site-footer"<?php scwd_schema_markup( 'footer' ); ?>>

        <?php scwd_hook_footer_top(); ?>

        <div id="footer-inner" class="site-footer-inner container clr">

            <?php scwd_hook_footer_inner(); // widgets are added via this hook ?>

        </div><!-- #footer-widgets -->

        <?php scwd_hook_footer_bottom(); ?>

    </footer><!-- #footer -->

<?php endif; ?>

<?php scwd_hook_footer_after(); ?>