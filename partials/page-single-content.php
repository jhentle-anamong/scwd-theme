<?php
/**
 * Page Content
 *
 * @package Total WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div class="single-content single-page-content entry clr"><?php the_content(); ?></div>

<?php
// Page links (for the <!-nextpage-> tag)
scwd_get_template_part( 'link_pages' ); ?>