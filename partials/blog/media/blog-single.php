<?php
/**
 * Blog single post standard format media
 *
 * @package Total WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Return if there isn't a thumbnail
if ( ! has_post_thumbnail() ) {
	return;
} ?>

<div id="post-media" class="clr">

	<?php
	// Image with lightbox link
	if ( scwd_get_mod( 'blog_post_image_lightbox' ) ) :

		scwd_enqueue_lightbox_scripts(); ?>

		<a href="<?php echo scwd_get_lightbox_image(); ?>" title="<?php esc_attr_e( 'Enlarge Image', 'scwd' ); ?>" class="scwd-lightbox<?php scwd_entry_image_animation_classes(); ?>"><?php echo scwd_get_blog_post_thumbnail(); ?></a>

	<?php
	// No lightbox
	else : ?>

		<?php echo scwd_get_blog_post_thumbnail(); ?>

	<?php endif; ?>

	<?php
	// Blog entry caption
	if ( scwd_get_mod( 'blog_thumbnail_caption' ) && $caption = scwd_featured_image_caption() ) : ?>

		<div class="post-media-caption clr"><?php echo wp_kses_post( $caption ); ?></div>

	<?php endif; ?>

</div><!-- #post-media -->