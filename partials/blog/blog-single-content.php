<?php
/**
 * Single blog post content
 *
 * @package SCWD WordPress theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div class="single-blog-content entry clr"<?php scwd_schema_markup( 'entry_content' ); ?>><?php the_content(); ?></div>

<?php
// Page links (for the <!-nextpage-> tag)
scwd_get_template_part( 'link_pages' ); ?>