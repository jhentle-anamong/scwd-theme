<?php
/**
 * Header Logo Inner
 *
 * This file displays the image logo or standard text logo
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Define variables
$logo_url   = scwd_header_logo_url();
$logo_img   = scwd_header_logo_img();
$logo_icon  = scwd_header_logo_icon();
$logo_title = scwd_header_logo_title();

// Overlay Header logo (make sure overlay header is enabled first)
$overlay_logo = scwd_has_overlay_header() ? scwd_overlay_header_logo_img() : '';

// Define output
$output = '';

// Display image logo
if ( $logo_img || $overlay_logo ) {

	// Logo img attributes
	$img_attrs = apply_filters( 'scwd_header_logo_img_attrs', array(
		'src'            => esc_url( $logo_img ),
		'alt'            => esc_attr( $logo_title ),
		'class'          => 'logo-img',
		'data-no-retina' => 'data-no-retina',
		'width'          => intval( scwd_header_logo_img_width() ),
		'height'         => intval( scwd_header_logo_img_height() ),
	) );

	// Custom site-wide image logo
	if ( $logo_img && ! $overlay_logo ) {

		$output .= '<a href="' . esc_url( $logo_url ) . '" rel="home" class="main-logo">';

			$output .= '<img ' . scwd_parse_attrs( $img_attrs ) . ' />';

		$output .= '</a>';

	}

	// Custom header-overlay logo => Must be added on it's own HTML. IMPORTANT!
	if ( $overlay_logo ) {

		$img_attrs['src'] = esc_url( $overlay_logo );

		$output .= '<a href="' . esc_url( $logo_url ) . '" rel="home" class="overlay-header-logo">';

			$output .= '<img ' . scwd_parse_attrs( $img_attrs ) . ' />';

		$output .= '</a>';

	}

}

// Display text logo
else {

	$output .= '<a href="' . esc_url( $logo_url ) . '" rel="home" class="site-logo-text">';

		$output .= $logo_icon . esc_html( $logo_title );

	$output .= '</a>';

}

// Echo logo output
echo apply_filters( 'scwd_header_logo_output', $output );