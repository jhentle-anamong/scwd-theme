<?php
/**
 * Main Header Layout Output
 * Have a look at app/hooks/actions to see what is hooked into the header
 * See all header parts at partials/header/
 *
 * @package SCWD WordPress Theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<?php scwd_hook_header_before(); ?>

<header id="site-header" class="<?php echo scwd_header_classes(); ?>"<?php scwd_schema_markup( 'header' ); ?><?php scwd_aria_landmark( 'header' ); ?>>

	<?php scwd_hook_header_top(); ?>

	<div id="site-header-inner" class="container clr">

		<?php scwd_hook_header_inner(); ?>

	</div><!-- #site-header -->

	<?php scwd_hook_header_bottom(); ?>

</header><!-- #masthead -->

<?php scwd_hook_header_after(); ?>