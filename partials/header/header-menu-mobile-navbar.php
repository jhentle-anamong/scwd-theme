<?php
/**
 * Navbar Style Mobile Menu Toggle
 *
 * Note: By default this file only loads if wpex_header_has_mobile_menu returns true.
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Get menu text
$text = scwd_get_translated_theme_mod( 'mobile_menu_toggle_text' );
$text = $text ? $text : esc_html__( 'Menu', 'scwd' );
$text = apply_filters( 'scwd_mobile_menu_navbar_open_text', $text ); ?>

<div id="scwd-mobile-menu-navbar" class="<?php echo scwd_header_mobile_menu_classes(); ?>">
	<div class="container clr">
		<a href="#mobile-menu" class="mobile-menu-toggle">
			<?php echo apply_filters( 'scwd_mobile_menu_navbar_open_icon', '<span class="ticon ticon-navicon" aria-hidden="true"></span>' ); ?><span class="scwd-text"><?php echo wp_kses_post( $text ); ?></span>
		</a>
	</div>
</div>