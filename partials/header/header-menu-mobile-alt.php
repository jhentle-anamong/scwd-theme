<?php
/**
 * Mobile Menu alternative.
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */ ?>

<div id="mobile-menu-alternative" class="wpex-hidden"<?php scwd_aria_landmark( 'mobile_menu_alt' ); ?> aria-label="<?php echo scwd_get_mod( 'mobile_menu_aria_label', esc_attr_x( 'Mobile menu', 'aria-label', 'scwd' ) ); ?>"><?php
		wp_nav_menu( array(
			'theme_location' => 'mobile_menu_alt',
			'menu_class'     => 'dropdown-menu',
			'fallback_cb'    => false,
		) );
?></div>