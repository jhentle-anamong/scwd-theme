<?php
/**
 * Mobile Icons Header Menu.
 *
 * Note: By default this file only loads if wpex_header_has_mobile_menu returns true.
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div id="scwd-mobile-menu-fixed-top" class="<?php echo scwd_header_mobile_menu_classes(); ?>">
	<div class="container clr">
		<div class="scwd-inner">
			<a href="#mobile-menu" class="mobile-menu-toggle"><?php echo apply_filters( 'scwd_mobile_menu_open_button_text', '<span class="ticon ticon-navicon"></span>' ); ?><span class="scwd-text"><?php echo scwd_get_mod( 'mobile_menu_toggle_text', esc_html__( 'Menu', 'scwd' ) ); ?></span></a>
			<?php /* if ( $extra_icons = wpex_get_mobile_menu_extra_icons() ) { ?>
				<div class="wpex-aside"><?php echo $extra_icons; ?></div>
			<?php } */ ?>
		</div>
	</div>
</div>