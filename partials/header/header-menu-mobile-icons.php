<?php
/**
 * Mobile Icons Header Menu
 *
 * Note: By default this file only loads if wpex_header_has_mobile_menu returns true.
 *
 * @package SCWD WordPress Theme
 * @subpackage Partials
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<div id="mobile-menu" class="<?php echo scwd_header_mobile_menu_classes(); ?>"><?php

	// Output user-defined mobile icons
	echo scwd_get_mobile_menu_extra_icons();

	// Output main toggle icon
	echo scwd_get_mobile_menu_toggle_icon();

?></div>