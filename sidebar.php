<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SCWD WordPress Theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<?php scwd_hook_sidebar_before(); ?>

<aside id="sidebar" class="sidebar-container sidebar-primary"<?php scwd_schema_markup( 'sidebar' ); ?><?php scwd_aria_landmark( 'sidebar' ); ?>>

	<div id="sidebar-inner" class="clr">

		<?php scwd_hook_sidebar_inner(); // Sidebar is added via framework/hooks/partials/ @see scwd_display_sidebar() ?>

	</div><!-- #sidebar-inner -->

	<?php scwd_hook_sidebar_bottom(); ?>

</aside><!-- #sidebar -->

<?php scwd_hook_sidebar_after(); ?>