<?php
/**
 * Silver Connect Web Design functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package SCWD WordPress Theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Start up class
final class SCWD_Theme_Setup {
	/**
	 * SCWD_Theme_Setup Constructor.
	 *
	 * Loads all necessary classes, functions, hooks, configuration files and actions for the theme.
	 * Everything starts here.
	 *
	 */
	public function __construct()
	{
		// Define constants
		$this->constants();

		// Store theme mods in $scwd_theme_mods global variable and register theme mod functions
		require_once SCWD_APP_DIR . 'theme-mods.php';

		// Include main Admin Panel
		require_once SCWD_APP_DIR . 'AdminPanel.php';

		// Load all core theme function files
		// Load Before classes and addons so we can make use of them => IMPORTANT!!!
		$this->main_includes();

		// Load framework classes
		// Runs after config files since there are filters used in the config files that may target these classes
		add_action( 'after_setup_theme', array( $this, 'classes' ), 4 );

		// Setup theme => add_theme_support, register_nav_menus, load_theme_textdomain, etc.
		// Must run on 10 priority or else child theme ` will be overritten
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ), 10 );

		// Defines hooks and adds theme actions
		// Moved to after_setup_theme hook in v3.6.0 so it can be accessed earlier if needed
		// to remove actions
		add_action( 'after_setup_theme', array( $this, 'hooks_actions' ), 10 );
	}

	/**
	 * Defines the constants for use within the theme.
	 */
	public function constants() {

		define( 'SCWD_THEME_ACTIVE', true );
		define( 'SCWD_THEME_VERSION', '1.0.0' );

		define( 'SCWD_THEME_DIR', get_template_directory() );
		define( 'SCWD_THEME_URI', get_template_directory_uri() );

		define( 'SCWD_THEME_BRANDING', get_theme_mod( 'theme_branding', 'Silver Connect' ) );

		define( 'SCWD_APP_DIR', SCWD_THEME_DIR . '/app/' );
		define( 'SCWD_APP_DIR_URI', SCWD_THEME_URI . '/app/' );

		define( 'SCWD_ClASSES_DIR', SCWD_APP_DIR . 'classes/' );

		define( 'SCWD_THEME_STYLE_HANDLE', 'scwd-style' );
		define( 'SCWD_THEME_JS_HANDLE', 'scwd-core' );

		define( 'SCWD_WOOCOMMERCE_ACTIVE', class_exists( 'WooCommerce' ) );
		define( 'SCWD_WPML_ACTIVE', class_exists( 'SitePress' ) );
	}

	/**
	 * Defines all theme hooks and runs all needed actions for theme hooks.
	 */
	public function hooks_actions()
	{
		// Register hooks (needed in admin for Custom Actions panel)
		require_once SCWD_APP_DIR . 'hooks/hooks.php';

		// Core theme hooks and actions // if running in backend it breaks VC grid builder
		if ( ! is_admin() ) {
			require_once SCWD_APP_DIR . 'hooks/actions.php';
			require_once SCWD_APP_DIR . 'hooks/partials.php';
		}
	}

	/**
	 * Theme functions (Must load before Classes & Addons so we can make use of them)
	 */
	public function main_includes()
	{

		require_once SCWD_APP_DIR . 'sanitize.php';
		require_once SCWD_APP_DIR . 'conditionals.php';
		require_once SCWD_APP_DIR . 'core-functions.php';

		require_once SCWD_APP_DIR . 'template-parts.php';
		require_once SCWD_APP_DIR . 'arrays.php';

		require_once SCWD_APP_DIR . 'helpers/js-localize-data.php';
		require_once SCWD_APP_DIR . 'helpers/aria-landmarks.php';
		require_once SCWD_APP_DIR . 'helpers/translations.php';
		require_once SCWD_APP_DIR . 'helpers/fonts.php';
		require_once SCWD_APP_DIR . 'helpers/schema-markup.php';
		require_once SCWD_APP_DIR . 'helpers/overlays.php';
		require_once SCWD_APP_DIR . 'helpers/header.php';
		require_once SCWD_APP_DIR . 'helpers/header-menu.php';
		require_once SCWD_APP_DIR . 'helpers/title.php';
		require_once SCWD_APP_DIR . 'helpers/post-slider.php';
		require_once SCWD_APP_DIR . 'helpers/page-header.php';
		require_once SCWD_APP_DIR . 'helpers/footer.php';
		require_once SCWD_APP_DIR . 'helpers/blog.php';
		require_once SCWD_APP_DIR . 'helpers/miscellaneous.php';

		require_once SCWD_APP_DIR . 'wp-actions/widgets-init.php';
		require_once SCWD_APP_DIR . 'wp-actions/enqueue-scripts.php';
		require_once SCWD_APP_DIR . 'wp-actions/meta-viewport.php';

		require_once SCWD_APP_DIR . 'wp-filters/body-class.php';

		if ( scwd_get_mod( 'remove_emoji_scripts_enable', true ) ) {
			require_once SCWD_APP_DIR . 'wp-actions/remove-emoji-scripts.php';
		}

	}

	/**
	 * Framework Classes.
	 */
	public function classes()
	{
		// Takes an input and outputs sanitized data
		require_once SCWD_ClASSES_DIR . 'SanitizeData.php';

		// Outputs inline CSS to the front-end of the site based on Customizer settings
		require_once SCWD_ClASSES_DIR . 'InlineCSS.php';

		// Outputs CSS to the live site for advanced Customizer settings
		require_once SCWD_ClASSES_DIR . 'AdvancedStyles.php';

		/*** IMPORTANT: Customizer Class must load last ***/
		require_once SCWD_APP_DIR . 'customizer/customizer.php';

		require_once SCWD_APP_DIR . 'vendor/ACF.php';

		// Admin only classes
		if ( is_admin() ) {

			// Recommends plugins to install and/or update
			if ( scwd_recommended_plugins() && scwd_get_mod( 'recommend_plugins_enable', true ) ) {
				require_once SCWD_APP_DIR . 'vendor/TGMPA.php';
			}

		}
	}

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	public function theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Silver Connect Web Design, use a find and replace
		 * to change 'scwd' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'scwd', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// Register theme navigation menus
		register_nav_menus( array(
			'topbar_menu'     => esc_html__( 'Top Bar', 'scwd' ),
			'main_menu'       => esc_html__( 'Main/Header', 'scwd' ),
			'mobile_menu_alt' => esc_html__( 'Mobile Menu Alternative', 'scwd' ),
			'mobile_menu'     => esc_html__( 'Mobile Icons', 'scwd' ),
			'footer_menu'     => esc_html__( 'Footer', 'scwd' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'scwd_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
}

new SCWD_Theme_Setup;

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function scwd_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'scwd_content_width', 640 );
}
add_action( 'after_setup_theme', 'scwd_content_width', 0 );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';