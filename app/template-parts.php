<?php
/**
 * Array of theme template parts and helper function to return correct template part
 *
 * @package Silver_Connect_Web
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Get Template Part
 *
 * @since 3.5.0
 */
function scwd_template_parts() {
	return apply_filters( 'scwd_template_parts', array(

		// Header
		'header'                       => 'partials/header/header-layout',
		'header_logo'                  => 'partials/header/header-logo',
		'header_logo_inner'            => 'partials/header/header-logo-inner',
		'header_menu'                  => 'partials/header/header-menu',
		'header_aside'                 => 'partials/header/header-aside',
		'header_buttons'               => 'partials/header/header-buttons',
		'header_search_dropdown'       => 'partials/search/header-search-dropdown',
		'header_search_replace'        => 'partials/search/header-search-replace',
		'header_search_overlay'        => 'partials/search/header-search-overlay',
		'header_mobile_menu_fixed_top' => 'partials/header/header-menu-mobile-fixed-top',
		'header_mobile_menu_navbar'    => 'partials/header/header-menu-mobile-navbar',
		'header_mobile_menu_icons'     => 'partials/header/header-menu-mobile-icons',
		'header_mobile_menu_alt'       => 'partials/header/header-menu-mobile-alt',

		// Page header
		'page_header'            => 'partials/page-header',
		'page_header_title'      => 'partials/page-header-title',
		'page_header_subheading' => 'partials/page-header-subheading',

		// Single blocks
		'cpt_single_blocks'          => 'partials/cpt/cpt-single',
		'page_single_blocks'         => 'partials/page-single-layout',
		'blog_single_blocks'         => 'partials/blog/blog-single-layout',

		// Blog
		// 'blog_entry'          => 'partials/blog/blog-entry-layout',
		// 'blog_single_quote'   => 'partials/blog/blog-single-quote',
		// 'blog_single_media'   => 'partials/blog/media/blog-single',
		'blog_single_title'   => 'partials/blog/blog-single-title',
		// 'blog_single_meta'    => 'partials/blog/blog-single-meta',
		'blog_single_content' => 'partials/blog/blog-single-content',
		// 'blog_single_tags'    => 'partials/blog/blog-single-tags',
		// 'blog_single_related' => 'partials/blog/blog-single-related',

		// Footer
		'footer'              => 'partials/footer/footer-layout',
		'footer_widgets'      => 'partials/footer/footer-widgets',
		'footer_bottom'       => 'partials/footer/footer-bottom',

		// Footer Bottom
		'footer_bottom_copyright' => 'partials/footer/footer-bottom-copyright',
		'footer_bottom_menu'      => 'partials/footer/footer-bottom-menu',

		// Mobile
		'mobile_searchform'  => 'partials/search/mobile-searchform',

		// Other
		'scroll_top'  => 'partials/scroll-top',
		'link_pages'   => 'partials/link-pages',
		'post_slider'  => 'partials/post-slider',
	) );
}

/**
 * Get Template Part
 */
function scwd_get_template_part( $slug, $name = null ) {
	if ( $slug ) {
		$parts = scwd_template_parts();
		if ( isset( $parts[$slug] ) ) {
			$output = $parts[$slug];
			if ( isset( $parts[$slug] ) ) {
				$output = $parts[$slug];
				if ( is_callable( $output ) ) {
					return call_user_func( $output );
				} else {
					get_template_part( $parts[$slug], $name );
				}
			}
		}
	}
}

/**
 * Returns correct post content template
 *
 * @since 4.3
 */
function scwd_get_singular_template_id( $type = '', $singular = true ) {
	$type = $type ? $type : get_post_type();
	$post_id = is_admin() ? get_the_ID() : scwd_get_current_post_id();
	if ( $meta = get_post_meta( $post_id, 'scwd_singular_template', true ) ) {
		$template = $meta;
	} else {
		$template = scwd_get_mod( $type . '_singular_template', null );
	}
	$template = apply_filters( 'scwd_get_singular_template_id', $template, $type );
	return $template ? scwd_parse_obj_id( $template, 'page' ) : null;
}

/**
 * Returns correct post content template
 *
 * @since 4.3
 */
function scwd_get_singular_template_content( $type = '' ) {
	$template = scwd_get_singular_template_id( $type );
	if ( ! $template ) {
		return;
	}
	$temp_post = get_post( $template );
	return $temp_post ? $temp_post->post_content : null;
}