<?php
/**
 * Useful functions that return arrays
 *
 * @package SCWD WordPress Theme
 * @subpackage Framework
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Site Layouts
 *
 * @since 4.9
 */
function scwd_get_site_layouts() {
	return apply_filters( 'scwd_get_site_layouts', array(
		''           => esc_html__( 'Default', 'scwd' ),
		'full-width' => esc_html__( 'Full-Width', 'scwd' ),
		'boxed'      => esc_html__( 'Boxed', 'scwd' ),
	) );
}

/**
 * Accent Colors
 *
 * @since 4.4.1
 */
function scwd_get_accent_colors() {
	return apply_filters( 'scwd_get_accent_colors', array(
		'default'  => array(
			'label' => esc_html__( 'Default', 'scwd' ),
			'hex'   => '', // Consider using wpex_get_custom_accent_color() for future updates?
		),
		'black'  => array(
			'label' => esc_html__( 'Black', 'scwd' ),
			'hex'   => '#333',
		),
		'blue'   => array(
			'label' => esc_html__( 'Blue', 'scwd' ),
			'hex'   => '#4a97c2',
		),
		'brown'  => array(
			'label' => esc_html__( 'Brown', 'scwd' ),
			'hex'   => '#804b35',
		),
		'grey'   => array(
			'label' => esc_html__( 'Grey', 'scwd' ),
			'hex'   => '#bbb',
		),
		'green'  => array(
			'label' => esc_html__( 'Green', 'scwd' ),
			'hex'   => '#87bf17',
		),
		'gold'   => array(
			'label' => esc_html__( 'Gold', 'scwd' ),
			'hex'   => '#ddba00',
		),
		'orange' => array(
			'label' => esc_html__( 'Orange', 'scwd' ),
			'hex'   => '#ee7836',
		),
		'pink'   => array(
			'label' => esc_html__( 'Pink', 'scwd' ),
			'hex'   => '#f261c2',
		),
		'purple' => array(
			'label' => esc_html__( 'Purple', 'scwd' ),
			'hex'   => '#9a5e9a',
		),
		'red'    => array(
			'label' => esc_html__( 'Red', 'scwd' ),
			'hex'   => '#f73936',
		),
		'rosy'   => array(
			'label' => esc_html__( 'Rosy', 'scwd' ),
			'hex'   => '#ea2487',
		),
		'teal'   => array(
			'label' => esc_html__( 'Teal', 'scwd' ),
			'hex'   => '#00b3b3',
		),
		'white'  => array(
			'label' => esc_html__( 'White', 'scwd' ),
			'hex'   => '#fff',
		),
	) );
}

/**
 * Returns array of custom widgets
 *
 * @since 3.6.0
 * @deprecated 4.9
 */
function scwd_custom_widgets_list() {
	return array();
}

/**
 * Returns array of header styles
 *
 * @since 4.3
 */
function scwd_get_header_styles() {
	return apply_filters( 'scwd_header_styles', array(
		'one'   => '1. ' . esc_html__( 'Left Logo & Right Menu','scwd' ),
		'two'   => '2. ' . esc_html__( 'Bottom Menu','scwd' ),
		'three' => '3. ' . esc_html__( 'Bottom Menu Centered','scwd' ),
		'four'  => '4. ' . esc_html__( 'Top Centered Menu','scwd' ),
		'five'  => '5. ' . esc_html__( 'Centered Inline Logo','scwd' ),
		'six'   => '6. ' . esc_html__( 'Vertical','scwd' ),
		//'vertical-2' => esc_html__( 'Seven - Vertical Minimal','scwd' ), //@todo Finish
	) );
}

/**
 * Returns array of image background styles
 *
 * @since 3.5.0
 */
function scwd_get_bg_img_styles() {
	return array(
		''             => esc_html__( 'Default', 'scwd' ),
		'cover'        => esc_html__( 'Cover', 'scwd' ),
		'stretched'    => esc_html__( 'Stretched', 'scwd' ),
		'repeat'       => esc_html__( 'Repeat', 'scwd' ),
		'fixed-top'    => esc_html__( 'Fixed Top', 'scwd' ),
		'fixed'        => esc_html__( 'Fixed Center', 'scwd' ),
		'fixed-bottom' => esc_html__( 'Fixed Bottom', 'scwd' ),
		'repeat-x'     => esc_html__( 'Repeat-x', 'scwd' ),
		'repeat-y'     => esc_html__( 'Repeat-y', 'scwd' ),
		'inherit'      => esc_html__( 'Inherit', 'scwd' ),
	);
}

/**
 * Returns array of dropdown styles
 *
 * @since 3.4.0
 */
function scwd_get_menu_dropdown_styles() {
	return apply_filters( 'scwd_get_header_menu_dropdown_styles', array(
		'default'    => esc_html__( 'Default', 'scwd' ),
		'minimal-sq' => esc_html__( 'Minimal', 'scwd' ),
		'minimal'    => esc_html__( 'Minimal - Rounded', 'scwd' ),
		'black'      => esc_html__( 'Black', 'scwd' ),
	) );
}

/**
 * Returns array of form styles
 *
 * @since 3.6.0
 */
function scwd_get_form_styles() {
	return apply_filters( 'scwd_get_form_styles', array(
		''        => esc_html__( 'Default', 'scwd' ),
		'min'     => esc_html__( 'Minimal', 'scwd' ),
		'modern'  => esc_html__( 'Modern', 'scwd' ),
		'white'   => esc_html__( 'White', 'scwd' ),
		'black'   => esc_html__( 'Black', 'scwd' ),
		'white-o' => esc_html__( 'White Outline', 'scwd' ),
		'black-o' => esc_html__( 'Black Outline', 'scwd' ),
	) );
}

/**
 * Array of carousel arrow positions
 *
 * @since 3.5.3
 */
function scwd_carousel_arrow_positions() {
	return apply_filters( 'scwd_carousel_arrow_positions', array(
		'default' => esc_html__( 'Default', 'scwd' ),
		'left'    => esc_html__( 'Left', 'scwd' ) ,
	 	'center'  => esc_html__( 'Center', 'scwd' ),
		'right'   => esc_html__( 'Right', 'scwd' ),
		'abs'     => esc_html__( 'Absolute', 'scwd' ),
	) );
}

/**
 * Array of carousel arrow styles
 *
 * @since 3.5.3
 */
function scwd_carousel_arrow_styles() {
	return apply_filters( 'scwd_carousel_arrow_styles', array(
		''       => esc_html__( 'Default', 'scwd' ),
		'slim'   => esc_html__( 'Slim', 'scwd' ),
		'min'    => esc_html__( 'Minimal', 'scwd' ),
		'border' => esc_html__( 'Border', 'scwd' ),
		'circle' => esc_html__( 'Circle', 'scwd' ),
	) );
}

/**
 * Returns array of page layouts
 *
 * @since 3.3.3
 */
function scwd_get_post_layouts() {
	return apply_filters( 'scwd_get_post_layouts', array(
		''              => esc_html__( 'Default', 'scwd' ),
		'right-sidebar' => esc_html__( 'Right Sidebar', 'scwd' ),
		'left-sidebar'  => esc_html__( 'Left Sidebar', 'scwd' ),
		'full-width'    => esc_html__( 'No Sidebar', 'scwd' ),
		'full-screen'   => esc_html__( 'Full Screen', 'scwd' ),
	) );
}

/**
 * Returns array of Header Overlay Styles
 *
 * @since 3.3.0
 */
function scwd_header_overlay_styles() {
	return apply_filters( 'scwd_header_overlay_styles', array(
		'white' => esc_html__( 'White Text', 'scwd' ),
		'light' => esc_html__( 'Light Text', 'scwd' ),
		'dark'  => esc_html__( 'Black Text', 'scwd' ),
		'core'  => esc_html__( 'Default Styles', 'scwd' ),
	) );
}


/**
 * Returns array of Header Overlay Styles
 *
 * @since 4.5.5.1
 */
function scwd_get_mobile_menu_styles() {
	return apply_filters( 'scwd_get_mobile_menu_styles', array(
		'sidr' => esc_html__( 'Sidebar', 'scwd' ),
		'toggle' => esc_html__( 'Toggle', 'scwd' ),
		'full_screen' => esc_html__( 'Full Screen Overlay', 'scwd' ),
		'disabled' => esc_html__( 'Disabled', 'scwd' ),
	) );
}

/**
 * Returns array of available post types
 *
 * @since 3.3.0
 */
function scwd_get_post_types( $instance = '', $exclude = array() ) {
	$types = array();
	$get_types = get_post_types( array(
		'public'   => true,
	), 'objects', 'and' );
	foreach ( $get_types as $key => $val ) {
		if ( ! in_array( $key, $exclude ) ) {
			$types[$key] = $val->labels->name;
		}
	}
	return apply_filters( 'scwd_get_post_types', $types, $instance );
}

/**
 * User social options
 *
 * @since 4.0
 */
function scwd_get_user_social_profile_settings_array() {
	return apply_filters( 'scwd_get_user_social_profile_settings_array', array(
		'twitter'    => array(
			'label' => 'Twitter',
			'icon'  => 'ticon ticon-twitter',
		),
		'facebook'   => array(
			'label' => 'Facebook',
			'icon'  => 'ticon ticon-facebook',
		),
		'linkedin'   => array(
			'label' => 'LinkedIn',
			'icon'  => 'ticon ticon-linkedin',
		),
		'pinterest'  => array(
			'label' => 'Pinterest',
			'icon'  => 'ticon ticon-pinterest',
		),
		'instagram'  => array(
			'label' => 'Instagram',
			'icon'  => 'ticon ticon-instagram',
		),
	) );
}

/**
 * Global List Social Link Options
 *
 * @since 4.3
 */
function scwd_social_profile_options_list() {
	return apply_filters ( 'scwd_social_profile_options_list', array(
		'twitter' => array(
			'label' => 'Twitter',
			'icon_class' => 'ticon ticon-twitter',
		),
		'facebook' => array(
			'label' => 'Facebook',
			'icon_class' => 'ticon ticon-facebook',
		),
		'pinterest'  => array(
			'label' => 'Pinterest',
			'icon_class' => 'ticon ticon-pinterest',
		),
		'dribbble' => array(
			'label' => 'Dribbble',
			'icon_class' => 'ticon ticon-dribbble',
		),
		'etsy'  => array(
			'label' => 'Etsy',
			'icon_class' => 'ticon ticon-etsy',
		),
		'vk' => array(
			'label' => 'VK',
			'icon_class' => 'ticon ticon-vk',
		),
		'instagram'  => array(
			'label' => 'Instagram',
			'icon_class' => 'ticon ticon-instagram',
		),
		'linkedin' => array(
			'label' => 'LinkedIn',
			'icon_class' => 'ticon ticon-linkedin',
		),
		'flickr' => array(
			'label' => 'Flickr',
			'icon_class' => 'ticon ticon-flickr',
		),
		'quora' => array(
			'label' => 'Quora',
			'icon_class' => 'ticon ticon-quora',
		),
		'skype' => array(
			'label' => 'Skype',
			'icon_class' => 'ticon ticon-skype',
		),
		'whatsapp' => array(
			'label' => 'Whatsapp',
			'icon_class' => 'ticon ticon-whatsapp',
		),
		'youtube' => array(
			'label' => 'Youtube',
			'icon_class' => 'ticon ticon-youtube',
		),
		'vimeo' => array(
			'label' => 'Vimeo',
			'icon_class' => 'ticon ticon-vimeo',
		),
		'vine' => array(
			'label' => 'Vine',
			'icon_class' => 'ticon ticon-vine',
		),
		'spotify' => array(
			'label' => 'Spotify',
			'icon_class' => 'ticon ticon-spotify',
		),
		'xing' => array(
			'label' => 'Xing',
			'icon_class' => 'ticon ticon-xing',
		),
		'yelp' => array(
			'label' => 'Yelp',
			'icon_class' => 'ticon ticon-yelp',
		),
		'tripadvisor' => array(
			'label' => 'Tripadvisor',
			'icon_class' => 'ticon ticon-tripadvisor',
		),
		'houzz' => array(
			'label' => 'Houzz',
			'icon_class' => 'ticon ticon-houzz',
		),
		'twitch' => array(
			'label' => 'Twitch',
			'icon_class' => 'ticon ticon-twitch',
		),
		'tumblr' => array(
			'label' => 'Tumblr',
			'icon_class' => 'ticon ticon-tumblr',
		),
		'github' => array(
			'label' => 'Github',
			'icon_class' => 'ticon ticon-github',
		),
		'rss'  => array(
			'label' => esc_html__( 'RSS', 'scwd' ),
			'icon_class' => 'ticon ticon-rss',
		),
		'email' => array(
			'label' => esc_html__( 'Email', 'scwd' ),
			'icon_class' => 'ticon ticon-envelope',
		),
		'phone' => array(
			'label' => esc_html__( 'Phone', 'scwd' ),
			'icon_class' => 'ticon ticon-phone',
		),
	) );
}

/**
 * Returns array of Social Options for the Top Bar
 *
 * Added here because it's needed in backend and front-end
 *
 * @since 1.6.0
 */
function scwd_topbar_social_options() {
	return apply_filters ( 'scwd_topbar_social_options', scwd_social_profile_options_list() );
}

/**
 * Returns array of WP dashicons
 *
 * @since 3.3.0
 */
function scwd_get_dashicons_array() {
	return array('admin-appearance' => 'f100', 'admin-collapse' => 'f148', 'admin-comments' => 'f117', 'admin-generic' => 'f111', 'admin-home' => 'f102', 'admin-media' => 'f104', 'admin-network' => 'f112', 'admin-page' => 'f133', 'admin-plugins' => 'f106', 'admin-settings' => 'f108', 'admin-site' => 'f319', 'admin-tools' => 'f107', 'admin-users' => 'f110', 'align-center' => 'f134', 'align-left' => 'f135', 'align-none' => 'f138', 'align-right' => 'f136', 'analytics' => 'f183', 'arrow-down' => 'f140', 'arrow-down-alt' => 'f346', 'arrow-down-alt2' => 'f347', 'arrow-left' => 'f141', 'arrow-left-alt' => 'f340', 'arrow-left-alt2' => 'f341', 'arrow-right' => 'f139', 'arrow-right-alt' => 'f344', 'arrow-right-alt2' => 'f345', 'arrow-up' => 'f142', 'arrow-up-alt' => 'f342', 'arrow-up-alt2' => 'f343', 'art' => 'f309', 'awards' => 'f313', 'backup' => 'f321', 'book' => 'f330', 'book-alt' => 'f331', 'businessman' => 'f338', 'calendar' => 'f145', 'camera' => 'f306', 'cart' => 'f174', 'category' => 'f318', 'chart-area' => 'f239', 'chart-bar' => 'f185', 'chart-line' => 'f238', 'chart-pie' => 'f184', 'clock' => 'f469', 'cloud' => 'f176', 'dashboard' => 'f226', 'desktop' => 'f472', 'dismiss' => 'f153', 'download' => 'f316', 'edit' => 'f464', 'editor-aligncenter' => 'f207', 'editor-alignleft' => 'f206', 'editor-alignright' => 'f208', 'editor-bold' => 'f200', 'editor-customchar' => 'f220', 'editor-distractionfree' => 'f211', 'editor-help' => 'f223', 'editor-indent' => 'f222', 'editor-insertmore' => 'f209', 'editor-italic' => 'f201', 'editor-justify' => 'f214', 'editor-kitchensink' => 'f212', 'editor-ol' => 'f204', 'editor-outdent' => 'f221', 'editor-paste-text' => 'f217', 'editor-paste-word' => 'f216', 'editor-quote' => 'f205', 'editor-removeformatting' => 'f218', 'editor-rtl' => 'f320', 'editor-spellcheck' => 'f210', 'editor-strikethrough' => 'f224', 'editor-textcolor' => 'f215', 'editor-ul' => 'f203', 'editor-underline' => 'f213', 'editor-unlink' => 'f225', 'editor-video' => 'f219', 'email' => 'f465', 'email-alt' => 'f466', 'exerpt-view' => 'f164', 'facebook' => 'f304', 'facebook-alt' => 'f305', 'feedback' => 'f175', 'flag' => 'f227', 'format-aside' => 'f123', 'format-audio' => 'f127', 'format-chat' => 'f125', 'format-gallery' => 'f161', 'format-image' => 'f128', 'format-links' => 'f103', 'format-quote' => 'f122', 'format-standard' => 'f109', 'format-status' => 'f130', 'format-video' => 'f126', 'forms' => 'f314', 'googleplus' => 'f462', 'groups' => 'f307', 'hammer' => 'f308', 'id' => 'f336', 'id-alt' => 'f337', 'image-crop' => 'f165', 'image-flip-horizontal' => 'f169', 'image-flip-vertical' => 'f168', 'image-rotate-left' => 'f166', 'image-rotate-right' => 'f167', 'images-alt' => 'f232', 'images-alt2' => 'f233', 'info' => 'f348', 'leftright' => 'f229', 'lightbulb' => 'f339', 'list-view' => 'f163', 'location' => 'f230', 'location-alt' => 'f231', 'lock' => 'f160', 'marker' => 'f159', 'menu' => 'f333', 'migrate' => 'f310', 'minus' => 'f460', 'networking' => 'f325', 'no' => 'f158', 'no-alt' => 'f335', 'performance' => 'f311', 'plus' => 'f132', 'portfolio' => 'f322', 'post-status' => 'f173', 'pressthis' => 'f157', 'products' => 'f312', 'redo' => 'f172', 'rss' => 'f303', 'screenoptions' => 'f180', 'search' => 'f179', 'share' => 'f237', 'share-alt' => 'f240', 'share-alt2' => 'f242', 'shield' => 'f332', 'shield-alt' => 'f334', 'slides' => 'f181', 'smartphone' => 'f470', 'smiley' => 'f328', 'sort' => 'f156', 'sos' => 'f468', 'star-empty' => 'f154', 'star-filled' => 'f155', 'star-half' => 'f459', 'tablet' => 'f471', 'tag' => 'f323', 'testimonial' => 'f473', 'translation' => 'f326', 'trash' => 'f182', 'twitter' => 'f301', 'undo' => 'f171', 'update' => 'f463', 'upload' => 'f317', 'vault' => 'f178', 'video-alt' => 'f234', 'video-alt2' => 'f235', 'video-alt3' => 'f236', 'visibility' => 'f177', 'welcome-add-page' => 'f133', 'welcome-comments' => 'f117', 'welcome-edit-page' => 'f119', 'welcome-learn-more' => 'f118', 'welcome-view-site' => 'f115', 'welcome-widgets-menus' => 'f116', 'wordpress' => 'f120', 'wordpress-alt' => 'f324', 'yes' => 'f147');
}

/**
 * Array of social profiles for staff members
 *
 * @since 1.5.4
 */
function scwd_staff_social_array() {
	return apply_filters( 'scwd_staff_social_array', array(
		'twitter'        => array(
			'key'        => 'twitter',
			'meta'       => 'scwd_staff_twitter',
			'icon_class' => 'ticon ticon-twitter',
			'label'      => 'Twitter',
		),
		'facebook'        => array(
			'key'        => 'facebook',
			'meta'       => 'scwd_staff_facebook',
			'icon_class' => 'ticon ticon-facebook',
			'label'      => 'Facebook',
		),
		'instagram'      => array(
			'key'        => 'instagram',
			'meta'       => 'scwd_staff_instagram',
			'icon_class' => 'ticon ticon-instagram',
			'label'      => 'Instagram',
		),
		'linkedin'       => array(
			'key'        => 'linkedin',
			'meta'       => 'scwd_staff_linkedin',
			'icon_class' => 'ticon ticon-linkedin',
			'label'      => 'Linkedin',
		),
		'dribbble'       => array(
			'key'        => 'dribbble',
			'meta'       => 'scwd_staff_dribbble',
			'icon_class' => 'ticon ticon-dribbble',
			'label'      => 'Dribbble',
		),
		'vk'             => array(
			'key'        => 'vk',
			'meta'       => 'scwd_staff_vk',
			'icon_class' => 'ticon ticon-vk',
			'label'      => 'VK',
		),
		'skype'          => array(
			'key'        => 'skype',
			'meta'       => 'scwd_staff_skype',
			'icon_class' => 'ticon ticon-skype',
			'label'      => 'Skype',
		),
		'phone_number'   => array(
			'key'        => 'phone_number',
			'meta'       => 'scwd_staff_phone_number',
			'icon_class' => 'ticon ticon-phone',
			'label'      => esc_html__( 'Phone Number', 'scwd' ),
		),
		'email'          => array(
			'key'        => 'email',
			'meta'       => 'scwd_staff_email',
			'icon_class' => 'ticon ticon-envelope',
			'label'      => esc_html__( 'Email', 'scwd' ),
		),
		'website'        => array(
			'key'        => 'website',
			'meta'       => 'scwd_staff_website',
			'icon_class' => 'ticon ticon-external-link-square',
			'label'      => esc_html__( 'Website', 'scwd' ),
		),
	) );
}

/**
 * Creates an array for adding the staff social options to the metaboxes
 *
 * @since 1.5.4
 */
function scwd_staff_social_meta_array() {
	$profiles = scwd_staff_social_array();
	$array = array();
	foreach ( $profiles as $profile ) {
		$array[] = array(
			'title' => '<span class="'. $profile['icon_class'] .'"></span>' . $profile['label'],
			'id'    => $profile['meta'],
			'type'  => 'text',
			'std'   => '',
		);
	}
	return $array;
}

/**
 * Grid Columns
 *
 * @since 2.0.0
 */
function scwd_grid_columns() {
	return apply_filters( 'scwd_grid_columns', array(
		'1' => '1',
		'2' => '2',
		'3' => '3',
		'4' => '4',
		'5' => '5',
		'6' => '6',
		'7' => '7',
	) );
}

/**
 * Grid Column Gaps
 *
 * @since 2.0.0
 */
function scwd_column_gaps() {
	return apply_filters( 'scwd_column_gaps', array(
		''     => esc_html__( 'Default', 'scwd' ),
		'none' => '0px',
		'1'    => '1px',
		'5'    => '5px',
		'10'   => '10px',
		'15'   => '15px',
		'20'   => '20px',
		'25'   => '25px',
		'30'   => '30px',
		'35'   => '35px',
		'40'   => '40px',
		'50'   => '50px',
		'60'   => '60px',
	) );
}

/**
 * Typography Styles
 *
 * @since 2.0.0
 */
function scwd_typography_styles() {
	return apply_filters( 'scwd_typography_styles', array(
		''             => esc_html__( 'Default', 'scwd' ),
		'light'        => esc_html__( 'Light', 'scwd' ),
		'white'        => esc_html__( 'White', 'scwd' ),
		'white-shadow' => esc_html__( 'White with Shadow', 'scwd' ),
		'black'        => esc_html__( 'Black', 'scwd' ),
		'none'         => esc_html__( 'None', 'scwd' ),
	) );
}

/**
 * Button styles
 *
 * @since 1.6.2
 */
function scwd_button_styles() {
	return apply_filters( 'scwd_button_styles', array(
		''               => esc_html__( 'Default', 'scwd' ),
		'flat'           => esc_html__( 'Flat', 'scwd' ),
		'graphical'      => esc_html__( 'Graphical', 'scwd' ),
		'clean'          => esc_html__( 'Clean', 'scwd' ),
		'three-d'        => esc_html__( '3D', 'scwd' ),
		'outline'        => esc_html__( 'Outline', 'scwd' ),
		'minimal-border' => esc_html__( 'Minimal Border', 'scwd' ),
		'plain-text'     => esc_html__( 'Plain Text', 'scwd' ),
	) );
}

/**
 * Button colors
 *
 * @since 1.6.2
 * @deprecated since 4.4.1 - theme now uses new wpex_get_accent_colors function.
 */
function scwd_button_colors() {
	$button_colors = array();
	$accents = ( array ) scwd_get_accent_colors();
	if ( $accents ) {
		foreach ( $accents as $k => $v ) {
			if ( 'default' == $k ) {
				$button_colors[''] = $v['label'];
			} else {
				$button_colors[$k] = $v['label'];
			}
		}
	}
	return apply_filters( 'scwd_button_colors', $button_colors );
}

/**
 * Array of image crop locations
 *
 * @link 2.0.0
 */
function scwd_image_crop_locations() {
	return array(
		''              => esc_html__( 'Default', 'scwd' ),
		'left-top'      => esc_html__( 'Top Left', 'scwd' ),
		'right-top'     => esc_html__( 'Top Right', 'scwd' ),
		'center-top'    => esc_html__( 'Top Center', 'scwd' ),
		'left-center'   => esc_html__( 'Center Left', 'scwd' ),
		'right-center'  => esc_html__( 'Center Right', 'scwd' ),
		'center-center' => esc_html__( 'Center Center', 'scwd' ),
		'left-bottom'   => esc_html__( 'Bottom Left', 'scwd' ),
		'right-bottom'  => esc_html__( 'Bottom Right', 'scwd' ),
		'center-bottom' => esc_html__( 'Bottom Center', 'scwd' ),
		'soft-crop'     => esc_html__( 'Soft Crop', 'scwd' ),
	);
}

/**
 * Image Hovers
 *
 * @since 1.6.2
 */
function scwd_image_hovers() {
	return apply_filters( 'scwd_image_hovers', array(
		''             => esc_html__( 'Default', 'scwd' ),
		'opacity'      => esc_html__( 'Opacity', 'scwd' ),
		'shrink'       => esc_html__( 'Shrink', 'scwd' ),
		'grow'         => esc_html__( 'Grow', 'scwd' ),
		'side-pan'     => esc_html__( 'Side Pan', 'scwd' ),
		'vertical-pan' => esc_html__( 'Vertical Pan', 'scwd' ),
		'tilt'         => esc_html__( 'Tilt', 'scwd' ),
		'blurr'        => esc_html__( 'Normal - Blurr', 'scwd' ),
		'blurr-invert' => esc_html__( 'Blurr - Normal', 'scwd' ),
		'sepia'        => esc_html__( 'Sepia', 'scwd' ),
		'fade-out'     => esc_html__( 'Fade Out', 'scwd' ),
		'fade-in'      => esc_html__( 'Fade In', 'scwd' ),
	) );
}

/**
 * Text decorations
 *
 * @since 1.6.2
 */
function scwd_text_decorations() {
	return apply_filters( 'scwd_text_decorations', array(
		''             => esc_html__( 'Default', 'scwd' ),
		'underline'    => esc_html__( 'Underline', 'scwd' ),
		'overline'     => esc_html__( 'Overline','scwd' ),
		'line-through' => esc_html__( 'Line Through', 'scwd' ),
	) );
}

/**
 * Font Weights
 *
 * @since 1.6.2
 */
function scwd_font_weights() {
	return apply_filters( 'scwd_font_weights', array(
		''         => esc_html__( 'Default', 'scwd' ),
		'normal'   => esc_html__( 'Normal', 'scwd' ),
		'semibold' => esc_html__( 'Semibold','scwd' ),
		'bold'     => esc_html__( 'Bold', 'scwd' ),
		'bolder'   => esc_html__( 'Bolder', 'scwd' ),
		'100'      => '100',
		'200'      => '200',
		'300'      => '300',
		'400'      => '400',
		'500'      => '500',
		'600'      => '600',
		'700'      => '700',
		'800'      => '800',
		'900'      => '900',
	) );
}

/**
 * Font Style
 *
 * @since 1.6.2
 */
function scwd_font_styles() {
	return apply_filters( 'scwd_font_styles', array(
		''        => esc_html__( 'Default', 'scwd' ),
		'normal'  => esc_html__( 'Normal', 'scwd' ),
		'italic'  => esc_html__( 'Italic', 'scwd' ),
		'oblique' => esc_html__( 'Oblique', 'scwd' ),
	) );
}

/**
 * Text Transform
 *
 * @since 1.6.2
 */
function scwd_text_transforms() {
	return array(
		''           => esc_html__( 'Default', 'scwd' ),
		'none'       => esc_html__( 'None', 'scwd' ) ,
		'capitalize' => esc_html__( 'Capitalize', 'scwd' ),
		'uppercase'  => esc_html__( 'Uppercase', 'scwd' ),
		'lowercase'  => esc_html__( 'Lowercase', 'scwd' ),
	);
}

/**
 * Border Styles
 *
 * @since 1.6.0
 */
function scwd_border_styles() {
	return array(
		''       => esc_html__( 'Default', 'scwd' ),
		'solid'  => esc_html__( 'Solid', 'scwd' ),
		'dotted' => esc_html__( 'Dotted', 'scwd' ),
		'dashed' => esc_html__( 'Dashed', 'scwd' ),
	);
}

/**
 * Alignments
 *
 * @since 1.6.0
 */
function scwd_alignments() {
	return array(
		''       => esc_html__( 'Default', 'scwd' ),
		'left'   => esc_html__( 'Left', 'scwd' ),
		'right'  => esc_html__( 'Right', 'scwd' ),
		'center' => esc_html__( 'Center', 'scwd' ),
	);
}

/**
 * Visibility
 *
 * @since 1.6.0
 */
function scwd_visibility() {
	return apply_filters( 'scwd_visibility', array(
		''                         => esc_html__( 'Always Visible', 'scwd' ),
		'hidden'                   => esc_html__( 'Always Hidden', 'scwd' ),
		'show-at-mm-breakpoint'    => esc_html__( 'Visible At Mobile Menu Breakpoint', 'scwd' ),
		'hide-at-mm-breakpoint'    => esc_html__( 'Hidden At Mobile Menu Breakpoint', 'scwd' ),
		'hidden-desktop-large'     => esc_html__( 'Hidden on Large Desktops (1280px or greater)', 'scwd' ),
		'hidden-desktop'           => esc_html__( 'Hidden on Desktop (959px or greater)', 'scwd' ),
		'hidden-tablet-landscape'  => esc_html__( 'Hidden on Tablets: Landscape (768px to 1024px)', 'scwd' ),
		'hidden-tablet-portrait'   => esc_html__( 'Hidden on Tablets: Portrait (768px to 959px)', 'scwd' ),
		'hidden-tablet'            => esc_html__( 'Hidden on Tablets (768px to 959px)', 'scwd' ),
		'hidden-phone'             => esc_html__( 'Hidden on Phones (767px or smaller)', 'scwd' ),
		'visible-desktop-large'    => esc_html__( 'Visible on Large Desktops (1280px or greater)', 'scwd' ),
		'visible-desktop'          => esc_html__( 'Visible on Desktop (959px or greater)', 'scwd' ),
		'visible-phone'            => esc_html__( 'Visible on Phones (767px or smaller)', 'scwd' ),
		'visible-tablet'           => esc_html__( 'Visible on Tablets (768px to 959px)', 'scwd' ),
		'visible-tablet-landscape' => esc_html__( 'Visible on Tablets: Landscape (768px to 1024px)', 'scwd' ),
		'visible-tablet-portrait'  => esc_html__( 'Visible on Tablets: Portrait (768px to 959px)', 'scwd' ),
	) );
}

/**
 * CSS Animations
 *
 * @since 1.6.0
 */
function scwd_css_animations() {
	return apply_filters( 'scwd_css_animations', array(
		''              => esc_html__( 'None', 'scwd') ,
		'top-to-bottom' => esc_html__( 'Top to bottom', 'scwd' ),
		'bottom-to-top' => esc_html__( 'Bottom to top', 'scwd' ),
		'left-to-right' => esc_html__( 'Left to right', 'scwd' ),
		'right-to-left' => esc_html__( 'Right to left', 'scwd' ),
		'appear'        => esc_html__( 'Appear from center', 'scwd' ),
	) );
}

/**
 * Array of Hover CSS animations
 *
 * @since 2.0.0
 */
function scwd_hover_css_animations() {
	return apply_filters( 'scwd_hover_css_animations', array(
		''                       => esc_html__( 'Default', 'scwd' ),
		'shadow'                 => esc_html__( 'Shadow', 'scwd' ),
		'grow-shadow'            => esc_html__( 'Grow Shadow', 'scwd' ),
		'float-shadow'           => esc_html__( 'Float Shadow', 'scwd' ),
		'grow'                   => esc_html__( 'Grow', 'scwd' ),
		'shrink'                 => esc_html__( 'Shrink', 'scwd' ),
		'pulse'                  => esc_html__( 'Pulse', 'scwd' ),
		'pulse-grow'             => esc_html__( 'Pulse Grow', 'scwd' ),
		'pulse-shrink'           => esc_html__( 'Pulse Shrink', 'scwd' ),
		'push'                   => esc_html__( 'Push', 'scwd' ),
		'pop'                    => esc_html__( 'Pop', 'scwd' ),
		'bounce-in'              => esc_html__( 'Bounce In', 'scwd' ),
		'bounce-out'             => esc_html__( 'Bounce Out', 'scwd' ),
		'rotate'                 => esc_html__( 'Rotate', 'scwd' ),
		'grow-rotate'            => esc_html__( 'Grow Rotate', 'scwd' ),
		'float'                  => esc_html__( 'Float', 'scwd' ),
		'sink'                   => esc_html__( 'Sink', 'scwd' ),
		'bob'                    => esc_html__( 'Bob', 'scwd' ),
		'hang'                   => esc_html__( 'Hang', 'scwd' ),
		'skew'                   => esc_html__( 'Skew', 'scwd' ),
		'skew-backward'          => esc_html__( 'Skew Backward', 'scwd' ),
		'wobble-horizontal'      => esc_html__( 'Wobble Horizontal', 'scwd' ),
		'wobble-vertical'        => esc_html__( 'Wobble Vertical', 'scwd' ),
		'wobble-to-bottom-right' => esc_html__( 'Wobble To Bottom Right', 'scwd' ),
		'wobble-to-top-right'    => esc_html__( 'Wobble To Top Right', 'scwd' ),
		'wobble-top'             => esc_html__( 'Wobble Top', 'scwd' ),
		'wobble-bottom'          => esc_html__( 'Wobble Bottom', 'scwd' ),
		'wobble-skew'            => esc_html__( 'Wobble Skew', 'scwd' ),
		'buzz'                   => esc_html__( 'Buzz', 'scwd' ),
		'buzz-out'               => esc_html__( 'Buzz Out', 'scwd' ),
		'glow'                   => esc_html__( 'Glow', 'scwd' ),
		'shadow-radial'          => esc_html__( 'Shadow Radial', 'scwd' ),
		'box-shadow-outset'      => esc_html__( 'Box Shadow Outset', 'scwd' ),
		'box-shadow-inset'       => esc_html__( 'Box Shadow Inset', 'scwd' ),
	) );
}

/**
 * Image filter styles
 *
 * @since 1.4.0
 */
function scwd_image_filters() {
	return apply_filters( 'scwd_image_filters', array(
		''          => esc_html__( 'None', 'scwd' ),
		'grayscale' => esc_html__( 'Grayscale', 'scwd' ),
	) );
}

/**
 * Social Link styles
 *
 * @since 3.0.0
 */
function scwd_social_button_styles() {
	return apply_filters( 'scwd_social_button_styles', array(
		'default'            => esc_html__( 'Skin Default', 'scwd' ),
		'none'               => esc_html__( 'None', 'scwd' ),
		'minimal'            => esc_html__( 'Minimal', 'scwd' ),
		'minimal-rounded'    => esc_html__( 'Minimal Rounded', 'scwd' ),
		'minimal-round'      => esc_html__( 'Minimal Round', 'scwd' ),
		'flat'               => esc_html__( 'Flat', 'scwd' ),
		'flat-rounded'       => esc_html__( 'Flat Rounded', 'scwd' ),
		'flat-round'         => esc_html__( 'Flat Round', 'scwd' ),
		'flat-color'         => esc_html__( 'Flat Color', 'scwd' ),
		'flat-color-rounded' => esc_html__( 'Flat Color Rounded', 'scwd' ),
		'flat-color-round'   => esc_html__( 'Flat Color Round', 'scwd' ),
		'3d'                 => esc_html__( '3D', 'scwd' ),
		'3d-color'           => esc_html__( '3D Color', 'scwd' ),
		'black'              => esc_html__( 'Black', 'scwd' ),
		'black-rounded'      => esc_html__( 'Black Rounded', 'scwd' ),
		'black-round'        => esc_html__( 'Black Round', 'scwd' ),
		'black-ch'           => esc_html__( 'Black with Color Hover', 'scwd' ),
		'black-ch-rounded'   => esc_html__( 'Black with Color Hover Rounded', 'scwd' ),
		'black-ch-round'     => esc_html__( 'Black with Color Hover Round', 'scwd' ),
		'graphical'          => esc_html__( 'Graphical', 'scwd' ),
		'graphical-rounded'  => esc_html__( 'Graphical Rounded', 'scwd' ),
		'graphical-round'    => esc_html__( 'Graphical Round', 'scwd' ),
		'bordered'           => esc_html__( 'Bordered', 'scwd' ),
		'bordered-rounded'   => esc_html__( 'Bordered Rounded', 'scwd' ),
		'bordered-round'     => esc_html__( 'Bordered Round', 'scwd' ),
	) );
}

/**
 * Array of background patterns
 *
 * @since 4.0
 */
function scwd_get_background_patterns() {
	$url = scwd_asset_url( 'images/patterns/' );
	return apply_filters( 'scwd_get_background_patterns', array(
		'dark_wood' => array(
			'label' => esc_html__( 'Dark Wood', 'scwd' ),
			'url'   => $url . 'dark_wood.png',
		),
		'diagmonds' => array(
			'label' => esc_html__( 'Diamonds', 'scwd' ),
			'url'   => $url . 'diagmonds.png',
		),
		'grilled' => array(
			'label' => esc_html__( 'Grilled', 'scwd' ),
			'url'   => $url . 'grilled.png',
		),
		'lined_paper' => array(
			'label' => esc_html__( 'Lined Paper', 'scwd' ),
			'url'   => $url . 'lined_paper.png',
		),
		'old_wall' => array(
			'label' => esc_html__( 'Old Wall', 'scwd' ),
			'url'   => $url . 'old_wall.png',
		),
		'ricepaper' => array(
			'label' => esc_html__( 'Rice Paper', 'scwd' ),
			'url'   => $url . 'ricepaper.png',
		),
		'tree_bark' => array(
			'label' => esc_html__( 'Tree Bark', 'scwd' ),
			'url'   => $url . 'tree_bark.png',
		),
		'triangular' => array(
			'label' => esc_html__( 'Triangular', 'scwd' ),
			'url'   => $url . 'triangular.png',
		),
		'white_plaster' => array(
			'label' => esc_html__( 'White Plaster', 'scwd' ),
			'url'   => $url . 'white_plaster.png',
		),
		'wild_flowers' => array(
			'label' => esc_html__( 'Wild Flowers', 'scwd' ),
			'url'   => $url . 'wild_flowers.png',
		),
		'wood_pattern' => array(
			'label' => esc_html__( 'Wood Pattern', 'scwd' ),
			'url'   => $url . 'wood_pattern.png',
		),
	) );
}