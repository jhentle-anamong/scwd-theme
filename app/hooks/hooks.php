<?php
/**
 * Setup theme hooks
 *
 * @package Silver_Connect_Web
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Outer Wrap Hooks
 *
 * @since 2.0.0
 */
function scwd_outer_wrap_before() {
	do_action( 'scwd_outer_wrap_before' );
}
function scwd_outer_wrap_after() {
	do_action( 'scwd_outer_wrap_after' );
}


/**
 * Main Header Hooks
 *
 * @since 1.0
 */
function scwd_hook_header_before() {
	do_action( 'scwd_hook_header_before' );
}
function scwd_hook_header_top() {
	do_action( 'scwd_hook_header_top' );
}
function scwd_hook_header_inner() {
	do_action( 'scwd_hook_header_inner' );
}
function scwd_hook_header_bottom() {
	do_action( 'scwd_hook_header_bottom' );
}
function scwd_hook_header_after() {
	do_action( 'scwd_hook_header_after' );
}

/**
 * Logo
 *
 * @since 3.5.1
 */
function scwd_hook_site_logo_inner() {
	do_action( 'scwd_hook_site_logo_inner' );
}

/**
 * Wrap Hooks
 *
 * @since 1.0
 */
function scwd_hook_wrap_before() {
	do_action( 'scwd_hook_wrap_before' );
}
function scwd_hook_wrap_top() {
	do_action( 'scwd_hook_wrap_top' );
}
function scwd_hook_wrap_bottom() {
	do_action( 'scwd_hook_wrap_bottom' );
}
function scwd_hook_wrap_after() {
	do_action( 'scwd_hook_wrap_after' );
}

/**
 * Main Hooks
 *
 * @since 1.0
 */
function scwd_hook_main_before() {
	do_action( 'scwd_hook_main_before' );
}
function scwd_hook_main_top() {
	do_action( 'scwd_hook_main_top' );
}
function scwd_hook_main_bottom() {
	do_action( 'scwd_hook_main_bottom' );
}
function scwd_hook_main_after() {
	do_action( 'scwd_hook_main_after' );
}

/**
 * Primary Hooks
 *
 * @since 2.0.0
 */
function scwd_hook_primary_before() {
	do_action( 'scwd_hook_primary_before' );
}
function scwd_hook_primary_after() {
	do_action( 'scwd_hook_primary_after' );
}

/**
 * Content Hooks
 *
 * @since 1.0
 */
function scwd_hook_content_before() {
	do_action( 'scwd_hook_content_before' );
}
function scwd_hook_content_top() {
	do_action( 'scwd_hook_content_top' );
}
function scwd_hook_content_bottom() {
	do_action( 'scwd_hook_content_bottom' );
}
function scwd_hook_content_after() {
	do_action( 'scwd_hook_content_after' );
}

/**
 * Sidebar Hooks
 *
 * @since 1.0
 */
function scwd_hook_sidebar_before() {
	do_action( 'scwd_hook_sidebar_before' );
}
function scwd_hook_sidebar_after() {
	do_action( 'scwd_hook_sidebar_after' );
}
function scwd_hook_sidebar_top() {
	do_action( 'scwd_hook_sidebar_top' );
}
function scwd_hook_sidebar_bottom() {
	do_action( 'scwd_hook_sidebar_bottom' );
}
function scwd_hook_sidebar_inner() {
	do_action( 'scwd_hook_sidebar_inner' );
}

/**
 * Footer Hooks
 *
 * @since 1.0
 */
function scwd_hook_footer_before() {
	do_action( 'scwd_hook_footer_before' );
}
function scwd_hook_footer_top() {
	do_action( 'scwd_hook_footer_top' );
}
function scwd_hook_footer_inner() {
	do_action( 'scwd_hook_footer_inner' );
}
function scwd_hook_footer_bottom() {
	do_action( 'scwd_hook_footer_bottom' );
}
function scwd_hook_footer_after() {
	do_action( 'scwd_hook_footer_after' );
}

/**
 * Footer Bottom Hooks
 *
 * @since 4.0
 */
function scwd_hook_footer_bottom_before() {
	do_action( 'scwd_hook_footer_bottom_before' );
}
function scwd_hook_footer_bottom_top() {
	do_action( 'scwd_hook_footer_bottom_top' );
}
function scwd_hook_footer_bottom_inner() {
	do_action( 'scwd_hook_footer_bottom_inner' );
}
function scwd_hook_footer_bottom_bottom() {
	do_action( 'scwd_hook_footer_bottom_bottom' );
}
function scwd_hook_footer_bottom_after() {
	do_action( 'scwd_hook_footer_bottom_after' );
}

/**
 * Main Menu Hooks
 *
 * @since 1.0
 */
function scwd_hook_main_menu_before() {
	do_action( 'scwd_hook_main_menu_before' );
}
function scwd_hook_main_menu_top() {
	do_action( 'scwd_hook_main_menu_top' );
}
function scwd_hook_main_menu_bottom() {
	do_action( 'scwd_hook_main_menu_bottom' );
}
function scwd_hook_main_menu_after() {
	do_action( 'scwd_hook_main_menu_after' );
}

/**
 * Page Header Hooks
 *
 * @since 1.0
 */
function scwd_hook_page_header_before() {
	do_action( 'scwd_hook_page_header_before' );
}
function scwd_hook_page_header_top() {
	do_action( 'scwd_hook_page_header_top' );
}
function scwd_hook_page_header_inner() {
	do_action( 'scwd_hook_page_header_inner' );
}
function scwd_hook_page_header_bottom() {
	do_action( 'scwd_hook_page_header_bottom' );
}
function scwd_hook_page_header_after() {
	do_action( 'scwd_hook_page_header_after' );
}