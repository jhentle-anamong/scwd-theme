<?php
/**
 * These functions are used to load template parts (partials) when used within action hooks,
 * and they probably should never be updated or modified.
 *
 * @package SCWD WordPress Theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*-------------------------------------------------------------------------------*/
/* -  Accessibility
/*-------------------------------------------------------------------------------*/

/**
 * Get skip to content link
 *
 * @since 4.2
 */
function scwd_skip_to_content_link() {
	if ( scwd_get_mod( 'skip_to_content', true ) ) {
		get_template_part( 'partials/accessibility/skip-to-content' );
	}
}

/*-------------------------------------------------------------------------------*/
/* -  Header
/*-------------------------------------------------------------------------------*/

/**
 * Get the header template part if enabled.
 */
function scwd_header() {
	if ( scwd_has_header() ) {
		scwd_get_template_part( 'header' );
	}
}

/**
 * Get the header logo template part.
 *
 * @since 1.0.0
 */
function scwd_header_logo() {
	scwd_get_template_part( 'header_logo' );
}

/**
 * Get the header logo inner content
 *
 * @since 4.5.5
 */
function scwd_header_logo_inner() {
	scwd_get_template_part( 'header_logo_inner' );
}

/**
 * Get the header aside content template part.
 *
 * @since 1.5.3
 */
/*function wpex_header_aside() {
	if ( wpex_header_supports_aside() ) {
		wpex_get_template_part( 'header_aside' );
	}
}*/

/**
 * Add search dropdown to header inner
 *
 * @since 1.0.0
 */
function scwd_header_inner_search_dropdown() {

	// Make sure site is set to dropdown style
	if ( 'drop_down' != scwd_header_menu_search_style() ) {
		return;
	}

	// Only added in the header for certain styles
	if ( in_array( scwd_header_style(), array( 'two', 'three', 'four', 'five', 'six', 'seven' ) ) ) {
		return;
	}

	// Get template part
	scwd_get_template_part( 'header_search_dropdown' );

}

/**
 * Get header search replace template part.
 *
 * @since 1.0.0
 */
function scwd_search_header_replace() {
	if ( 'header_replace' == scwd_header_menu_search_style() ) {
		scwd_get_template_part( 'header_search_replace' );
	}
}

/**
 * Gets header search overlay template part.
 *
 * @since 1.0.0
 */
function scwd_search_overlay() {
	if ( 'overlay' == scwd_header_menu_search_style() ) {
		scwd_get_template_part( 'header_search_overlay' );
	}
}

/**
 * Overlay Header Wrap Open
 *
 * @since 1.0.0
 */
function scwd_overlay_header_wrap_open() {
	if ( scwd_has_overlay_header() ) {
		echo '<div id="overlay-header-wrap" class="clr">';
	}
}

/**
 * Overlay Header Wrap Close
 *
 * @since 1.0.0
 */
function scwd_overlay_header_wrap_close() {
	if ( scwd_has_overlay_header() ) {
		echo '</div><!-- .overlay-header-wrap -->';
	}
}

/*-------------------------------------------------------------------------------*/
/* -  Menu
/*-------------------------------------------------------------------------------*/

/**
 * Outputs the main header menu
 *
 * @since 1.0.0
 */
function scwd_header_menu() {
	$get          = true; // should be set to false
	$header_style = scwd_header_style();
	$filter       = current_filter();


	// Header Inner Hook
	if ( 'scwd_hook_header_inner' == $filter ) {
		if ( in_array( $header_style, array( 'one', 'five', 'six', 'vertical-2', 'seven' ) ) ) {
			$get = true;
		}
	}

	// Header Top Hook
	elseif ( 'scwd_hook_header_top' == $filter ) {
		if (  'four' == $header_style ) {
			$get = true;
		}
	}

	// Header bottom hook
	elseif ( 'scwd_hook_header_bottom' == $filter ) {
		if ( in_array( $header_style, array( 'two', 'three' ) ) ) {
			$get = true;
		}
	}


	// Get menu template part
	if ( $get ) {
		scwd_get_template_part( 'header_menu' );
	}

}

/*-------------------------------------------------------------------------------*/
/* -  Menu > Mobile
/*-------------------------------------------------------------------------------*/

/**
 * Gets the template part for the fixed top mobile menu style
 *
 * @since 3.0.0
 */
function scwd_mobile_menu_fixed_top() {
	if ( scwd_header_has_mobile_menu() && 'fixed_top' == scwd_header_menu_mobile_toggle_style() ) {
		scwd_get_template_part( 'header_mobile_menu_fixed_top' );
	}
}

/**
 * Gets the template part for the navbar mobile menu_style
 *
 * @since 3.0.0
 */
function scwd_mobile_menu_navbar() {

	// Get var
	$get = false;

	// Current filter
	$filter = current_filter();

	// Check where to place menu
	$before_wrap = ( 'outer_wrap_before' == scwd_get_mod( 'mobile_menu_navbar_position' ) ) ? true : false;

	// Check if overlay header is enabled
	if ( ! $before_wrap ) {
		$before_wrap = scwd_has_overlay_header();
	}

	// Overlay header should display above and others below
	if ( $filter == 'scwd_outer_wrap_before' && $before_wrap ) {
		$get = true;
	} elseif ( $filter == 'scwd_hook_header_bottom' && ! $before_wrap ) {
		$get = true;
	}

	// Get mobile menu navbar
	if ( $get && scwd_header_has_mobile_menu() && 'navbar' == scwd_header_menu_mobile_toggle_style() ) {
		scwd_get_template_part( 'header_mobile_menu_navbar' );
	}

}

/**
 * Gets the template part for the "icons" style mobile menu.
 *
 * @since 1.0.0
 */
function scwd_mobile_menu_icons() {
	$style = scwd_header_menu_mobile_toggle_style();
	if ( scwd_header_has_mobile_menu()
		&& ( 'icon_buttons' == $style || 'icon_buttons_under_logo' == $style )
	) {
		scwd_get_template_part( 'header_mobile_menu_icons' );
	}
}

/**
 * Get mobile menu alternative if enabled.
 *
 * @since 1.3.0
 */
function scwd_mobile_menu_alt() {
	if ( scwd_has_mobile_menu_alt() ) {
		scwd_get_template_part( 'header_mobile_menu_alt' );
	}
}

/**
 * Sidr Close button
 *
 * @since 3.2.0
 */
function scwd_sidr_close() {
	if ( 'sidr' != scwd_header_menu_mobile_style() ) {
		return;
	}
	echo '<div id="sidr-close"><div class="scwd-close"><a href="#" aria-hidden="true" role="button" tabindex="-1">&times;</a></div></div>';
}

/*-------------------------------------------------------------------------------*/
/* -  Page Header
/*-------------------------------------------------------------------------------*/

/**
 * Get page header template part if enabled.
 *
 * @since 1.5.2
 */
function scwd_page_header() {
	if ( scwd_has_page_header() ) {
		scwd_get_template_part( 'page_header' );
	}
}

/**
 * Get page header title template part if enabled.
 *
 * @since 1.0.0
 */
function scwd_page_header_title() {
	if ( scwd_has_page_header_title() ) {
		scwd_get_template_part( 'page_header_title' );
	}
}

/**
 * Get post heading template part.
 *
 * @since 1.0.0
 */
function scwd_page_header_subheading() {
	if ( scwd_page_header_has_subheading() ) {
		scwd_get_template_part( 'page_header_subheading' );
	}
}

/**
 * Open wrapper around page header content to vertical align things
 *
 * @since 3.3.3
 */
function scwd_page_header_title_table_wrap_open() {
	if ( 'background-image' == scwd_page_header_style() ) {
		echo '<div class="page-header-table clr"><div class="page-header-table-cell">';
	}
}

/**
 * Close wrapper around page header content to vertical align things
 *
 * @since 3.3.3
 */
function scwd_page_header_title_table_wrap_close() {
	if ( 'background-image' == scwd_page_header_style() ) {
		echo '</div></div>';
	}
}

/*-------------------------------------------------------------------------------*/
/* -  Content
/*-------------------------------------------------------------------------------*/

/**
 * Gets sidebar template
 *
 * @since 2.1.0
 */
function scwd_get_sidebar_template() {
	if ( ! in_array( scwd_content_area_layout(), array( 'full-screen', 'full-width' ) ) ) {
		get_sidebar( apply_filters( 'scwd_get_sidebar_template', null ) );
	}
}

/**
 * Displays correct sidebar
 *
 * @since 1.6.5
 */
function scwd_display_sidebar() {
	if ( scwd_has_sidebar() && $sidebar = scwd_get_sidebar() ) {
		dynamic_sidebar( $sidebar );
	}
}

/**
 * Get term description.
 *
 * @since 1.0.0
 */
function scwd_term_description() {
	if ( scwd_has_term_description_above_loop() ) {
		scwd_get_template_part( 'term_description' );
	}
}

/**
 * Get next/previous links.
 *
 * @since 1.0.0
 */
function scwd_next_prev() {
	if ( scwd_has_next_prev() ) {
		scwd_get_template_part( 'next_prev' );
	}
}

/**
 * Get next/previous links.
 *
 * @since 1.0.0
 */
function scwd_post_edit() {
	if ( scwd_has_post_edit() ) {
		scwd_get_template_part( 'post_edit' );
	}
}

/*-------------------------------------------------------------------------------*/
/* -  Footer
/*-------------------------------------------------------------------------------*/

/**
 * Gets the footer layout template part.
 *
 * @since 2.0.0
 */
function scwd_footer() {
	if ( scwd_has_footer() ) {
		scwd_get_template_part( 'footer' );
	}
}

/**
 * Get the footer widgets template part.
 *
 * @since 1.0.0
 */
function scwd_footer_widgets() {
	scwd_get_template_part( 'footer_widgets' );
}

/**
 * Gets the footer bottom template part.
 *
 * @since 1.0.0
 */
function scwd_footer_bottom() {
	if ( scwd_has_footer_bottom() ) {
		scwd_get_template_part( 'footer_bottom' );
	}
}

/**
 * Gets the scroll to top button template part.
 *
 * @since 1.0.0
 */
function scwd_scroll_top() {
	if ( scwd_get_mod( 'scroll_top', true ) ) {
		scwd_get_template_part( 'scroll_top' );
	}
}

/*-------------------------------------------------------------------------------*/
/* -  Footer Bottom
/*-------------------------------------------------------------------------------*/

/**
 * Footer bottom flex box open
 *
 * @since 4.9.3
 */
function scwd_footer_bottom_flex_open() {
	echo '<div class="footer-bottom-flex clr">';
}


/**
 * Footer bottom flex box close
 *
 * @since 4.9.3
 */
function scwd_footer_bottom_flex_close() {
	echo '</div><!-- .footer-bottom-flex -->';
}

/**
 * Footer bottom copyright
 *
 * @since 2.0.0
 */
function scwd_footer_bottom_copyright() {
	scwd_get_template_part( 'footer_bottom_copyright' );
}

/**
 * Footer bottom menu
 *
 * @since 2.0.0
 */
function scwd_footer_bottom_menu() {
	scwd_get_template_part( 'footer_bottom_menu' );
}

/*-------------------------------------------------------------------------------*/
/* -  Other
/*-------------------------------------------------------------------------------*/

/**
 * Site Overlay
 *
 * @since 3.4.0
 */
function scwd_site_overlay() {
	echo '<div class="scwd-site-overlay"></div>';
}

/**
 * Adds a hidden searchbox in the footer for use with the mobile menu
 *
 * @since 1.5.1
 */
function scwd_mobile_searchform() {
	if ( scwd_get_mod( 'mobile_menu_search', true ) ) {
		$mm_style = scwd_header_menu_mobile_style();
		if ( $mm_style && 'custom' != $mm_style ) {
			scwd_get_template_part( 'mobile_searchform' );
		}
	}
}

/**
 * Outputs page/post slider based on the wpex_post_slider_shortcode custom field
 *
 * @since 1.0.0
 */
function scwd_post_slider( $post_id = '', $postion = '' ) {

	// Get post id
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();

	// Return if there isn't a slider defined
	if ( ! scwd_post_has_slider( $post_id ) ) {
		return;
	}

	// Get current filter
	$filter = current_filter();

	// Define get variable
	$get = false;

	// Get slider position
	$position = scwd_post_slider_position( $post_id );

	// Get current filter against slider position
	if ( 'above_topbar' == $position && 'scwd_hook_topbar_before' == $filter ) {
		$get = true;
	} elseif ( 'above_header' == $position && 'scwd_hook_header_before' == $filter ) {
		$get = true;
	} elseif ( 'above_menu' == $position && 'scwd_hook_header_bottom' == $filter ) {
		$get = true;
	} elseif ( 'above_title' == $position && 'scwd_hook_page_header_before' == $filter ) {
		$get = true;
	} elseif ( 'below_title' == $position && 'scwd_hook_main_top' == $filter ) {
		$get = true;
	}

	// Return if $get is still false after checking filters
	if ( $get ) {
		scwd_get_template_part( 'post_slider' );
	}

}