<?php
/**
 * Conditonal functions.
 * These functions load before anything else in the main theme class so they can be used
 * early on in pretty much any hook.
 *
 * @package SCWD WordPress Theme
 *
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Check if responsiveness is enabled.
 *
 * @since 4.0
 */
function scwd_is_layout_responsive() {
	return apply_filters( 'scwd_is_layout_responsive', scwd_get_mod( 'responsive', true ) );
}

/**
 * Check if current page has a sidebar.
 *
 * @since 4.3
 */
function scwd_has_sidebar( $post_id = '' ) {
	$bool = in_array( scwd_content_area_layout( $post_id ), array( 'left-sidebar', 'right-sidebar' ) ) ? true : false;
	return apply_filters( 'scwd_has_sidebar', $bool );
}

/**
 * Check if Google Services are enabled.
 *
 * @since 4.3
 */
function scwd_has_google_services_support() {
	return scwd_disable_google_services() ? false : true;
}

/*-------------------------------------------------------------------------------*/
/* [ Blog ]
/*-------------------------------------------------------------------------------*/

/**
 * Returns true if the current Query is a query related to standard blog posts.
 *
 * @since 1.6.0
 */
function scwd_is_blog_query() {

	// False by default
	$bool = false;

	// Return true for blog archives
	if ( is_search() ) {
		$bool = false; // Fixes wp bug
	} elseif (
		is_home()
		|| is_category()
		|| is_tag()
		|| is_date()
		|| is_author()
		|| is_page_template( 'templates/blog.php' )
		|| is_page_template( 'templates/blog-content-above.php' )
		|| ( is_tax( 'post_series' ) && 'post' == get_post_type() )
		|| ( is_tax( 'post_format' ) && 'post' == get_post_type() )
	) {
		$bool = true;
	}

	// Apply filters and return
	return apply_filters( 'scwd_is_blog_query', $bool );

}