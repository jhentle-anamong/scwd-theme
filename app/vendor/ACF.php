<?php

/**
 * Advanced Custom Field PRO
 *
 * @package SCWD WordPress Theme
 * @subpackage 3rd Party
 * @version 1.0
 */

namespace SCWDTheme\Vendor;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class ACF
{

	function __construct()
	{
		// Define path and URL to the ACF plugin.
		define( 'SCWD_ACF_PATH', SCWD_APP_DIR . 'lib/acf/' );
		define( 'SCWD_ACF_URL', SCWD_APP_DIR_URI . 'lib/acf/' );

		// Include the ACF plugin.
		include_once( SCWD_ACF_PATH . 'acf.php' );

		add_filter('acf/settings/url', array($this, 'settings_url'));
		add_filter('acf/settings/show_admin', array($this, 'settings_show_admin'));
		add_filter('acf/settings/save_json', array($this, 'json_save_point'));
		add_filter('acf/settings/load_json', array($this, 'json_load_point'));
	}

	// Customize the url setting to fix incorrect asset URLs.
	public function settings_url( $url ) {
		return SCWD_ACF_URL;
	}

	// (Optional) Hide the ACF admin menu item.
	function settings_show_admin( $show_admin ) {
		return true;
	}

	// Customize the JSON file save point
	function json_save_point( $path ) {
		// update path
		$path = SCWD_APP_DIR .'lib/acf/acf-json';

		return $path;
	}

	// Customize the JSON file load point
	function json_load_point( $paths ) {
		// remove original path (optional)
	    unset($paths[0]);

	    // append path
	    $paths[] = SCWD_APP_DIR .'lib/acf/acf-json';

	    // return
	    return $paths;
	}
}

new ACF();