<?php
/**
 * Post Slider functions
 *
 * @package SCWD WordPress Theme
 * @subpackage Framework
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Check if a post has a slider defined
 *
 * @since 1.0
 */
function scwd_post_has_slider( $post_id = '' ) {

	// Check for shortcode
	if ( scwd_get_post_slider_shortcode( $post_id ) ) {
		$return = true;
	} else {
		$return = false;
	}

	// Apply filters and return
	return apply_filters( 'scwd_has_post_slider', $return );
}

/**
 * Get correct post slider position
 *
 * @since 1.0
 */
function scwd_post_slider_position( $post_id = '' ) {

	// Default position is below the title
	$position = 'below_title';

	// Get post id
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();

	// Define empty meta var
	$meta = '';

	// Check meta field for position
	if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_post_slider_shortcode_position', true ) ) {
		$position = $meta;
	}

	// Apply filters and return
	return apply_filters( 'scwd_post_slider_position', $position, $meta );

}

/**
 * Get correct post slider shortcode
 *
 * @since 1.0
 */
function scwd_get_post_slider_shortcode( $post_id = '' ) {

	// None by default
	$slider = '';

	// Get post Id
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();

	// Check meta field for slider shortcode
	if ( $post_id ) {
		$slider = get_post_meta( $post_id, 'scwd_post_slider_shortcode', true );
		$slider = $slider ? $slider : get_post_meta( $post_id, 'scwd_page_slider_shortcode', true ); // deprecated option
	}

	// Apply filters and return
	return apply_filters( 'scwd_post_slider_shortcode', $slider );

}