<?php
/**
 * Create awesome overlays for image hovers
 *
 * @package SCWD WordPress Theme
 * @subpackage Framework
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Displays the Overlay HTML
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'scwd_overlay' ) ) {

	function scwd_overlay( $position = 'inside_link', $style = '', $args = array() ) {

		// If style is set to none lets bail
		if ( 'none' == $style ) {
			return;
		}

		// If style not defined get correct style based on theme_mods
		elseif ( ! $style ) {
			$style = scwd_overlay_style();
		}

		// If style is defined lets locate and include the overlay template
		if ( $style ) {

			// Add position to args
			$args['overlay_position'] = $position;

			// Add new action for loading custom templates
			do_action( 'scwd_pre_include_overlay_template', $style, $args );

			// Load the overlay template
			$overlays_dir = 'partials/overlays/';
			$template = $overlays_dir . $style . '.php';
			$template = locate_template( $template, false );

			// Only load template if it exists
			if ( $template ) {
				include( $template );
			}

		}

	}

}

/**
 * Create an array of overlay styles so they can be altered via child themes
 *
 * @since 1.0.0
 */
function scwd_overlay_styles_array() {
	$styles = array(
		''                                => esc_html__( 'None', 'scwd' ),
		'hover-button'                    => esc_html__( 'Hover Button', 'scwd' ),
		'magnifying-hover'                => esc_html__( 'Magnifying Glass Hover', 'scwd' ),
		'plus-hover'                      => esc_html__( 'Plus Icon Hover', 'scwd' ),
		'plus-two-hover'                  => esc_html__( 'Plus Icon #2 Hover', 'scwd' ),
		'plus-three-hover'                => esc_html__( 'Plus Icon #3 Hover', 'scwd' ),
		'view-lightbox-buttons-buttons'   => esc_html__( 'View/Lightbox Icons Hover', 'scwd' ),
		'view-lightbox-buttons-text'      => esc_html__( 'View/Lightbox Text Hover', 'scwd' ),
		'title-center'                    => esc_html__( 'Title Centered', 'scwd' ),
		'title-center-boxed'              => esc_html__( 'Title Centered Boxed', 'scwd' ),
		'title-bottom'                    => esc_html__( 'Title Bottom', 'scwd' ),
		'title-bottom-see-through'        => esc_html__( 'Title Bottom See Through', 'scwd' ),
		'title-push-up'                   => esc_html__( 'Title Push Up', 'scwd' ),
		'title-excerpt-hover'             => esc_html__( 'Title + Excerpt Hover', 'scwd' ),
		'title-category-hover'            => esc_html__( 'Title + Category Hover', 'scwd' ),
		'title-category-visible'          => esc_html__( 'Title + Category Visible', 'scwd' ),
		'title-date-hover'                => esc_html__( 'Title + Date Hover', 'scwd' ),
		'title-date-visible'              => esc_html__( 'Title + Date Visible', 'scwd' ),
		'categories-title-bottom-visible' => esc_html__( 'Categories + Title Bottom Visible', 'scwd' ),
		'slideup-title-white'             => esc_html__( 'Slide-Up Title White', 'scwd' ),
		'slideup-title-black'             => esc_html__( 'Slide-Up Title Black', 'scwd' ),
		'category-tag'                    => esc_html__( 'Category Tag', 'scwd' ),
		'category-tag-two'                => esc_html__( 'Category Tag', 'scwd' ) .' 2',
		'thumb-swap'                      => esc_html__( 'Secondary Image Swap', 'scwd' ),
		'thumb-swap-title'                => esc_html__( 'Secondary Image Swap & Title', 'scwd' ),
		'video-icon'                      => esc_html__( 'Video Icon', 'scwd' ),
	);
	if ( SCWD_WOOCOMMERCE_ACTIVE ) {
		$styles['title-price-hover'] = esc_html__( 'Title + Price Hover', 'scwd' );
	}
	return apply_filters( 'scwd_overlay_styles_array', $styles );
}

/**
 * Returns the overlay type depending on your theme options & post type
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'scwd_overlay_style' ) ) {

	function scwd_overlay_style( $style = '' ) {

		$style = $style ? $style : get_post_type();

		if ( 'portfolio' == $style ) {
			$style = scwd_get_mod( 'portfolio_entry_overlay_style' );
		} elseif ( 'staff' == $style ) {
			$style = scwd_get_mod( 'staff_entry_overlay_style' );
		}

		return apply_filters( 'scwd_overlay_style', $style );

	}

}

/**
 * Returns the correct overlay Classname
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'scwd_overlay_classes' ) ) {

	function scwd_overlay_classes( $style = '' ) {

		// Return if style is none
		if ( 'none' == $style ) {
			return;
		}

		// Sanitize style
		$style = $style ? $style : scwd_overlay_style();

		// Return if no style is defined
		if ( ! $style ) {
			return;
		}

		// Check mobile support (for hovers)
		$hover = '';
		$mobile_support = false;
		if ( in_array( $style, apply_filters( 'scwd_overlays_with_hover', array(
			'hover-button',
			'magnifying-hover',
			'plus-hover',
			'plus-two-hover',
			'plus-three-hover',
			'view-lightbox-buttons-buttons',
			'view-lightbox-buttons-text',
			'title-push-up',
			'title-excerpt-hover',
			'title-category-hover',
			'title-date-hover',
			'slideup-title-white',
			'slideup-title-black',
			'thumb-swap',
			'thumb-swap-title',
		) ) ) ) {
			$mobile_support = true;
			$hover = ' overlay-h';
		}

		$mobile_support = apply_filters( 'scwd_overlay_mobile_support', $mobile_support, $style );
		$mobile_support = $mobile_support ? ' overlay-ms' : '';

		// Return classes
		return 'overlay-parent overlay-parent-' . $style . $mobile_support . $hover;

	}

}