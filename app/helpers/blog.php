<?php
/**
 * Helper functions for the blog
 *
 * @package SCWD WordPress Theme
 * @subpackage Framework
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Returns single blog post blocks
 *
 * @since 2.0.0
 * @todo  rename to 'scwd_blog_single_blocks' for consistency
 */
function scwd_blog_single_layout_blocks() {

	// Default blocks
	$defaults = array(
		// 'featured_media',
		'title',
		// 'meta',
		// 'post_series',
		'the_content',
		// 'post_tags',
		// 'social_share',
		// 'author_bio',
		// 'related_posts',
		// 'comments',
	);

	// Get layout blocks
	$blocks = scwd_get_mod( 'blog_single_composer' );

	// If blocks are empty return defaults
	$blocks = $blocks ? $blocks : $defaults;

	// Convert blocks to array so we can loop through them
	if ( ! is_array( $blocks ) ) {
		$blocks = explode( ',', $blocks );
	}

	// Set block keys equal to vals
	$blocks = $blocks ? array_combine( $blocks, $blocks ) : array();

	// Remove items if post is password protected
	if ( post_password_required() ) {
		unset( $blocks['featured_media'] );
		unset( $blocks['post_tags'] );
		unset( $blocks['social_share'] );
		unset( $blocks['author_bio'] );
		unset( $blocks['author_bio'] );
	}

	// Apply filters to single layout blocks
	// This filter won't add setting to Customizer
	// This setting is simply for filtering on front-end > must be different.
	$blocks = apply_filters( 'scwd_blog_single_layout_blocks', $blocks, 'front-end' );

	// Return blocks
	return $blocks;

}