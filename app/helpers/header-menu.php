<?php
/**
 * Site Header Menu Functions
 *
 * @package Silver_Connect_Web
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*-------------------------------------------------------------------------------*/
/* [ Table of contents ]
/*-------------------------------------------------------------------------------*

	# General
	# Search
	# Mobile

/*-------------------------------------------------------------------------------*/
/* [ General ]
/*-------------------------------------------------------------------------------*/

/**
 * Check if header has a menu
 *
 * @since 1.0
 */
function scwd_header_has_menu() {

	// Return false if header is disabled or set to custom header
	// if ( ! scwd_has_header() || scwd_has_custom_header() ) {
	if ( ! scwd_has_header() ) {
		return;
	}

	// Menu Location
	$menu_location = scwd_header_menu_location();

	// Custom menu
	$custom_menu = scwd_custom_menu();

	// Multisite global menu
	$ms_global_menu = apply_filters( 'scwd_ms_global_menu', false );

	// Display menu if defined
	if ( has_nav_menu( $menu_location ) || $custom_menu || $ms_global_menu ) {
		return true;
	}

}

/**
 * Returns correct header menu location
 *
 * @since 1.0
 */
function scwd_header_menu_location() {
	return apply_filters( 'scwd_main_menu_location', 'main_menu' );
}

/**
 * Return true if the header menu is sticky
 *
 * @since 4.5.2
 * @return bool
 */
function scwd_has_sticky_header_menu() {
	$bool = false;
	$header_style = scwd_header_style();
	if ( scwd_get_mod( 'fixed_header', true )
		&& ( 'two' == $header_style || 'three' == $header_style || 'four' == $header_style )
	) {
		$bool = true;
	}
	return apply_filters( 'scwd_has_sticky_header_menu', $bool );
}

/**
 * Returns correct menu classes
 *
 * @since 2.0.0
 * @return string
 */
function scwd_header_menu_classes( $return = '' ) {

	// Define classes array
	$classes = array();

	// Get data
	$header_style = scwd_header_style();
	$has_overlay  = scwd_has_overlay_header();

	// Return wrapper classes
	if ( 'wrapper' == $return ) {

		// Add Header Style to wrapper
		$classes[] = 'navbar-style-' . $header_style;

		// Add classes for the sticky header menu
		if ( scwd_has_sticky_header_menu() ) {
			$classes[] = 'fixed-nav';
		}

		// Dropdown caret
		if ( 'one' == $header_style || 'five' == $header_style || 'seven' == $header_style || $has_overlay ) {
			$classes[] = 'scwd-dropdowns-caret';
		}

		// Flush Dropdowns
		if ( scwd_get_mod( 'menu_flush_dropdowns' ) && 'one' == $header_style && ! $has_overlay ) {
			$classes[] = 'scwd-flush-dropdowns';
		}

		// Add special class if the dropdown top border option in the admin is enabled
		if ( scwd_get_mod( 'menu_dropdown_top_border' ) ) {
			$classes[] = 'scwd-dropdown-top-border';
		}

		// Disable header two borders
		if ( 'two' == $header_style && scwd_get_mod( 'header_menu_disable_borders', false ) ) {
			$classes[] = 'no-borders';
		}

		// Center items
		if ( 'two' == $header_style && scwd_get_mod( 'header_menu_center', false ) ) {
			$classes[] = 'center-items';
		}

		// Add breakpoint class
		if ( scwd_header_has_mobile_menu() ) {
			$classes[] = 'hide-at-mm-breakpoint';
		}

		// Add clearfix
		$classes[] = 'clr';

		// Set keys equal to vals
		$classes = array_combine( $classes, $classes );

		// Apply filters
		$classes = apply_filters( 'scwd_header_menu_wrap_classes', $classes );

	}

	// Inner Classes
	elseif ( 'inner' == $return ) {

		// Core
		$classes[] = 'navigation';
		$classes[] = 'main-navigation';
		$classes[] = 'clr';

		// Add the container div for specific header styles
		if ( in_array( $header_style, array( 'two', 'three', 'four' ) ) ) {
			$classes[] = 'container';
		}

		// Set keys equal to vals
		$classes = array_combine( $classes, $classes );

		// Apply filters
		$classes = apply_filters( 'scwd_header_menu_classes', $classes );

	}

	if ( is_array( $classes ) ) {
		return implode( ' ', $classes );
	}

	return $return;

}

/**
 * Returns correct menu classes
 *
 * @return string
 */
function scwd_header_menu_ul_classes() {
	$classes = array();
	if ( in_array( scwd_header_style(), array( 'vertical-2' ) ) ) {
		$classes[] = 'scwd-toggle-menu';
	} else {
		$classes[] = 'dropdown-menu';
		$classes[] = 'sf-menu';
	}
	return implode( ' ', apply_filters( 'scwd_header_menu_ul_classes', $classes ) );
}

/**
 * Custom menu walker
 *
 * @since 1.3.0
 */
if ( ! class_exists( 'SCWD_Dropdown_Walker_Nav_Menu' ) ) {
	class SCWD_Dropdown_Walker_Nav_Menu extends Walker_Nav_Menu {
		function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

			// Define vars
			$id_field     = $this->db_fields['id'];
			$header_style = scwd_header_style();

			// Down Arrows
			if ( ! empty( $children_elements[$element->$id_field] ) && ( $depth == 0 ) ) {
				$element->classes[] = 'dropdown';
				if ( scwd_get_mod( 'menu_arrow_down' ) ) {
					$arrow_class = 'six' == $header_style ? 'ticon-chevron-right' : 'ticon-angle-down';
					$element->title .= ' <span class="nav-arrow top-level ticon '. $arrow_class .'"></span>';
				}
			}

			// Right/Left Arrows
			if ( ! empty( $children_elements[$element->$id_field] ) && ( $depth > 0 ) ) {
				$element->classes[] = 'dropdown';
				if ( scwd_get_mod( 'menu_arrow_side', true ) ) {
					if ( 'vertical-2' == $header_style ) {
						$element->title .= ' <span class="nav-arrow second-level ticon ticon-angle-down"></span>';
					} else {
						if ( is_rtl() ) {
							$element->title .= '<span class="nav-arrow second-level ticon ticon-angle-left"></span>';
						} else {
							$element->title .= '<span class="nav-arrow second-level ticon ticon-angle-right"></span>';
						}
					}
				}
			}

			// Remove current menu item when using local-scroll class
			if ( is_array( $element->classes )
				&& in_array( 'local-scroll', $element->classes )
				&& in_array( 'current-menu-item', $element->classes )
			) {
				$key = array_search( 'current-menu-item', $element->classes );
				unset( $element->classes[$key] );
			}

			// Define walker
			Walker_Nav_Menu::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

		}
	}
}

/**
 * Checks for custom menus.
 *
 * @since 1.0.0
 */
function scwd_custom_menu() {
	$menu = get_post_meta( scwd_get_current_post_id(), 'scwd_custom_menu', true );
	$menu = 'default' != $menu ? $menu : '';
	return apply_filters( 'scwd_custom_menu', $menu );
}

/**
 * Adds the search icon to the menu items
 *
 * @since 1.0.0
 */
function scwd_add_search_to_menu( $items, $args ) {

	// Check menu location
	if ( ! in_array( $args->theme_location, apply_filters( 'scwd_menu_search_icon_theme_locations', array( 'main_menu' ) ) ) ) {
		return $items;
	}

	// Get search style
	$search_style = scwd_header_menu_search_style();

	// Return if disabled
	if ( ! $search_style || 'disabled' == $search_style ) {
		return $items;
	}

	// Define classes
	$li_classes = 'search-toggle-li scwd-menu-extra menu-item';
	$a_classes  = 'site-search-toggle';

	// Get header style
	$header_style = scwd_header_style();

	// Get correct search icon class
	if ( 'overlay' == $search_style) {
		$a_classes .= ' search-overlay-toggle';
	} elseif ( 'drop_down' == $search_style ) {
		$a_classes .= ' search-dropdown-toggle';
	} elseif ( 'header_replace' == $search_style ) {
		$a_classes .= ' search-header-replace-toggle';
	}

	// Ubermenu Classes for search icon
	if ( class_exists( 'UberMenu' ) && apply_filters( 'scwd_add_search_toggle_ubermenu_classes', true ) ) {
		$li_classes .= ' ubermenu-item-level-0 ubermenu-item';
		$a_classes  .= ' ubermenu-target ubermenu-item-layout-default ubermenu-item-layout-text_only';
	}

	// Add search icon and dropdown style
	$menu_search = '';
	$menu_search .= '<li class="'. $li_classes .'">';

		$menu_search .= '<a href="#" class="'. $a_classes .'">';

			$menu_search .= '<span class="link-inner">';

				$text = apply_filters( 'scwd_header_search_text', esc_html__( 'Search', 'scwd' ) );

				if ( 'six' == $header_style ) {
					$menu_search .= '<span class="scwd-menu-search-icon ticon ticon-search"></span>';
					$menu_search .= '<span class="scwd-menu-search-text">' . esc_html( $text ) . '</span>';
				} else {
					$menu_search .= '<span class="scwd-menu-search-text">' . esc_html( $text ) . '</span>';
					$menu_search .= '<span class="scwd-menu-search-icon ticon ticon-search" aria-hidden="true"></span>';
				}

			$menu_search .= '</span>';

		$menu_search .= '</a>';

		if ( in_array( $header_style, array( 'two', 'three', 'four', 'five', 'six', 'seven' ) )
			&& 'drop_down' == scwd_header_menu_search_style()
		) {
			ob_start();
			scwd_get_template_part( 'header_search_dropdown' );
			$menu_search .= ob_get_clean();
		}

	$menu_search .= '</li>';

	// Check search item position
	$menu_search_position = apply_filters( 'scwd_header_menu_search_position', 'end' );

	// Insert menu search
	if ( $menu_search_position == 'end' ) {
		$items = $items . $menu_search;
	} elseif ( $menu_search_position == 'start' ) {
		$items = $menu_search . $items;
	}

	// Return nav $items
	return $items;

}
add_filter( 'wp_nav_menu_items', 'scwd_add_search_to_menu', 11, 2 );

/*-------------------------------------------------------------------------------*/
/* [ Search ]
/*-------------------------------------------------------------------------------*/

/**
 * Check if current header menu style supports search icon
 *
 * @since 1.0
 */
function scwd_header_menu_supports_search() {
	if ( scwd_has_header() ) {
		return apply_filters( 'scwd_has_menu_search', true );
	}
}

/**
 * Returns correct header menu search style
 *
 * @since 1.0
 */
function scwd_header_menu_search_style() {

	// Return if not allowed
	if ( ! scwd_header_menu_supports_search() ) {
		return false;
	}

	// Get style as set in the customizer
	$style = scwd_get_mod( 'menu_search_style', 'drop_down' );

	// Don't allow header replace on overlay header
	if ( 'header_replace' == $style && scwd_has_overlay_header() ) {
		$style = 'overlay';
	}

	// Get style and apply filters
	$style = apply_filters( 'scwd_menu_search_style', $style );

	// Sanitize output so it's not empty and return
	$style = $style ? $style : 'drop_down';

	// Return style
	return $style;

}

/**
 * Returns header menu search form
 *
 * @since 4.5.2
 */
function scwd_get_header_menu_search_form() {
	if ( SCWD_WOOCOMMERCE_ACTIVE && scwd_get_mod( 'woo_header_product_searchform', false ) ) {
		$form = get_product_search_form( false );
	} else {
		$form = get_search_form( false );
	}
	return apply_filters( 'scwd_get_header_menu_search_form', $form );
}

/**
 * Returns header menu search form placeholder
 *
 * @since 4.5.2
 */
function scwd_get_header_menu_search_form_placeholder() {
	$style = scwd_header_menu_search_style();
	$placeholder = esc_html__( 'Search', 'scwd' );
	if ( 'overlay' == $style || 'header_replace' == $style ) {
		$placeholder = esc_html__( 'Type then hit enter to search&hellip;', 'scwd' );
	}
	return apply_filters( 'scwd_get_header_menu_search_form_placeholder', $placeholder );
}

/*-------------------------------------------------------------------------------*/
/* [ Mobile ]
/*-------------------------------------------------------------------------------*/

/**
 * Return correct header menu mobile style
 *
 * @since 1.0
 */
function scwd_header_menu_mobile_style() {

	// Get and sanitize style
	$style = scwd_get_mod( 'mobile_menu_style' );

	/* Toggle style not supported when overlay header is enabled
	// Deprecated in 3.5.0
	if ( 'toggle' == $style && scwd_has_overlay_header() ) {
		//$style = 'sidr';
	}*/

	// Disable if responsive is disabled
	$style = scwd_is_layout_responsive() ? $style : 'disabled';

	// Sanitize => can't be empty
	$style = $style ? $style : 'sidr';

	// Apply filters and return
	//@todo rename filter to scwd_header_menu_mobile_style and deprecate old filter
	return apply_filters( 'scwd_mobile_menu_style', $style );

}

/**
 * Check if mobile menu is enabled
 *
 * @since 1.0
 * @todo rename function to scwd_has_mobile_menu() ?
 */
function scwd_header_has_mobile_menu() {

	// Always return false for custom header
	if ( scwd_has_custom_header() ) {
		return false;
	}

	// Check if enabled
	if ( ! scwd_is_layout_responsive() ) {
		$bool = false;
	} elseif ( 'disabled' == scwd_header_menu_mobile_style() ) {
		$bool = false;
	} elseif ( has_nav_menu( 'mobile_menu_alt' ) || scwd_header_has_menu() ) {
		$bool = true;
	} else {
		$bool = false;
	}

	// Apply filters and return
	return apply_filters( 'scwd_has_mobile_menu', $bool );

}

/**
 * Return correct header menu mobile toggle style
 *
 * @since 1.0
 */
function scwd_header_mobile_menu_classes() {
	$classes = 'scwd-mobile-menu-toggle show-at-mm-breakpoint scwd-clr';
	return esc_attr( $classes );
}

/**
 * Return correct header menu mobile toggle style
 *
 * @since 1.0
 */
function scwd_header_menu_mobile_toggle_style() {

	// Set to false if header builder is enabled or mobile menu is disabled
	if ( 'disabled' == scwd_header_menu_mobile_style() ) {
		return false;
	}

	// Get current header style
	$header_style = scwd_header_style();

	// Get toggle style
	$toggle_style = scwd_get_mod( 'mobile_menu_toggle_style' );

	// Limit the style for some headers
	if ( 'seven' == $header_style ) {
		$toggle_style = 'icon_buttons';
	}

	// Sanitize => can't be empty
	$toggle_style = $toggle_style ? $toggle_style : 'icon_buttons';

	// Apply filters and return style
	return apply_filters( 'scwd_mobile_menu_toggle_style', $toggle_style );

}

/**
 * Return correct header menu mobile toggle style
 *
 * @since 1.0
 */
function scwd_header_menu_mobile_breakpoint() {
	$bp = scwd_has_vertical_header() ? null : scwd_get_mod( 'mobile_menu_breakpoint' );
	return intval( apply_filters( 'scwd_header_menu_mobile_toggle_style', $bp ) );
}

/**
 * Return sidr menu source
 *
 * @since 4.0
 */
function scwd_sidr_menu_source( $id = '' ) {

	// Define array of items
	$items = array();

	// Add close button
	$items['sidrclose'] = '#sidr-close';

	// Add mobile menu alternative if defined
	if ( scwd_has_mobile_menu_alt() ) {
		$items['nav'] = '#mobile-menu-alternative';
	}

	// If mobile menu alternative is not defined add main navigation
	else {
		$items['nav'] = '#site-navigation';
	}

	// Add search form
	if ( scwd_get_mod( 'mobile_menu_search', true ) ) {
		$items['search'] = '#mobile-menu-search';
	}

	// Apply filters for child theming
	$items = apply_filters( 'scwd_mobile_menu_source', $items );

	// Turn items into comma seperated list and return
	return implode( ', ', $items );

}

/**
 * Conditional check for alternative menu
 *
 * @since 4.0
 */
function scwd_has_mobile_menu_alt() {
	return apply_filters( 'scwd_has_mobile_menu_alt', has_nav_menu( 'mobile_menu_alt' ) );
}

/**
 * Return mobile menu extra icons
 *
 * @since 4.9
 */
function scwd_get_mobile_menu_toggle_icon() {

	$output = '';

	$aria_label = scwd_get_mod( 'mobile_menu_toggle_aria_label', esc_attr_x( 'Toggle mobile menu', 'aria-label', 'scwd' ) );

	$output .= '<a href="#" class="mobile-menu-toggle" aria-label="' . esc_attr( $aria_label ) .'">';

		$output .= apply_filters(
			'scwd_mobile_menu_open_button_text',
			'<span class="scwd-bars" aria-hidden="true"><span></span></span>'
		);

		$output .= '<span class="screen-reader-text">' . esc_html__( 'Open Mobile Menu', 'scwd' ) . '</span>';

	$output .= '</a>';

	return $output;

}

/**
 * Return mobile menu extra icons
 *
 * @since 4.6.6
 */
function scwd_get_mobile_menu_extra_icons() {

	$output = '';
	$menu_has_cart_icon = false;

	if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ 'mobile_menu' ] ) ) {

		$menu = wp_get_nav_menu_object( $locations[ 'mobile_menu' ] );

		if ( ! empty( $menu ) ) {

			$menu_items = wp_get_nav_menu_items( $menu->term_id );

			if ( $menu_items ) {

				foreach ( $menu_items as $key => $menu_item ) {

					// Only add items if a correct font icon is added for the menu item label
					if ( in_array( $menu_item->title, scwd_get_awesome_icons() ) ) {

						$title      = $menu_item->title;
						$attr_title = $menu_item->attr_title;
						$link_icon  = '<span class="ticon ticon-' . esc_attr( $title ) . '" aria-hidden="true"></span>';
						$classes    = 'mobile-menu-extra-icons mobile-menu-' . esc_attr( $title );

						if ( 'shopping-cart' == $title || 'shopping-bag' == $title || 'shopping-basket' == $title ) {
							$menu_has_cart_icon = true;
							$is_shop_icon = true;
						} else {
							$is_shop_icon = false;
						}

						if ( $is_shop_icon ) {
							$classes .= ' wpex-shop';
						}

						if ( ! empty( $menu_item->classes[0] ) ) {
							$classes .= ' '. implode( ' ', array_filter( $menu_item->classes, 'trim' ) );
						}

						$link_attrs = array(
							'href'  => esc_url( $menu_item->url ),
							'class' => esc_attr( $classes ),
							'title' => $attr_title,
						);

						$reader_text = $attr_title ? $attr_title : $title;
						$reader_text = '<span class="screen-reader-text">' . esc_html( $reader_text ) . '</span>';

						$inner_html = $link_icon . $reader_text;

						if ( $menu_has_cart_icon && $is_shop_icon && function_exists( 'scwd_mobile_menu_cart_count' ) ) {
							$inner_html .= scwd_mobile_menu_cart_count();
						}

						$output .= scwd_parse_html( 'a', $link_attrs, $inner_html ); ?>

				<?php }

				} // End foreach

			}

		}

	} // End menu check

	return apply_filters( 'scwd_get_mobile_menu_extra_icons', $output );

}