<?php
/**
 * Site Footer Helper Functions
 *
 * @package SCWD WordPress Theme
 * @subpackage Framework
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Check if footer is enabled
 *
 * @since 4.0
 */
function scwd_has_footer() {

	// Return true by default
	$bool = true;

	// Disabled on landing page
	if ( is_page_template( 'templates/landing-page.php' ) ) {
		$bool = false;
	}

	// Get current post id
	$post_id = scwd_get_current_post_id();

	// Check page settings
	if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_disable_footer', true ) ) {
		if ( 'on' == $meta ) {
			$bool = false;
		} elseif ( 'enable' == $meta ) {
			$bool = true;
		}
	}

	// Apply filters and return bool
	return apply_filters( 'scwd_display_footer', $bool );

}

/**
 * Check if footer has widgets
 *
 * @since 4.0
 */
function scwd_footer_has_widgets() {
	if ( scwd_has_custom_footer() || ! empty( $_GET[ 'scwd_inline_footer_template_editor' ] ) ) {
		$bool = scwd_get_mod( 'footer_builder_footer_widgets', false );
	} else {
		$bool = scwd_get_mod( 'footer_widgets', true );
	}
	$post_id = scwd_get_current_post_id();
	if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_disable_footer_widgets', true ) ) {
		if ( 'on' == $meta ) {
			$bool = false;
		} elseif ( 'enable' == $meta ) {
			$bool = true;
		}
	}
	return apply_filters( 'scwd_display_footer_widgets', $bool );
}

/**
 * Get footer builder ID
 *
 * @since 4.0
 */
function scwd_footer_builder_id() {
	if ( ! scwd_get_mod( 'footer_builder_enable', true ) ) {
		return;
	}
	$id = intval( apply_filters( 'scwd_footer_builder_page_id', scwd_get_mod( 'footer_builder_page_id' ) ) );
	if ( $id ) {
		$translated_id = scwd_parse_obj_id( $id, 'page' ); // translate
		$id = $translated_id ? $translated_id : $id; // if not translated return original ID
		if ( 'publish' == get_post_status( $id ) ) {
			return $id;
		}
	}
}

/**
 * Check if footer builder is enabled
 *
 * @since 4.6.5
 */
function scwd_has_custom_footer() {
	return scwd_footer_builder_id();
}

/**
 * Check if footer reveal is enabled
 *
 * @since 4.0
 */
function scwd_footer_has_reveal( $post_id = '' ) {

	// Disable here always
	if ( ! scwd_has_footer()
		|| 'boxed' == scwd_site_layout()
		|| 'six' == scwd_header_style()
		|| scwd_vc_is_inline()
	) {
		return false;
	}

	// Check customizer setting
	$bool = scwd_get_mod( 'footer_reveal', false );

	// Get current post id if not set
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();

	// Check page settings
	if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_footer_reveal', true ) ) {
		if ( 'on' == $meta ) {
			$bool = true;
		} elseif ( 'off' == $meta ) {
			$bool = false;
		}
	}

	// Apply filters and return
	return apply_filters( 'scwd_has_footer_reveal', $bool );
}

/**
 * Check if footer bottom is enabled
 *
 * @since 4.1
 */
function scwd_has_footer_bottom( $post_id = '' ) {
	if ( scwd_has_custom_footer() || ! empty( $_GET[ 'scwd_inline_footer_template_editor' ] ) ) {
		$bool = scwd_get_mod( 'footer_builder_footer_bottom', false );
	} else {
		$bool = scwd_get_mod( 'footer_bottom', true );
	}
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();
	if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_footer_bottom', true ) ) {
		if ( 'on' == $meta ) {
			$bool = true;
		} elseif ( 'off' == $meta ) {
			$bool = false;
		}
	}
	return apply_filters( 'scwd_has_footer_bottom', $bool );
}