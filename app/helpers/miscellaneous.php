<?php
/**
 * Miscellaneous functions
 *
 * @package SCWD WordPress Theme
 * @subpackage Framework
 * @version 1.0
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'dd' ) ) :
	/**
	 * Var_dump and die method
	 *
	 * @return void
	 */
	function dd() {
		echo '<pre>';
		array_map( function( $x ) {
			var_dump( $x );
		}, func_get_args() );
		echo '</pre>';
		die;
	}
endif;

if ( ! function_exists( 'pr' ) ) {
	/**
	 * Prints human-readable information about a variable
	 */
	function pr( $expression ) {
		echo '<pre>', print_r( $expression ) ,'</pre>';
	}
}

if ( ! function_exists( 'sanitize' ) ) {
	function sanitize( $array ) {
		foreach ( (array) $array as $key => $value ) {
			if ( is_array( $value ) ) {
				$array[$key] =  sanitize( $value );
			} else {
				$array[$key] = sanitize_text_field( $value );
			}
		}
		return $array;
	}
}