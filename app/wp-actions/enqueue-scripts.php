<?php
/**
 * Enqueue front end theme scripts [CSS & JS]
 *
 * @package SCWD WordPress Theme
 *
 * @version 1.0.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Core theme CSS.
 */
function scwd_enqueue_front_end_main_css() {

	// Register hover-css for use with shortcodes
	wp_register_style(
		'scwd-hover-animations',
		scwd_asset_url( 'lib/hover-css/hover-css.min.css' ),
		array(),
		'2.0.1'
	);

	wp_register_style(
		'fancybox',
		scwd_asset_url( 'lib/fancybox/jquery.fancybox.min.css' ),
		array(),
		'3.5.7'
	);

	// Main style.css File
	wp_enqueue_style(
		SCWD_THEME_STYLE_HANDLE,
		get_stylesheet_uri(),
		array(),
		SCWD_THEME_VERSION
	);
}
add_action( 'wp_enqueue_scripts', 'scwd_enqueue_front_end_main_css' );

/**
 * Load theme js.
 */
function scwd_enqueue_front_end_js() {

	// First lets make sure html5 shiv is on the site
	wp_enqueue_script(
		'scwd-html5shiv',
		scwd_asset_url( 'js/dynamic/html5.js' ),
		array(),
		SCWD_THEME_VERSION,
		false
	);
	wp_script_add_data( 'scwd-html5shiv', 'conditional', 'lt IE 9' );

	// Comment reply
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Register scripts
	wp_register_script(
		'fancybox',
		scwd_asset_url( 'lib/fancybox/jquery.fancybox.min.js' ),
		array( 'jquery', SCWD_THEME_JS_HANDLE ),
		'3.5.7',
		true
	);
	// wp_localize_script(
	// 	'fancybox',
	// 	'wpexLightboxSettings',
	// 	wpex_get_lightbox_settings()
	// );

	// Load Lightbox Globally
	/*if ( wpex_get_mod( 'lightbox_auto', false )
		|| apply_filters( 'wpex_load_ilightbox_globally', wpex_get_mod( 'lightbox_load_style_globally', false ) )
		) {
		wpex_enqueue_lightbox_scripts();
	}*/

	// Load minified js
	if ( scwd_get_mod( 'minify_js_enable', false ) ) { // default true

		wp_enqueue_script(
			SCWD_THEME_JS_HANDLE,
			scwd_asset_url( 'js/scwd.min.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

	}

	// Load all non-minified js
	else {

		wp_enqueue_script(
			'scwd-easing',
			scwd_asset_url( 'js/core/jquery.easing.js' ), // @todo maybe remove in the future since we are barely using it.
			array( 'jquery' ),
			'1.3.2',
			true
		);

		wp_enqueue_script(
			'scwd-superfish',
			scwd_asset_url( 'js/core/superfish.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'scwd-supersubs',
			scwd_asset_url( 'js/core/supersubs.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'scwd-hoverintent',
			scwd_asset_url( 'js/core/hoverintent.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'scwd-sidr',
			scwd_asset_url( 'js/core/sidr.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'scwd-equal-heights',
			scwd_asset_url( 'js/core/jquery.wpexEqualHeights.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'scwd-mousewheel',
			scwd_asset_url( 'js/core/jquery.mousewheel.js' ), // @REMOVE WHEN WE REMOVE LIGHTBOX
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'scwd-scrolly',
			scwd_asset_url( 'js/core/scrolly.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

		// Core global functions
		wp_enqueue_script(
			SCWD_THEME_JS_HANDLE,
			scwd_asset_url( 'js/scwd.js' ),
			array( 'jquery' ),
			SCWD_THEME_VERSION,
			true
		);

	}

	// Localize core js
	wp_localize_script( SCWD_THEME_JS_HANDLE, 'scwdLocalize', scwd_js_localize_data() );

	// Retina.js
	/*if ( wpex_is_retina_enabled() ) {

		wp_enqueue_script(
			'wpex-retina',
			scwd_asset_url( 'js/dynamic/retina.js' ),
			array( 'jquery' ),
			'1.3',
			true
		);

	}*/

	// Register social share script
	/*wp_register_script(
		'wpex-social-share',
		scwd_asset_url( 'js/dynamic/wpex-social-share.min.js' ),
		array( SCWD_THEME_JS_HANDLE ),
		SCWD_THEME_VERSION,
		true
	);*/

}
add_action( 'wp_enqueue_scripts', 'scwd_enqueue_front_end_js' );