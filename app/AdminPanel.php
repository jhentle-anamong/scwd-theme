<?php
/**
 * Main Theme Panel
 *
 * @package Total WordPress theme
 * @subpackage Framework
 * @version 1.0
 *
 * @todo Remove 'custom_id' parameter and instead add an ID parameter
 */

namespace SCWDTheme;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AdminPanel {

	/**
	 * Start things up
	 *
	 * @since 1.6.0
	 */
	public function __construct() {
		// Regisgert admin page and subpages
		// add_action( 'admin_menu', array( $this, 'add_menu_page' ), 0 );
		// add_action( 'admin_menu', array( $this, 'add_menu_subpage' ) );

		// Load admin scripts
		// add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// Register admin settings
		// add_action( 'admin_init', array( $this, 'register_settings' ) );

		// Load all the theme addons => Must run on this hook!!! (must load before the configs files)
		add_action( 'after_setup_theme', array( $this, 'load_addons' ), 2 );
	}

	/**
	 * Include addons
	 *
	 * @since 1.6.0
	 */
	public function load_addons() {

		// Addons directory location
		$dir = SCWD_APP_DIR . 'addons/';

		// Typography
		// if ( scwd_get_mod( 'typography_enable', true ) ) {
			require_once $dir . 'Typography.php';
		// }
	}
}

new AdminPanel();