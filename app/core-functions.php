<?php
/**
 * Core theme functions - VERY IMPORTANT!!
 *
 * These functions are used throughout the theme and must be loaded
 * early on.
 *
 * Do not ever edit this file, if you need to make
 * adjustments, please use a child theme. If you aren't sure how, please ask!
 *
 * @package Silver_Connect_Web
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*-------------------------------------------------------------------------------*/
/* [ General ]
/*-------------------------------------------------------------------------------*/

/**
 * Get Theme Branding.
 *
 * @since 1.0.0
 */
function scwd_get_theme_branding() {
	$branding = SCWD_THEME_BRANDING;
	if ( $branding && 'disabled' != $branding ) {
		return $branding;
	}
}

/**
 * Return correct assets url for loading scripts.
 *
 * @since 1.0.0
 */
if ( ! function_exists( 'scwd_asset_url' ) ) {
	function scwd_asset_url( $part = '' ) {
		return SCWD_THEME_URI . '/assets/' . $part;
	}
}

/**
 * Returns array of recommended plugins.
 *
 * @since 1.0.0
 */
function scwd_recommended_plugins() {
	return apply_filters( 'scwd_recommended_plugins', array(
		'smart-slider-3'		=> array(
			'name'				=> 'Smart Slider 3',
			'slug'				=> 'smart-slider-3',
			'required'			=> false,
			'force_activation'	=> false,
		),
		'elementor'				=> array(
			'name'				=> 'Elementor',
			'slug'				=> 'elementor',
			'required'			=> false,
			'force_activation'	=> false,
		),
		'contact-form-7'		=> array(
			'name'				=> 'Contact Form 7',
			'slug'				=> 'contact-form-7',
			'required'			=> false,
			'force_activation'	=> false,
		),
		'customize-posts'		=> array(
			'name'				=> 'Customize Posts',
			'slug'				=> 'customize-posts',
			'required'			=> false,
			'force_activation'	=> false,
		),
		'wordfence'				=> array(
			'name'				=> 'Wordfence Security',
			'slug'				=> 'wordfence',
			'required'			=> false,
			'force_activation'	=> false,
		),
		'cookie-law-info'		=> array(
			'name'				=> 'GDPR Cookie Consent',
			'slug'				=> 'cookie-law-info',
			'required'			=> true,
			'force_activation'	=> false
		)
	) );
}

/**
 * Returns current URL.
 *
 * @since 4.0
 */
function scwd_get_current_url() {
	global $wp;
	if ( $wp ) {
		return home_url( add_query_arg( array(), $wp->request ) );
	}
}

/**
 * Returns correct ID.
 *
 * Fixes some issues with posts page and 3rd party plugins that use custom pages for archives
 * such as WooCommerce. So we can correctly get post_meta values
 */
function scwd_get_current_post_id() {

	// Default value is empty
	$id = '';

	// If singular get_the_ID
	if ( is_singular() ) {
		$id = get_queried_object_id();
		$id = $id ? $id : get_the_ID(); // backup
	}

	// Posts page
	elseif ( is_home() && $page_for_posts = get_option( 'page_for_posts' ) ) {
		$id = $page_for_posts;
	}

	// Get current shop ID
	// if ( ! $id && scwd_is_woo_shop() ) {
	// 	$id = wpex_parse_obj_id( wc_get_page_id( 'shop' ) );
	// }

	// Apply filters and return
	return apply_filters( 'scwd_post_id', $id );

}

/**
 * Returns theme custom post types.
 *
 * @since 1.0.0
 */
function scwd_theme_post_types() {
	$post_types = array();
	$post_types = array_combine( $post_types, $post_types );
	return apply_filters( 'scwd_theme_post_types', $post_types );
}

/**
 * Returns body font size.
 * Used to convert EM values to PX values such as for responsive headings.
 *
 * @since 1.0.0
 */
function scwd_get_body_font_size() {
	$body_typo = scwd_get_mod( 'body_typography' );
	$font_size = ! empty( $body_typo['font-size'] ) ? $body_typo['font-size'] : 13;
	return apply_filters( 'scwd_get_body_font_size', $font_size );
}

/**
 * Echo the post URL.
 *
 * @since 1.0.0
 */
function scwd_permalink( $post_id = '' ) {
	echo scwd_get_permalink( $post_id );
}

/**
 * Return the post URL.
 *
 * @since 1.0.0
 */
function scwd_get_permalink( $post_id = '' ) {

	// If post ID isn't defined lets get it
	$post_id = $post_id ? $post_id : get_the_ID();

	// Check wpex_post_link custom field for custom link
	$redirect = scwd_get_post_redirect_link( $post_id );

	// If wpex_post_link custom field is defined return that otherwise return the permalink
	$permalink = $redirect ? $redirect : get_permalink( $post_id );

	// Apply filters and return
	return esc_url( apply_filters( 'scwd_permalink', $permalink ) );

}

/**
 * Get custom post link.
 *
 * @since 1.0.0
 */
function scwd_get_post_redirect_link( $post_id = '' ) {
	$post_id = $post_id ? $post_id : get_the_ID();
	return get_post_meta( $post_id, 'scwd_post_link', true );
}

/**
 * Return custom permalink.
 *
 * @since 1.0.0
 */
function scwd_get_custom_permalink() {
	if ( $custom_link = get_post_meta( get_the_ID(), 'scwd_post_link', true ) ) {
		$custom_link = ( 'home_url' == $custom_link ) ? home_url( '/' ) : $custom_link;
		return esc_url( $custom_link );
	}
}

/**
 * Returns correct site layout.
 *
 * @since 1.0
 */
function scwd_site_layout( $post_id = '' ) {

	// Check URL
	if ( ! empty( $_GET['site_layout'] ) ) {
		return esc_html( $_GET['site_layout'] );
	}

	// Get layout from theme mod
	$layout = scwd_get_mod( 'main_layout_style', 'full-width' );

	// Get post ID
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();

	// Check meta
	if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_main_layout', true ) ) {
		$layout = $meta;
	}

	// Apply filters
	$layout = apply_filters( 'scwd_main_layout', $layout );

	// Sanitize layout => Can't be empty!!
	$layout = $layout ? $layout : 'full-width';

	// Return layout
	return $layout;

}

/**
 * Returns default content layout.
 *
 * @since 1.0
 */
function scwd_get_default_content_area_layout() {
	$default = scwd_get_mod( 'content_layout' );
	return $default ? $default : 'right-sidebar';
}

/**
 * Returns correct content area layout.
 *
 * @since 4.0
 */
function scwd_content_area_layout( $post_id = '' ) {

	if ( ! empty( $_GET['post_layout'] ) ) {
		return esc_html( $_GET['post_layout'] );
	}

	$default  = scwd_get_default_content_area_layout();
	$class    = $default;
	$post_id  = $post_id ? $post_id : scwd_get_current_post_id();
	$instance = '';

	// Singular checks // Must use the post_id check to prevent issues
	// with custom pages like Events Calendar, 404 page, etc
	if ( $post_id ) {

		// Check meta first to override and return (prevents filters from overriding meta)
		if ( $post_id && $meta = get_post_meta( $post_id, 'scwd_post_layout', true ) ) {
			return $meta;
		}

		// Get post type
		$post_type = get_post_type();
		$instance  = 'singular_' . $post_type;

		// Singular Page
		if ( 'page' == $post_type ) {

			// Get page layout setting value
			$class = scwd_get_mod( 'page_single_layout' );

			// Get page template layouts
			if ( $page_template = get_page_template_slug( $post_id ) ) {

				// Blog template
				if ( $page_template == 'templates/blog.php' ) {
					$class = scwd_get_mod( 'blog_archives_layout' );
				}

				// Landing Page
				elseif ( $page_template == 'templates/landing-page.php' ) {
					$class = 'full-width';
				}

				// No Sidebar
				elseif ( $page_template == 'templates/no-sidebar.php' ) {
					$class = 'full-width';
				}

				// Left Sidebar
				elseif ( $page_template == 'templates/left-sidebar.php' ) {
					$class = 'left-sidebar';
				}

				// Right Sidebar
				elseif ( $page_template == 'templates/right-sidebar.php' ) {
					$class = 'right-sidebar';
				}

			}

		}

		// Singular Post
		elseif ( 'post' == $post_type ) {
			$class = scwd_get_mod( 'blog_single_layout' );
		}

		// Attachment
		elseif ( 'attachment' == $post_type ) {
			$class = 'full-width';
		}

		// Templatera
		elseif ( 'templatera' == $post_type ) {
			return 'full-width'; // Always return full-width
		}

		// Elementor
		elseif( 'elementor_library' == $post_type ) {
			return 'full-width'; // Always return full-width
		}

	} // End singular

	// 404 page => must check before archives due to WP bug with pagination
	elseif ( is_404() ) {
		$instance = '404';
		if ( ! scwd_get_mod( 'error_page_content_id' ) ) {
			$class = 'full-width';
		}
	}

	// Home
	elseif ( is_home() ) {
		$instance = 'home';
		$class = scwd_get_mod( 'blog_archives_layout' );
	}

	// Search => MUST BE BEFORE TAX CHECK, WP returns true for is_tax on search results
	elseif ( is_search() ) {
		$instance = 'search';
		$class = get_theme_mod( 'search_layout' );
	}

	// Define tax instance
	elseif ( is_tax() ) {
		$instance = 'tax'; // Used for filter - do NOT remove!!
	}

	// Define post type archive instance
	elseif( is_post_type_archive() ) {
		$instance = 'post_type_archive';
	}

	// Blog Query => Must come before category check
	elseif ( scwd_is_blog_query() ) {
		$instance = 'scwd_is_blog_query';
		$class = scwd_get_mod( 'blog_archives_layout' );

		// Extra check for categories with custom meta
		if ( is_category() ) {
			$instance = 'category';
			$class = scwd_get_mod( 'blog_archives_layout' );
			$term  = get_query_var( 'cat' );
			if ( $term_data = get_option( "category_$term" ) ) {
				if ( ! empty( $term_data['scwd_term_layout'] ) ) {
					$class = $term_data['scwd_term_layout'];
				}
			}
		}

	}

	// All else
	else {
		$class = $default;
	}

	// Apply filters
	$class = apply_filters( 'scwd_post_layout_class', $class, $instance );

	// Class should never be empty
	if ( empty( $class ) ) {
		$class = $default;
	}

	// Apply filters and return
	return $class;

}

/**
 * Returns the correct sidebar ID.
 *
 * @since  1.0.0
 */
function scwd_get_sidebar( $sidebar = '', $post_id = '' ) {
	$instance    = '';
	$is_singular = is_singular();
	$type        = $is_singular ? get_post_type() : '';
	$fallback    = apply_filters( 'scwd_sidebar_has_fallback', true );
	$sidebar     = ( ! $sidebar && $fallback ) ? 'sidebar' : $sidebar;

	// Page Sidebar
	if ( $is_singular ) {

		$instance = 'singular_' . $type;

		// Pages
		if ( 'page' == $type ) {

			if ( scwd_get_mod( 'pages_custom_sidebar', true ) && ! is_page_template( 'templates/blog.php' ) ) {
				$sidebar = 'pages_sidebar';
			}

		}

		// Posts
		elseif ( 'post' == $type ) {

			if ( scwd_get_mod( 'blog_custom_sidebar', false ) ) {
				$sidebar = 'blog_sidebar';
			}

		}

	// Archives
	} else {

		$instance = 'archive';

		// Search Sidebar
		if ( is_search() ) {
			$instance = 'search';
			if ( scwd_get_mod( 'search_custom_sidebar', true ) ) {
				$sidebar = 'search_sidebar';
			}
		}

		// Blog sidebar
		elseif ( scwd_get_mod( 'blog_custom_sidebar', false ) && scwd_is_blog_query() ) {
			$instance = 'scwd_is_blog_query';
			$sidebar = 'blog_sidebar';
		}

		// 404
		elseif ( is_404() ) {
			$instance = '404';
			if ( scwd_get_mod( 'pages_custom_sidebar', true ) ) {
				$sidebar = 'pages_sidebar';
			}
		}

	}

	// WooCommerce sidebar
	if ( function_exists( 'is_woocommerce' ) &&
		scwd_get_mod( 'woo_custom_sidebar', true )
		&& is_woocommerce()
	) {
		$sidebar = 'woo_sidebar';
	}

	/***
	 * FILTER    => Add filter for tweaking the sidebar display via child theme's
	 * IMPORTANT => Must be added before meta options so that it doesn't take priority
	 ***/
	$sidebar = apply_filters( 'scwd_get_sidebar', $sidebar, $instance );

	// Get current post id
	$post_id = $post_id ? $post_id : scwd_get_current_post_id();

	// Check meta option after filter so it always overrides
	if ( $meta = get_post_meta( $post_id, 'sidebar', true ) ) {
		$sidebar = $meta;
	}

	// Check term meta after filter so it always overrides
	// get_term_meta introduced in WP 4.4.0
	if ( function_exists( 'get_term_meta' ) ) {

		$meta_sidebar = '';

		if ( $is_singular ) {

			if ( 'page' != $type ) {

				$meta = '';
				$taxonomies = get_object_taxonomies( $type );

				foreach( $taxonomies as $taxonomy ) {
					if ( $meta ) break; // stop loop we found a custom sidebar
					$terms = get_the_terms( get_the_ID(), $taxonomy );
					if ( $terms ) {
						foreach ( $terms as $term ) {
							if ( $meta ) break; // stop loop we found a custom sidebar
							$meta = get_term_meta( $term->term_id, 'scwd_sidebar', true );
						}
					}
				}

				if ( $meta ) {
					$meta_sidebar = $meta;
				}

			}

		}

		// Taxonomies
		elseif ( is_tax() || is_category() || is_tag() ) {
			$term_id = get_queried_object()->term_id;
			if ( $term_id && $meta = get_term_meta( $term_id, 'scwd_sidebar', true ) ) {
				$meta_sidebar = $meta;
			}
		}

		// Check if taxonomy sidebar is active and exits and if not fallback to filter
		global $wp_registered_sidebars;
		if ( is_array( $wp_registered_sidebars )
			&& array_key_exists( $meta_sidebar, $wp_registered_sidebars )
			&& is_active_sidebar( $meta_sidebar )
		) {
			$sidebar = $meta_sidebar;
		}

	}

	// Never show empty sidebar
	if ( $sidebar && $fallback && ! is_active_sidebar( $sidebar ) ) {
		$sidebar = 'sidebar';
	}

	// Return the correct sidebar
	return $sidebar;

}

/**
 * Returns the correct classname for any specific column grid.
 *
 * @since 1.0.0
 */
function scwd_grid_class( $col = '4' ) {
	return apply_filters( 'scwd_grid_class', 'span_1_of_'. $col );
}

/**
 * Returns correct Google Fonts URL if you want to change it to another CDN.
 * such as the one in for China.
 *
 * https://chineseseoshifu.com/blog/google-fonts-instable-in-china.html
 *
 * @since 3.3.2
 */
function scwd_get_google_fonts_url() {
	return esc_url( apply_filters( 'scwd_get_google_fonts_url', '//fonts.googleapis.com' ) );
}

/*-------------------------------------------------------------------------------*/
/* [ Sanitize Data ]
/*-------------------------------------------------------------------------------*/

/**
 * Sanitize data via the TotalTheme\SanitizeData class.
 *
 * @since 2.0.0
 */
function scwd_sanitize_data( $data = '', $type = '' ) {
	if ( $data && $type ) {
		$class = new SCWDTheme\SanitizeData();
		return $class->parse_data( $data, $type );
	}
}

/**
 * Validate Boolean.
 *
 * @since 4.9
 */
function scwd_validate_boolean( $var ) {
	if ( is_bool( $var ) ) {
		return $var;
	}
	if ( is_string( $var ) ) {
		$var = strtolower( $var );
		if ( in_array( $var, array( 'false', 'off', 'disabled' ) ) ) {
			return false;
		}
		if ( in_array( $var, array( 'true', 'on', 'enabled' ) ) ) {
			return true;
		}
	}
	return (bool) $var;
}

/*-------------------------------------------------------------------------------*/
/* [ Parse HTML ]
/*-------------------------------------------------------------------------------*/

/**
 * Takes an array of attributes and outputs them for HTML.
 *
 * @since 3.4.0
 */
function scwd_parse_html( $tag = '', $attrs = array(), $content = '' ) {
	$attrs = scwd_parse_attrs( $attrs );
	$tag   = trim( wp_strip_all_tags( $tag ) );
	$output = '<' . $tag . ' ' . $attrs . '>';
	if ( $content ) {
		$output .= $content;
	}
	$output .= '</' . $tag . '>';
	return $output;
}

/**
 * Parses an html data attribute.
 *
 * @since 3.4.0
 */
function scwd_parse_attrs( $attrs = null ) {

	if ( ! $attrs || ! is_array( $attrs ) ) {
		return $attrs;
	}

	// Define output
	$output = '';

	// Loop through attributes
	foreach ( $attrs as $key => $val ) {

		// Attributes used for other things, we can skip these
		if ( 'content' == $key ) {
			continue;
		}

		// If the attribute is an array convert to string
		if ( is_array( $val ) ) {
			$val = array_filter( $val, 'trim' ); // Remove extra space
			$val = implode( ' ', $val );
		}

		// Sanitize rel attribute
		if ( 'rel' == $key && 'nofollow' != $val ) {
			continue;
		}

		// Sanitize ID
		if ( 'id' == $key ) {
			$val = trim ( str_replace( '#', '', $val ) );
			$val = str_replace( ' ', '', $val );
		}

		// Sanitize targets
		if ( 'target' == $key ) {
			$val = ( strpos( $val, 'blank' ) !== false ) ? '_blank' : '';
		}

		// Add attribute to output
		if ( $val ) {
			if ( in_array( $key, array( 'download' ) ) ) {
				$output .= ' ' . trim( $val ); // Used for example on total button download attribute
			} else {
				$needle = ( 'data' == $key ) ? 'data-' : $key . '=';
				if ( strpos( $val, $needle ) !== false ) {
					$output .= ' ' . trim( $val ); // Already has tag added
				} else {
					if ( 'data-scwd-hover' == $key ) {
						$output .= " " . $key . "='" . $val . "'";
					} else {
						$output .= ' ' . $key . '="' . $val . '"';
					}
				}
			}
		}

		// Items with empty vals
		else {

			// Empty alts are allowed
			if ( 'alt' == $key ) {
				$output .= " alt='" . esc_attr( $val ) . "'";
			}

			// Data attributes
			elseif ( strpos( $key, 'data-' ) !== false ) {
				$output .= ' ' . $key;
			}

		}

	}

	// Return output
	return trim( $output ); // leave space in front-end

}

/*-------------------------------------------------------------------------------*/
/* [ Content Blocks ( Entrys & Posts ) ]
/*-------------------------------------------------------------------------------*/

/**
 * Returns array of blocks for the entry post type layout.
 *
 * @since 3.2.0
 */
function scwd_entry_blocks() {
	$blocks = array(
		'media'    => 'media',
		'title'    => 'title',
		'meta'     => 'meta',
		'content'  => 'content',
		'readmore' => 'readmore',
	);
	$type   = get_post_type();
	$blocks = apply_filters( 'wpex_' . $type . '_entry_blocks', $blocks, $type );
	$blocks = apply_filters( 'wpex_entry_blocks', $blocks, $type );
	return $blocks;
}

/**
 * Returns array of blocks for the single post type layout.
 *
 * @since 3.2.0
 * @todo Update so all post types pass through the wpex_single_blocks filter. And update files so all post types use the wpex_single_blocks function.
 */
function scwd_single_blocks( $post_type = '' ) {

	// Define empty blocks array
	$blocks = array();

	// Get type
	$type = $post_type ? $post_type : get_post_type();

	// Get correct blocks by post type
	if ( 'page' == $type ) {
		$blocks = scwd_get_mod( 'page_composer', array( 'content' ) );
	} elseif ( 'elementor_library' == $type ) {
		return array( 'content' );
	} elseif( 'post' == $type ) {
		return scwd_blog_single_layout_blocks();
	} elseif ( 'portfolio' == $type ) {
		if ( function_exists( 'scwd_portfolio_single_blocks' ) ) {
			return scwd_portfolio_single_blocks();
		}
	} elseif ( 'staff' == $type ) {
		if ( function_exists( 'scwd_staff_single_blocks' ) ) {
			return scwd_staff_single_blocks();
		}
	} elseif ( 'testimonials' == $type ) {
		if ( function_exists( 'scwd_testimonials_single_blocks' ) ) {
			return scwd_testimonials_single_blocks();
		}
	} else {
		$blocks = array( 'media', 'title', 'meta', 'post-series', 'content', 'page-links', 'share', 'comments' );
	}

	// Convert to array if not already (for customizer settings)
	if ( ! is_array( $blocks ) ) {
		$blocks = explode( ',', $blocks );
	}

	// Set keys equal to values for easier filter removal
	// MUST RUN BEFORE FILTERS !!!
	$blocks = $blocks ? array_combine( $blocks, $blocks ) : array();

	// Type specific filter //@todo remove extra filter and update snippets/docs
	// This one is deprecated used filter defined below
	$blocks = apply_filters( 'scwd_' . $type . '_single_blocks', $blocks, $type );

	// Needed because of plugins using archives such as bbPress - @todo deprecate previouos filter?
	$blocks = apply_filters( 'scwd_single_blocks', $blocks, $type );

	// Sanitize & return blocks
	return $blocks;

}

/**
 * Returns array of blocks for the entry meta.
 *
 * @since 3.6.0
 */
function scwd_meta_blocks() {
	return apply_filters( 'scwd_meta_blocks', array( 'date', 'author', 'categories', 'comments' ), get_post_type() );
}

/**
 * Outputs target attribute.
 *
 * @since 4.9
 */
function scwd_parse_link_target( $echo = true ) {
	if ( 'blank' == $target || '_blank' == $target ) {
		if ( $echo ) {
			echo ' target="_blank"';
		} else {
			return ' target="_blank"';
		}
	}
}

/*-------------------------------------------------------------------------------*/
/* [ Taxonomy & Terms ]
/*-------------------------------------------------------------------------------*/
/**
 * Returns the "category" taxonomy for a given post type.
 *
 * @since 1.0.0
 */
function scwd_get_post_type_cat_tax( $post_type = '' ) {
	$post_type = $post_type ? $post_type : get_post_type();
	if ( 'post' == $post_type ) {
		$tax = 'category';
	} elseif ( 'portfolio' == $post_type ) {
		$tax = 'portfolio_category';
	} elseif ( 'staff' == $post_type ) {
		$tax = 'staff_category';
	} elseif ( 'testimonials' == $post_type ) {
		$tax = 'testimonials_category';
	} elseif ( 'product' == $post_type ) {
		$tax = 'product_cat';
	} elseif ( 'tribe_events' == $post_type ) {
		$tax = 'tribe_events_cat';
	} elseif ( 'download' == $post_type ) {
		$tax = 'download_category';
	} else {
		$tax = false;
	}
	return apply_filters( 'scwd_get_post_type_cat_tax', $tax, $post_type );
}

/*-------------------------------------------------------------------------------*/
/* [ Other ]
/*-------------------------------------------------------------------------------*/

/**
 * Check if the header supports aside content.
 *
 * @since 3.2.0
 */
function scwd_disable_google_services() {
	// return apply_filters( 'wpex_disable_google_services', wpex_get_mod( 'disable_gs', false ) );
	return false;
}

/**
 * Minify CSS.
 *
 * @since 1.6.3
 */
function scwd_minify_css( $css = '' ) {

	// Return if no CSS
	if ( ! $css ) return;

	// Normalize whitespace
	$css = preg_replace( '/\s+/', ' ', $css );

	// Remove ; before }
	$css = preg_replace( '/;(?=\s*})/', '', $css );

	// Remove space after , : ; { } */ >
	$css = preg_replace( '/(,|:|;|\{|}|\*\/|>) /', '$1', $css );

	// Remove space before , ; { }
	$css = preg_replace( '/ (,|;|\{|})/', '$1', $css );

	// Strips leading 0 on decimal values (converts 0.5px into .5px)
	$css = preg_replace( '/(:| )0\.([0-9]+)(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}.${2}${3}', $css );

	// Strips units if value is 0 (converts 0px to 0)
	$css = preg_replace( '/(:| )(\.?)0(%|em|ex|px|in|cm|mm|pt|pc)/i', '${1}0', $css );

	// Trim
	$css = trim( $css );

	// Return minified CSS
	return $css;

}