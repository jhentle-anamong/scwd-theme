<?php
/**
 * Gets and stores all theme mods for use with the theme.
 *
 * @package Silver_Connect_Web
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Define global $scwd_theme_mods
global $scwd_theme_mods;

// Gets all theme mods and stores them in the global $scwd_theme_mods variable to limit DB requests & filter checks
$scwd_theme_mods = get_theme_mods();

/**
 * Returns global mods
 */
function scwd_get_mods() {
	global $scwd_theme_mods;
	return $scwd_theme_mods;
}

/**
 * Returns theme mod from global var
 */
function scwd_get_mod( $id, $default = '', $not_empty = false ) {

	// Return get_theme_mod on customize_preview => IMPORTANT !!!
	if ( is_customize_preview() ) {
		$value = get_theme_mod( $id, $default );
		$value = ( $not_empty && ! $value ) ? $default : $value;
		return $value;
	}

	// Get global array of all theme mods
	global $scwd_theme_mods;

	// Return data from global array
	if ( ! empty( $scwd_theme_mods ) ) {

		// Check if mod is in array
		if ( isset( $scwd_theme_mods[$id] ) ) {
			$value = $scwd_theme_mods[$id];
		}

		// Set value to default if mod not in array
		else {
			$value = $default;
		}

		// Check if value can be empty if not set value to default
		$value = ( $not_empty && ! $value ) ? $default : $value;

		// Return value
		return $value;

	}

	// Global arary not found return using get_theme_mod
	else {

		$value = get_theme_mod( $id, $default );
		$value = ( $not_empty && ! $value ) ? $default : $value;
		return $value;

	}

}