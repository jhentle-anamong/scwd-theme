<?php
/**
 * Customizer Templates Select Control
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

if ( ! class_exists( 'SCWD_Customizer_Dropdown_Templates' ) ) {

	class SCWD_Customizer_Dropdown_Templates extends WP_Customize_Control {

		/**
		 * The control type.
		 *
		 * @access public
		 * @var string
		 */
		public $type = 'scwd-dropdown-templates';

		/**
		 * Render the content
		 *
		 * @access public
		 */
		public function render_content() {
			$value = $this->value(); ?>

			<label class="customize-control-select">

			<?php if ( ! empty( $this->label ) ) : ?>

				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

			<?php endif;

			// Description
			if ( ! empty( $this->description ) ) { ?>
				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>
			<?php } ?>

			<div class="scwd-customizer-chosen-select">

				<select <?php $this->link(); ?>>

					<option value="" <?php if ( empty( $val ) ) echo 'selected="selected"'; ?>><?php esc_html_e( 'Select', 'scwd' ); ?></option>

					<?php
					$templates  = new WP_Query( array(
						'posts_per_page' => -1,
						'post_type'      => apply_filters( 'scwd_singular_template_supported_post_types', array( 'templatera', 'elementor_library' ) ),
						'fields'         => 'ids',
					) );

					$templates = $templates->posts;

					if ( $templates ) {

						foreach ( $templates as $template ) {

							echo '<option value="' . esc_attr( $template ) . '"' . selected( $value, $template, false ) . '>' . wp_strip_all_tags( get_the_title( $template ) ) . '</option>';

						}

					} ?>

				</select>

			</div>

		<?php }
	}

}