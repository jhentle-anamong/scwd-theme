<?php
/**
 * Customizer Pages Select Control
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

if ( ! class_exists( 'SCWD_Customizer_Dropdown_Pages' ) ) {

	class SCWD_Customizer_Dropdown_Pages extends WP_Customize_Control {

		public $type = 'scwd-dropdown-pages';
		public $include_templates = false;

		/**
		 * Render the content
		 *
		 * @access public
		 */
		public function render_content() { ?>

			<label class="customize-control-select">

			<?php if ( ! empty( $this->label ) ) { ?>

				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

			<?php } ?>

			<?php
			// Description
			if ( ! empty( $this->description ) ) { ?>

				<span class="description customize-control-description"><?php echo esc_html( $this->description ); ?></span>

			<?php } ?>

			<div class="scwd-customizer-chosen-select">

				<?php if ( $this->include_templates && post_type_exists( 'templatera' ) ) { ?>

					<select <?php $this->link(); ?>>

						<option value="">- <?php esc_html_e( 'Select', 'scwd' ); ?> -</option>

						<?php
						// Add Templatera templates
						$templatera_templates = new WP_Query( array(
							'posts_per_page' => -1,
							'post_type'      => 'templatera',
						) );
						if ( $templatera_templates->have_posts() ) { ?>

							<optgroup label="<?php esc_html_e( 'Templatera', 'scwd' ); ?>">

								<?php while ( $templatera_templates->have_posts() ) {

									$templatera_templates->the_post();

									echo '<option value="' . absint( get_the_ID() ) . '"' . selected( $this->value(), get_the_ID(), false ) . '>' . esc_html( get_the_title() ) . '</option>';

								}
								wp_reset_postdata(); ?>

							</optgroup>

						<?php } ?>

						<optgroup label="<?php esc_html_e( 'Pages', 'scwd' ); ?>">
							<?php
							$pages = get_pages( array(
								'exclude' => get_option( 'page_on_front' ),
							) );
							if ( $pages ) {
								foreach ( $pages as $page ) {
									echo '<option value="' . absint( $page->ID ) . '"' . selected( $this->value(), $page->ID, false ) . '>' . esc_html( $page->post_title ) . '</option>';
								}
							} ?>
						</optgroup>

					</select>

				<?php } else {

					$dropdown = wp_dropdown_pages( array(
						'name'              => '_customize-dropdown-pages-' . $this->id,
						'echo'              => 0,
						'show_option_none'  => '&mdash; ' . esc_html__( 'Select', 'scwd' ) . ' &mdash;',
						'option_none_value' => '0',
						'selected'          => $this->value(),
					) );

					// Hackily add in the data link parameter.
					echo str_replace( '<select', '<select ' . $this->get_link(), $dropdown );

				} ?>

			</div>

		<?php }
	}

}