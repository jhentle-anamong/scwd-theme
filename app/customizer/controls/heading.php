<?php
/**
 * Customizer Heading Control
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'SCWD_Customizer_Heading_Control' ) ) {

	class SCWD_Customizer_Heading_Control extends WP_Customize_Control {

		/**
		 * The control type
		 *
		 * @access public
		 * @var string
		 */
		public $type = 'scwd-heading';

		/**
		 * Don't render the control content from PHP, as it's rendered via JS on load.
		 *
		 * @since 3.6.0
		 */
		public function render_content() {}

		/**
		 * The control template
		 *
		 * @since 3.6.0
		 */
		public function content_template() { ?>

			<# if ( data.label ) { #>
				<span class="scwd-customizer-heading">{{ data.label }}</span>
			<# } #>

		<?php }

	}

}