<?php
/**
 * Customizer Patterns Control
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'SCWD_Customizer_BG_Patterns_Control' ) ) {

	class SCWD_Customizer_BG_Patterns_Control extends WP_Customize_Control {

		/**
		 * The control type
		 *
		 * @access public
		 * @var string
		 */
		public $type = 'scwd-bg-patterns';

		/**
		 * The control template
		 *
		 * @since 4.0
		 */
		public function render_content() {

			$this_val = $this->value(); ?>

			<label class="customize-control-select">

				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

				<select <?php $this->link(); ?>>
					<option value="" <?php selected( $this_val, '' ); ?>><?php esc_html_e( 'None', 'scwd' ); ?></option>
					<?php if ( $patterns = scwd_get_background_patterns() ) {
						foreach ( $patterns as $key => $val ) { ?>
							<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $this_val, $key ); ?>><?php echo esc_html( $val['label'] ); ?></option>
						<?php }
					} ?>
				</select>

			</label>

		<?php }

	}

}