<?php
/**
 * Header Customizer Options
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*-----------------------------------------------------------------------------------*/
/* - Header => General
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_header_general'] = array(
	'title' => esc_html__( 'General', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'enable_header',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Enable', 'scwd' ),
				'type' => 'checkbox',
			),
		),
		array(
			'id' => 'full_width_header',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Full-Width', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'main_layout_style',
				'value' => 'full-width',
			),
		),
		array(
			'id' => 'header_style',
			'default' => 'one',
			'control' => array(
				'label' => esc_html__( 'Style', 'scwd' ),
				'type' => 'select',
				'choices' => scwd_get_header_styles(),
			),
		),
		array(
			'id' => 'vertical_header_style',
			'transport' => 'postMessage',
			'default' => '',
			'control' => array(
				'label' => esc_html__( 'Vertical Header Style', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'' => esc_html__( 'Default', 'scwd' ),
					'fixed' => esc_html__( 'Fixed', 'scwd' ),
				),
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => array( 'six', 'vertical-2' ),
			),
		),
		array(
			'id' => 'vertical_header_width',
			'transport' => 'refresh',
			'default' => '',
			'control' => array(
				'label' => esc_html__( 'Vertical Header Width (in pixels)', 'scwd' ),
				'type' => 'text',
				'description' => esc_html__( 'Default', 'scwd' ) . ' : 280px',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => array( 'six', 'vertical-2' ),
			),
		),
		array(
			'id' => 'header_top_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Top Padding', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => array(
					'#site-header #site-header-inner',
					'#site-header.overlay-header #site-header-inner',
				),
				'alter' => 'padding-top',
				'sanitize' => 'px',
			),
		),
		array(
			'id' => 'header_bottom_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Bottom Padding', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => array(
					'#site-header-inner',
					'#site-header.overlay-header #site-header-inner',
				),
				'alter' => 'padding-bottom',
				'sanitize' => 'px',
			),
		),
		array(
			'id' => 'header_background',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Background', 'scwd' ),
				'type' => 'color',
			),
			'inline_css' => array(
				'target' => array(
					'#site-header',
					'#site-header-sticky-wrapper',
					'#site-header-sticky-wrapper.is-sticky #site-header',
					'.footer-has-reveal #site-header',
					'#searchform-header-replace',
					'body.scwd-has-vertical-header #site-header',
				),
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'header_background_image',
			'control' => array(
				'type' => 'media',
				'mime_type' => 'image',
				'label' => esc_html__( 'Background Image', 'scwd' ),
			),
		),
		array(
			'id' => 'header_background_image_style',
			'default' => '',
			'control' => array(
				'label' => esc_html__( 'Background Image Style', 'scwd' ),
				'type'  => 'select',
				'choices' => $bg_styles,
			),
		),
		/*** Aside ***/
		array(
			'id' => 'header_aside_heading',
			'control' => array(
				'type' => 'scwd-heading',
				'label' => esc_html__( 'Aside', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => array( 'two', 'three', 'four' ),
			),
		),
		array(
			'id' => 'header_aside_visibility',
			'transport' => 'postMessage',
			'default' => 'visible-desktop',
			'control' => array(
				'label' => esc_html__( 'Visibility', 'scwd' ),
				'type' => 'select',
				'choices' => $choices_visibility,
			),
			'control_display' => array(
				'check'      => 'header_style',
				'value'      => array( 'two', 'three', 'four' ),
			),
		),
		array(
			'id' => 'header_flex_items',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Vertical Align Aside Content', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => 'two',
			),
		),
		array(
			'id' => 'header_aside_search',
			'transport' => 'partialRefresh',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Header Aside Search', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => 'two',
			),
		),
		array(
			'id' => 'header_aside',
			'transport' => 'partialRefresh',
			'control' => array(
				'label' => esc_html__( 'Header Aside Content', 'scwd' ),
				'type' => 'textarea',
				'description' => $post_id_content_desc,
			),
			'control_display' => array(
				'check'      => 'header_style',
				'value'      => array( 'two', 'three', 'four' ),
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Logo
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_header_logo'] = array(
	'title' => esc_html__( 'Logo', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'logo_top_margin',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Top Margin', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '#site-logo',
				'alter' => 'padding-top',
				'sanitize' => 'px',
			),
		),
		array(
			'id' => 'logo_bottom_margin',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Bottom Margin', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '#site-logo',
				'alter' => 'padding-bottom',
				'sanitize' => 'px',
			),
		),
		array(
			'id' => 'logo_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
				'active_callback' => 'scwd_cac_hasnt_custom_logo',
			),
			'inline_css' => array(
				'target' => '#site-logo a.site-logo-text',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'logo_hover_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Hover Color', 'scwd' ),
				'active_callback' => 'scwd_cac_hasnt_custom_logo',
			),
			'inline_css' => array(
				'target' => '#site-logo a.site-logo-text:hover',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'custom_logo',
			'control' => array(
				'label' => esc_html__( 'Image Logo', 'scwd' ),
				'type' => 'media',
				'mime_type' => 'image'
			),
		),
		array(
			'id' => 'logo_height',
			'control' => array(
				'label' => esc_html__( 'Height', 'scwd' ),
				'type' => 'text',
				'description' => esc_html__( 'Used for retina and image height attribute tag.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_image_logo',
			),
		),
		array(
			'id' => 'apply_logo_height',
			'default' => false,
			'control' => array(
				'label' => esc_html__( 'Apply Height', 'scwd' ),
				'type' => 'checkbox',
				'description' => __( 'Check this box to apply your logo height to the image. Useful for displaying large logos at a smaller size. Note: If you have enabled the shrink sticky header style you need to alter your height value under the Sticky Header settings.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_image_logo',
			),
		),
		array(
			'id' => 'logo_width',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Width', 'scwd' ),
				'description' => esc_html__( 'Used for image width attribute tag.', 'scwd' ),
				'type' => 'text',
				'active_callback' => 'scwd_cac_has_image_logo',
			),
		),
		array(
			'id' => 'retina_logo',
			'default' => '',
			'control' => array(
				'label' => esc_html__( 'Retina Image Logo', 'scwd' ),
				'type' => 'media',
				'mime_type' => 'image',
				'active_callback' => 'scwd_cac_has_image_logo',
			),
		),
		array(
			'id' => 'logo_max_width',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Logo Max Width: Desktop', 'scwd' ),
				'type' => 'text',
				'description' => esc_html__( 'Screens 960px wide and greater.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_image_logo',
			),
			'inline_css' => array(
				'media_query' => '(min-width: 960px)',
				'target' => '#site-logo img',
				'alter' => 'max-width',
			),
		),
		array(
			'id' => 'logo_max_width_tablet_portrait',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Logo Max Width: Tablet Portrait', 'scwd' ),
				'type' => 'text',
				'description' => esc_html__( 'Screens 768px-959px wide.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_image_logo',
			),
			'inline_css' => array(
				'media_query' => '(min-width: 768px) and (max-width: 959px)',
				'target' => '#site-logo img',
				'alter' => 'max-width',
			),
		),
		array(
			'id' => 'logo_max_width_phone',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Logo Max Width: Phone', 'scwd' ),
				'type' => 'text',
				'description' => esc_html__( 'Screens smaller than 767px wide.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_image_logo',
			),
			'inline_css' => array(
				'media_query' => '(max-width: 767px)',
				'target' => '#site-logo img',
				'alter' => 'max-width',
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Logo Icon
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_header_logo_icon'] = array(
	'title' => esc_html__( 'Logo Icon', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'logo_icon',
			'transport' => 'postMessage',
			'default' => 'none',
			'control' => array(
				'label' => esc_html__( 'Icon Select', 'scwd' ),
				'type' => 'scwd-fa-icon-select',
				'active_callback' => 'scwd_cac_hasnt_custom_logo',
			),
		),
		array(
			'id' => 'logo_icon_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Logo Icon Color', 'scwd' ),
				'active_callback' => 'scwd_cac_hasnt_custom_logo',
			),
			'inline_css' => array(
				'target' => '#site-logo-fa-icon',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'logo_icon_right_margin',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Logo Icon Right Margin', 'scwd' ),
				'description' => $pixel_desc,
				'active_callback' => 'scwd_cac_hasnt_custom_logo',
			),
			'inline_css' => array(
				'target' => '#site-logo-fa-icon',
				'alter' => 'margin-right',
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Fixed On Scroll
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_header_fixed'] = array(
	'title' => esc_html__( 'Sticky Header', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'fixed_header_style',
			'transport' => 'refresh',
			'default' => 'standard',
			'sanitize_callback' => 'esc_html',
			'control' => array(
				'label' => esc_html__( 'Style', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'disabled' => esc_html__( 'Disabled', 'scwd' ),
					'standard' => esc_html__( 'Standard', 'scwd' ),
					'shrink' => esc_html__( 'Shrink', 'scwd' ),
					'shrink_animated' => esc_html__( 'CSS3 Animated Shrink (Best with Image Logo)', 'scwd' ),
				),
				'active_callback' => 'scwd_cac_header_supports_fixed_header',
			),
		),
		array(
			'id' => 'fixed_header_start_position',
			'sanitize_callback' => 'esc_html',
			'control' => array(
				'label' => esc_html__( 'Sticky Start Position', 'scwd' ),
				'type' => 'text',
				'active_callback' => 'scwd_cac_header_supports_fixed_header',
				'description' => esc_html__( 'By default, the header becomes sticky as soon as you reach the header while scrolling. You can use this field to enter a number (in pixels) to offset the point at which the header becomes sticky (based on the top of the page) or the classname or ID of another element so that the header becomes sticky when it reaches that point (example: #my-custom-div).', 'scwd' ),
			),
		),
		array(
			'id' => 'fixed_header_shrink_start_height',
			'sanitize_callback' => 'absint',
			'default' => 60,
			'control' => array(
				'label' => esc_html__( 'Logo Start Height', 'scwd' ),
				'type' => 'number',
				'description' => esc_html__( 'In order to properly animate the header with CSS3 it is important to apply a fixed height to the header logo by default.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_fixed_header_shrink',
			),
		),
		array(
			'id' => 'fixed_header_shrink_end_height',
			'default' => 50,
			'sanitize_callback' => 'absint',
			'control' => array(
				'label' => esc_html__( 'Logo Shrunk Height', 'scwd' ),
				'type' => 'number',
				'active_callback' => 'scwd_cac_has_fixed_header_shrink',
				'description' => esc_html__( 'Your shrink header height will be set to your Logo Shrunk Height plus 20px for a top and bottom padding of 10px.', 'scwd' ),
			),
		),
		array(
			'id' => 'fixed_header_mobile',
			'sanitize_callback' => 'esc_html',
			'control' => array(
				'label' => esc_html__( 'Mobile Support', 'scwd' ),
				'type' => 'checkbox',
				'active_callback' => 'scwd_cac_header_supports_fixed_header',
			),
		),
		array(
			'id' => 'fixed_header_opacity',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'number',
				'label' => esc_html__( 'Opacity', 'scwd' ),
				'active_callback' => 'scwd_cac_has_fixed_header',
				'input_attrs' => array(
					'min' => 0.1,
					'max' => 1,
					'step' => 0.1,
				),
			),
			'inline_css' => array(
				'target' => '.scwd-sticky-header-holder.is-sticky #site-header',
				'alter' => 'opacity',
			),
		),
		array(
			'id' => 'fixed_header_logo',
			'sanitize_callback' => 'absint',
			'control' => array(
				'label' => esc_html__( 'Custom Logo', 'scwd' ),
				'type' => 'media',
				'mime_type' => 'image',
				'active_callback' => 'scwd_cac_supports_fixed_header_logo',
				'description' => esc_html__( 'If this custom logo is a different size, for best results go to the Logo section and apply a custom height to your logo.', 'scwd' ),
			),
		),
		array(
			'id' => 'fixed_header_logo_retina',
			'sanitize_callback' => 'absint',
			'control' => array(
				'label' => esc_html__( 'Custom Logo Retina', 'scwd' ) .' '. esc_html__( 'Retina', 'scwd' ),
				'type' => 'media',
				'mime_type' => 'image',
				'active_callback' => 'scwd_cac_has_fixed_header_logo',
			),
		),
		array(
			'id' => 'fixed_header_logo_retina_height',
			'sanitize_callback' => 'absint',
			'control' => array(
				'label' => esc_html__( 'Custom Logo Retina Height', 'scwd' ),
				'type' => 'number',
				'active_callback' => 'scwd_supports_fixed_header_logo_retina_height',
			),
			'inline_css' => array(
				'target' => 'body.scwd-is-retina #site-header-sticky-wrapper.is-sticky #site-logo img',
				'alter' => 'height',
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Menu
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_header_menu'] = array(
	'title' => esc_html__( 'Menu', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'menu_arrow_down',
			'default' => false,
			'control' => array(
				'label' => esc_html__( 'Top Level Dropdown Icon', 'scwd' ),
				'type' => 'checkbox',
			),
		),
		array(
			'id' => 'menu_arrow_side',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Second+ Level Dropdown Icon', 'scwd' ),
				'type' => 'checkbox',
			),
		),
		array(
			'id' => 'header_menu_disable_borders',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Disable Menu Inner Borders', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => 'two',
			),
		),
		array(
			'id' => 'header_menu_center',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Center Menu Items', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => 'two',
			),
		),
		array(
			'id' => 'menu_dropdown_top_border',
			//'transport' => 'postMessage', // Can't cause it has dependent options
			'default' => false,
			'control' => array(
				'label' => esc_html__( 'Dropdown Top Border', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => array( 'one', 'two', 'three', 'four', 'five', 'six' ),
			),
		),
		array(
			'id' => 'menu_flush_dropdowns',
			'default' => false,
			'control' => array(
				'label' => esc_html__( 'Flush Dropdowns', 'scwd' ),
				'type' => 'checkbox',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => 'one',
			),
		),
		array(
			'id' => 'menu_dropdown_style',
			'transport' => 'postMessage',
			'default' => 'default',
			'control' => array(
				'label' => esc_html__( 'Dropdown Style', 'scwd' ),
				'type' => 'select',
				'choices' => scwd_get_menu_dropdown_styles(),
			),
		),
		array(
			'id' => 'menu_dropdown_dropshadow',
			'transport' => 'postMessage',
			'default' => '',
			'control' => array(
				'label' => esc_html__( 'Dropdown Dropshadow Style', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'' => esc_html__( 'None', 'scwd' ),
					'one' => esc_html__( 'One', 'scwd' ),
					'two' => esc_html__( 'Two', 'scwd' ),
					'three' => esc_html__( 'Three', 'scwd' ),
					'four' => esc_html__( 'Four', 'scwd' ),
					'five' => esc_html__( 'Five', 'scwd' ),
				),
			),
		),
		array(
			'id' => 'menu_li_left_margin',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Menu Items Left Margin', 'scwd' ),
				'description' => esc_html__( 'Can be used to increase the spacing between your items. Value in pixels.', 'scwd' ),
			),
			'inline_css' => array(
				'target' => 'body .navbar-style-one .dropdown-menu > li.menu-item',
				'alter' => 'margin-left',
				'sanitize' => 'px',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => 'one',
			),
		),
		array(
			'id' => 'menu_a_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Menu Items Left/Right Padding', 'scwd' ),
				'description' => esc_html__( 'Can be used to increase the spacing between your items. Value in pixels.', 'scwd' ),
			),
			'inline_css' => array(
				'target' => array(
					'body .navbar-style-two .dropdown-menu > li.menu-item > a',
					'body .navbar-style-three .dropdown-menu > li.menu-item > a',
					'body .navbar-style-four .dropdown-menu > li.menu-item > a',
					'body .navbar-style-five .dropdown-menu > li.menu-item > a',
				),
				'alter' => array( 'padding-left', 'padding-right' ),
				'sanitize' => 'px',
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => array( 'two', 'three', 'four', 'five' ),
			),
		),

		/*** Main Styling ***/
		array(
			'id' => 'menu_main_styling_heading',
			'control' => array(
				'type' => 'scwd-heading',
				'label' => esc_html__( 'Styling: Main', 'scwd' ),
			),
		),
		array(
			'id' => 'menu_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
			),
			'inline_css' => array(
				'target' => array(
					'#site-navigation-wrap',
					'#site-navigation-sticky-wrapper.is-sticky #site-navigation-wrap',
				),
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'menu_borders',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Borders', 'scwd' ),
				'description' => esc_html__( 'Not all menus have borders, but this setting is for those that do', 'scwd' ),
			),
			'inline_css' => array(
				'target' => array(
					'#site-navigation > ul li.menu-item',
					'#site-navigation a',
					'#site-navigation ul',
					'#site-navigation-wrap',
					'#site-navigation',
					'.navbar-style-six #site-navigation',
					'#site-navigation-sticky-wrapper.is-sticky #site-navigation-wrap',
				),
				'alter' => 'border-color',
			),
		),
		// Menu Link Colors
		array(
			'id' => 'menu_link_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Color', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item > a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'menu_link_color_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Color: Hover', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item > a:hover,
							#site-navigation .dropdown-menu > li.menu-item.dropdown.sfHover > a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'menu_link_color_active',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Color: Current Menu Item', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item.current-menu-item > a,
							#site-navigation .dropdown-menu > li.menu-item.current-menu-parent > a',
				'alter' => 'color',
				//'important' => true, // removed in 4.4.1 - causes issues with superfish hover settings
			),
		),
		// Link Background
		array(
			'id' => 'menu_link_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Background', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item > a',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'menu_link_hover_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Background: Hover', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item > a:hover,
							#site-navigation .dropdown-menu > li.menu-item.dropdown.sfHover > a',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'menu_link_active_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Background: Current Menu Item', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item.current-menu-item > a,
							#site-navigation .dropdown-menu > li.menu-item.current-menu-parent > a',
				'alter' => 'background-color',
			),
		),
		// Link Inner
		array(
			'id' => 'menu_link_span_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Inner Background', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item > a > span.link-inner',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'menu_link_span_hover_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Inner Background: Hover', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item > a:hover > span.link-inner,
							#site-navigation .dropdown-menu > li.menu-item.dropdown.sfHover > a > span.link-inner',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'menu_link_span_active_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Inner Background: Current Menu Item', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-navigation .dropdown-menu > li.menu-item.current-menu-item > a > span.link-inner,
							#site-navigation .dropdown-menu > li.menu-item.current-menu-parent > a > span.link-inner',
				'alter' => 'background-color',
			),
		),

		/**** Dropdown Styling ****/
		array(
			'id' => 'menu_dropdowns_styling_heading',
			'control' => array(
				'type' => 'scwd-heading',
				'label' => esc_html__( 'Styling: Dropdowns', 'scwd' ),
			),
		),

		// Menu Dropdowns
		array(
			'id' => 'dropdown_menu_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .dropdown-menu ul.sub-menu',
				'alter' => 'background-color',
			),
		),
		// Pointer
		array(
			'id' => 'dropdown_menu_pointer_bg',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Pointer Background', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '.scwd-dropdowns-caret .dropdown-menu ul.sub-menu::after',
				'alter' => 'border-bottom-color',
			),
		),
		array(
			'id' => 'dropdown_menu_pointer_border',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Pointer Border', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '.scwd-dropdowns-caret .dropdown-menu ul.sub-menu::before',
				'alter' => 'border-bottom-color',
			),
		),
		// Borders
		array(
			'id' => 'dropdown_menu_borders',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Dropdown Borders', 'scwd' ),
			),
			'inline_css' => array(
				'target' => array(
					'#site-header #site-navigation .dropdown-menu ul.sub-menu',
					'#site-header #site-navigation .dropdown-menu ul.sub-menu li.menu-item',
					'#site-header #site-navigation .dropdown-menu ul.sub-menu li.menu-item a',
				),
				'alter' => 'border-color',
			),
		),
		array(
			'id' => 'menu_dropdown_top_border_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Top Border', 'scwd' ),
				'active_callback' => 'scwd_cac_has_menu_dropdown_top_border',
			),
			'inline_css' => array(
				'target' => array(
					'.scwd-dropdown-top-border #site-navigation .dropdown-menu li.menu-item ul.sub-menu',
					'#searchform-dropdown',
					'#current-shop-items-dropdown',
				),
				'alter' => 'border-top-color',
				'important' => true,
			),
		),
		// Link color
		array(
			'id' => 'dropdown_menu_link_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Color', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .dropdown-menu ul.sub-menu > li.menu-item > a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'dropdown_menu_link_color_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Color: Hover', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .dropdown-menu ul.sub-menu > li.menu-item > a:hover',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'dropdown_menu_link_hover_bg',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Background: Hover', 'scwd' ),
			),
			'subtitle' => esc_html__( 'Select your custom hex color.', 'scwd' ),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .dropdown-menu ul.sub-menu > li.menu-item > a:hover',
				'alter' => 'background-color',
			),
		),
		// Current item
		array(
			'id' => 'dropdown_menu_link_color_active',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Color: Current Menu Item', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .dropdown-menu ul.sub-menu > li.menu-item.current-menu-item > a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'dropdown_menu_link_bg_active',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Link Background: Current Menu Item', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .dropdown-menu ul.sub-menu > li.menu-item.current-menu-item > a',
				'alter' => 'background-color',
			),
		),
		// Mega menu
		array(
			'id' => 'mega_menu_title',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Megamenu Subtitle Color', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '#site-header #site-navigation .sf-menu > li.megamenu > ul.sub-menu > .menu-item-has-children > a',
				'alter' => 'color',
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Menu Search Form
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_menu_search'] = array(
	'title' => esc_html__( 'Menu Search', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'menu_search_style',
			'default' => 'drop_down',
			'control' => array(
				'label' => esc_html__( 'Style', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'disabled' => esc_html__( 'Disabled','scwd' ),
					'drop_down' => esc_html__( 'Drop Down','scwd' ),
					'overlay' => esc_html__( 'Site Overlay','scwd' ),
					'header_replace' => esc_html__( 'Header Replace','scwd' )
				),
				'description' => esc_html__( 'Vertical header styles only support the disabled and overlay styles.', 'scwd' ),
			),
		),
		array(
			'id' => 'search_dropdown_top_border',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Drop Down Top Border', 'scwd' ),
				'type' => 'color',
			),
			'inline_css' => array(
				'target' => '#searchform-dropdown',
				'alter' => 'border-top-color',
				'important' => true,
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Fixed Menu
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_fixed_menu'] = array(
	'title' => esc_html__( 'Sticky Menu', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		array(
			'id' => 'fixed_header_menu',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Sticky Header Menu', 'scwd' ),
				'type' => 'checkbox',
				'active_callback' => 'scwd_cac_header_supports_fixed_menu',
			),
		),
	)
);

/*-----------------------------------------------------------------------------------*/
/* - Header => Mobile Menu
/*-----------------------------------------------------------------------------------*/
$this->sections['scwd_header_mobile_menu'] = array(
	'title' => esc_html__( 'Mobile Menu', 'scwd' ),
	'panel' => 'scwd_header',
	'settings' => array(
		// Breakpoint
		array(
			'id' => 'mobile_menu_breakpoint',
			'control' => array(
				'label' => esc_html__( 'Mobile Menu Breakpoint', 'scwd' ),
				'type' => 'text',
				'desc' => esc_html__( 'Default:', 'scwd' ) .' 959px'
			),
			'control_display' => array(
				'check' => 'header_style',
				'value' => array( 'one', 'two', 'three', 'four', 'five', 'seven' ),
			),
		),
		// Search
		array(
			'id' => 'mobile_menu_search',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Mobile Menu Search', 'scwd' ),
				'type' => 'checkbox',
			),
		),
		/*** Mobile Menu > Toggle Style ***/
		array(
			'id' => 'mobile_menu_toggle_style',
			'default' => 'icon_buttons',
			'control' => array(
				'label' => esc_html__( 'Toggle Button Style', 'scwd' ),
				'type' => 'select',
				'active_callback' => 'scwd_cac_mobile_menu_toggle_style',
				'choices' => array(
					'icon_buttons' => esc_html__( 'Right Aligned Icon Button(s)', 'scwd' ),
					'icon_buttons_under_logo' => esc_html__( 'Under The Logo Icon Button(s)', 'scwd' ),
					'navbar' => esc_html__( 'Navbar', 'scwd' ),
					'fixed_top'  => esc_html__( 'Fixed Site Top', 'scwd' ),
					'custom'  => esc_html__( 'Custom', 'scwd' ),
				),
				'desc' => esc_html__( 'If you select "custom" the theme will load the needed code for your mobile menu which you can then open/close by adding any link to the page with the classname "mobile-menu-toggle".', 'scwd' )
			),
		),
		array(
			'id' => 'mobile_menu_navbar_position',
			'default' => 'scwd_hook_header_bottom',
			'control' => array(
				'label' => esc_html__( 'Menu Position', 'scwd' ),
				'type' => 'select',
				'active_callback' => 'scwd_cac_is_mobile_navbar',
				'choices' => array(
					'scwd_hook_header_bottom' => esc_html__( 'Header Bottom', 'scwd' ),
					'outer_wrap_before' => esc_html__( 'Top of site', 'scwd' ),
				),
			),
		),
		array(
			'id' => 'mobile_menu_toggle_fixed_top_bg',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Toggle Background', 'scwd' ),
				'type' => 'color',
				'active_callback' => 'scwd_cac_is_mobile_fixed_or_navbar',
			),
			'inline_css' => array(
				'target' => '#scwd-mobile-menu-fixed-top, #scwd-mobile-menu-navbar',
				'alter' => 'background',
			),
		),
		array(
			'id' => 'mobile_menu_toggle_text',
			'transport' => 'postMessage',
			'default' => esc_html__( 'Menu', 'scwd' ),
			'control' => array(
				'label' => esc_html__( 'Toggle Text', 'scwd' ),
				'type' => 'text',
				'active_callback' => 'scwd_cac_is_mobile_fixed_or_navbar',
			),
		),
		/*** Mobile Menu > Style */
		array(
			'id' => 'mobile_menu_style',
			'default' => 'sidr',
			'control' => array(
				'label' => esc_html__( 'Mobile Menu Style', 'scwd' ),
				'type' => 'select',
				'choices' => scwd_get_mobile_menu_styles(),
			),
		),
		array(
			'id' => 'full_screen_mobile_menu_style',
			'default' => 'white',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Style', 'scwd' ),
				'type' => 'select',
				'active_callback' => 'scwd_cac_mobile_menu_is_full_screen',
				'choices' => array(
					'white'	=> esc_html__( 'White', 'scwd' ),
					'black'	=> esc_html__( 'Black', 'scwd' ),
				),
			),
		),
		array(
			'id' => 'mobile_menu_sidr_direction',
			'default' => 'right',
			'control' => array(
				'label' => esc_html__( 'Direction', 'scwd' ),
				'type' => 'select',
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
				'choices' => array(
					'right'	=> esc_html__( 'Right', 'scwd' ),
					'left'	=> esc_html__( 'Left', 'scwd' ),
				),
			),
		),
		array(
			'id' => 'mobile_menu_sidr_displace',
			'default' => false,
			'control' => array(
				'label' => esc_html__( 'Displace', 'scwd' ),
				'type' => 'checkbox',
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
				'description' => esc_html__( 'Enable to display/push the site content over when opening the sidebar mobile menu.', 'scwd' ),
			),
		),
		/*** Mobile Menu > Mobile Icons Styling ***/
		array(
			'id' => 'mobile_menu_icons_styling',
			'control' => array(
				'type' => 'scwd-heading',
				'label' => esc_html__( 'Icons Styling', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
		),

		/* @deprecated in v4.5.5
		array(
			'id' => 'mobile_menu_icon_size',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Font Size', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '#mobile-menu a',
				'alter' => 'font-size',
				'sanitize' => 'px',
			),
		),*/

		array(
			'id' => 'mobile_menu_icon_color',
			'transport' => 'refresh',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
			'inline_css' => array(
				'target' => '#mobile-menu a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'mobile_menu_icon_color_hover',
			'transport' => 'refresh',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color: Hover', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
			'inline_css' => array(
				'target' => '#mobile-menu a:hover',
				'alter' => 'color',
			),
		),

		/* @deprecated in v4.5.5
		array(
			'id' => 'mobile_menu_icon_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
			'inline_css' => array(
				'target' => '#mobile-menu a',
				'alter' => 'background',
			),
		),
		array(
			'id' => 'mobile_menu_icon_background_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background: Hover', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
			'inline_css' => array(
				'target' => '#mobile-menu a:hover',
				'alter' => 'background',
			),
		),
		array(
			'id' => 'mobile_menu_icon_border',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Border', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
			'inline_css' => array(
				'target' => '#mobile-menu a',
				'alter' => 'border-color',
			),
		),
		array(
			'id' => 'mobile_menu_icon_border_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Border: Hover', 'scwd' ),
				'active_callback' => 'scwd_cac_has_mobile_menu_icons',
			),
			'inline_css' => array(
				'target' => '#mobile-menu a:hover',
				'alter' => 'border-color',
			),
		), */

		/*** Mobile Menu > Sidr ***/
		array(
			'id' => 'mobile_menu_sidr_styling',
			'control' => array(
				'type' => 'scwd-heading',
				'label' => esc_html__( 'Sidebar Menu Styling', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
			),
		),
		array(
			'id' => 'mobile_menu_sidr_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
			),
			'inline_css' => array(
				'target' => '#sidr-main',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'mobile_menu_sidr_borders',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Borders', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
			),
			'inline_css' => array(
				'target' => '#sidr-main li, #sidr-main ul, .sidr-class-mobile-menu-searchform input, .sidr-class-mobile-menu-searchform',
				'alter' => 'border-color',
			),
		),
		array(
			'id' => 'mobile_menu_links',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
			),
			'inline_css' => array(
				'target' => '#sidr-main,.sidr-class-scwd-close',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'mobile_menu_links_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Links: Hover', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_sidr',
			),
			'inline_css' => array(
				'target' => '.sidr a:hover,.sidr-class-menu-item-has-children.active > a',
				'alter' => 'color',
			),
		),

		/*** Mobile Menu > Toggle Menu ***/
		array(
			'id' => 'mobile_menu_toggle_styling',
			'control' => array(
				'type' => 'scwd-heading',
				'label' => esc_html__( 'Toggle Menu Styling', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_toggle',
			),
		),
		array(
			'id' => 'toggle_mobile_menu_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_toggle',
			),
			'inline_css' => array(
				'target' => array(
					'.mobile-toggle-nav',
					'.scwd-mobile-toggle-menu-fixed_top .mobile-toggle-nav',
				),
				'alter' => 'background',
			),
		),
		array(
			'id' => 'toggle_mobile_menu_borders',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Borders', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_toggle',
			),
			'inline_css' => array(
				'target' => array(
					'.mobile-toggle-nav a',
					'.scwd-mobile-toggle-menu-fixed_top .mobile-toggle-nav a',
				),
				'alter' => 'border-color',
			),
		),
		array(
			'id' => 'toggle_mobile_menu_links',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Links', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_toggle',
			),
			'inline_css' => array(
				'target' => array(
					'.mobile-toggle-nav a',
					'.scwd-mobile-toggle-menu-fixed_top .mobile-toggle-nav a',
				),
				'alter' => 'color',
			),
		),
		array(
			'id' => 'toggle_mobile_menu_links_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Links: Hover', 'scwd' ),
				'active_callback' => 'scwd_cac_mobile_menu_is_toggle',
			),
			'inline_css' => array(
				'target' => array(
					'.mobile-toggle-nav a:hover',
					'.scwd-mobile-toggle-menu-fixed_top .mobile-toggle-nav a:hover',
				),
				'alter' => 'color',
			),
		),
	),
);