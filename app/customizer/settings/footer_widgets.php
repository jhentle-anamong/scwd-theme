<?php
/**
 * Customizer => Footer Widgets
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// General
$this->sections['scwd_footer_general'] = array(
	'title'    => esc_html__( 'General', 'scwd' ),
	'panel'    => 'scwd_footer_widgets',
	'settings' => array(
		array(
			'id' => 'footer_widgets',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Footer Widgets', 'scwd' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'If you disable this option we recommend you go to the Customizer Manager and disable the section as well so the next time you work with the Customizer it will load faster.', 'scwd' ),
			),
		),
		array(
			'id' => 'fixed_footer',
			'default' => false,
			'control' => array(
				'label' => esc_html__( 'Fixed Footer', 'scwd' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'This setting will not "fix" your footer per-se but will add a min-height to your #main container to keep your footer always at the bottom of the page.', 'scwd' ),
				//'active_callback' => 'scwd_cac_has_footer_widgets', // Also affects footer bottom
			),
		),
		array(
			'id' => 'footer_reveal',
			'control' => array(
				'label' => esc_html__( 'Footer Reveal', 'scwd' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'Enable the footer reveal style. The footer will be placed in a fixed postion and display on scroll. This setting is for the "Full-Width" layout only and desktops only.', 'scwd' ),
				//'active_callback' => 'scwd_cac_supports_reveal',
			),
		),
		array(
			'id' => 'footer_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Padding', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
				'description' => $padding_desc,
			),
			'inline_css' => array(
				'target' => '#footer-inner',
				'alter' => 'padding',
			),
		),
		array(
			'id' => 'footer_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '#footer',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'footer_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => array(
					'#footer',
					'#footer p',
					'#footer li a:before',
					'#footer .widget-recent-posts-icons li .fa',
					'#footer strong'
				),
				'alter' => 'color',
			),
		),
		array(
			'id' => 'footer_borders',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Borders', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => array(
					'#footer li',
					'#footer #wp-calendar thead th',
					'#footer #wp-calendar tbody td',
					'#footer table th',
					'#footer table td'
				),
				'alter' => 'border-color',
			),
		),
		array(
			'id' => 'footer_link_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Links', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '#footer a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'footer_link_color_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Links: Hover', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '#footer a:hover',
				'alter' => 'color',
			),
		),
	),
);

$this->sections['scwd_footer_widgets_columns'] = array(
	'title'    => esc_html__( 'Widget Columns', 'scwd' ),
	'panel'    => 'scwd_footer_widgets',
	'settings' => array(
		array(
			'id' => 'footer_widgets_columns',
			'default' => '4',
			'control' => array(
				'label' => esc_html__( 'Columns', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'5' => '5',
					'4' => '4',
					'3' => '3',
					'2' => '2',
					'1' => '1',
				),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
		),
		array(
			'id' => 'footer_widgets_gap',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Gap', 'scwd' ),
				'type' => 'select',
				'choices' => scwd_column_gaps(),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
		),
		array(
			'id' => 'footer_widgets_bottom_margin',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Bottom Margin', 'scwd' ),
				'type' => 'text',
				'active_callback' => 'scwd_cac_has_footer_widgets',
				'description'     => esc_html__( 'The Bottom Margin is technically applied to each widget so you have space between widgets added in the same column. If you alter this value you should probably also change your general Footer top padding so the top/bottom spacing in your footer area match.', 'scwd' ),
			),
			'inline_css'   => array(
				'target'   => '.footer-widget',
				'alter'    => 'padding-bottom',
				'sanitize' => 'px-pct',
			),
		),
		array(
			'id' => 'footer_widgets_col_1_width',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Column 1 Width', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css'   => array(
				'target'   => '.footer-box.col-1',
				'alter'    => 'width',
				'sanitize' => 'px-pct',
			),
		),
		array(
			'id' => 'footer_widgets_col_2_width',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Column 2 Width', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css'   => array(
				'target'   => '.footer-box.col-2',
				'alter'    => 'width',
				'sanitize' => 'px-pct',
			),
		),
		array(
			'id' => 'footer_widgets_col_3_width',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Column 3 Width', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css'   => array(
				'target'   => '.footer-box.col-3',
				'alter'    => 'width',
				'sanitize' => 'px-pct',
			),
		),
		array(
			'id' => 'footer_widgets_col_4_width',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Column 4 Width', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css'   => array(
				'target'   => '.footer-box.col-4',
				'alter'    => 'width',
				'sanitize' => 'px-pct',
			),
		),
		array(
			'id' => 'footer_widgets_col_5_width',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Column 5 Width', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css'   => array(
				'target'   => '.footer-box.col-5',
				'alter'    => 'width',
				'sanitize' => 'px-pct',
			),
		),
	),
);

$this->sections['scwd_footer_widgets_titles'] = array(
	'title'    => esc_html__( 'Widget Titles', 'scwd' ),
	'panel'    => 'scwd_footer_widgets',
	'settings' => array(
		array(
			'id' => 'footer_headings',
			'transport' => 'postMessage',
			'default' => 'div',
			'control' => array(
				'label' => esc_html__( 'Tag', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'h2' => 'h2',
					'h3' => 'h3',
					'h4' => 'h4',
					'h5' => 'h5',
					'h6' => 'h6',
					'span' => 'span',
					'div' => 'div',
				),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
		),
		array(
			'id' => 'footer_headings_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '.footer-widget .widget-title',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'footer_headings_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '.footer-widget .widget-title',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'footer_headings_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Padding', 'scwd' ),
				'description' => $padding_desc,
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '.footer-widget .widget-title',
				'alter' => 'padding',
			),
		),
		array(
			'id' => 'footer_headings_align',
			'transport' => 'postMessage',
			'control' =>  array(
				'type' => 'select',
				'label' => esc_html__( 'Text Align', 'scwd' ),
				'choices' => array(
					'' => esc_html__( 'Default','scwd' ),
					'left' => esc_html__( 'Left','scwd' ),
					'right' => esc_html__( 'Right','scwd' ),
					'center' => esc_html__( 'Center','scwd' ),
				),
				'active_callback' => 'scwd_cac_has_footer_widgets',
			),
			'inline_css' => array(
				'target' => '.footer-widget .widget-title',
				'alter' => 'text-align',
			),
		),
	),
);