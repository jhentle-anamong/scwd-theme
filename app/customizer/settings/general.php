<?php
/**
 * Customizer => General Panel
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Accent Colors
// $this->sections['scwd_accent_colors'] = array(
// 	'title' => esc_html__( 'Accent Colors', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'accent_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Accent Color', 'scwd' ),
// 				'type' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'accent_color_hover',
// 			'transport' => 'partialRefresh',
// 			'control' => array(
// 				'label' => esc_html__( 'Accent Hover Color', 'scwd' ),
// 				'description' => esc_html__( 'Used for various hovers on accent colors such as buttons. If left empty it will inherit the custom accent color defined above', 'scwd' ),
// 				'type' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'main_border_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Border Accent Color', 'scwd' ),
// 				'type' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'highlight_bg',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Highlight Background', 'scwd' ),
// 				'type' => 'color',
// 			),
// 			'inline_css' => array(
// 				'target' => array( '::selection', '::-moz-selection' ),
// 				'alter' => 'background',
// 			),
// 		),
// 		array(
// 			'id' => 'highlight_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Highlight Color', 'scwd' ),
// 				'type' => 'color',
// 			),
// 			'inline_css' => array(
// 				'target' => array( '::selection', '::-moz-selection' ),
// 				'alter' => 'color',
// 			),
// 		),
// 	)
// );

// Background
// $this->sections['scwd_background'] = array(
// 	'title'  => esc_html__( 'Site Background', 'scwd' ),
// 	'panel'  => 'scwd_general',
// 	'desc' => esc_html__( 'Here you can alter the global site background. It is highly recommended that you first set the site layout to "Boxed" under the Layout options.', 'scwd' ),
// 	'settings' => array(
// 		array(
// 			'id' => 't_background_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Background Color', 'scwd' ),
// 				'type' => 'color',
// 			),
// 			'inline_css' => array(
// 				'target' => 'body,.footer-has-reveal #main,body.boxed-main-layout',
// 				'alter' => 'background-color',
// 			),
// 		),
// 		array(
// 			'id' => 't_background_image',
// 			'sanitize_callback' => 'absint',
// 			'control' => array(
// 				'label' => esc_html__( 'Custom Background Image', 'scwd' ),
// 				'type' => 'media',
// 				'mime_type' => 'image',
// 			),
// 		),
// 		array(
// 			'id' => 't_background_style',
// 			'default' => 'stretched',
// 			'control' => array(
// 				'label' => esc_html__( 'Background Image Style', 'scwd' ),
// 				'type'  => 'select',
// 				'choices' => $bg_styles,
// 			),
// 		),
// 		array(
// 			'id' => 't_background_pattern',
// 			'sanitize_callback' => 'esc_html',
// 			'control' => array(
// 				'label' => esc_html__( 'Background Pattern', 'scwd' ),
// 				'type'  => 'scwd-bg-patterns',
// 			),
// 		),
// 	),
// );

// Social Sharing Section
/*$social_share_items = wpex_get_social_items();

if ( $social_share_items ) {

	$social_share_choices = array();

	foreach ( $social_share_items as $k => $v ) {
		$social_share_choices[$k] = $v['site'];
	}

	$this->sections['scwd_social_sharing'] = array(
		'title'  => esc_html__( 'Social Sharing', 'scwd' ),
		'panel'  => 'scwd_general',
		'settings' => array(
			array(
				'id'  => 'social_share_sites',
				'transport' => 'partialRefresh',
				'default' => array( 'twitter', 'facebook', 'linkedin', 'email' ),
				'control' => array(
					'label'  => esc_html__( 'Sites', 'scwd' ),
					'desc' => esc_html__( 'Click and drag and drop elements to re-order them.', 'scwd' ),
					'type' => 'scwd-sortable',
					'object' => 'scwd_Customize_Control_Sorter',
					'choices' => $social_share_choices,
					'active_callback' => 'scwd_cac_hasnt_custom_social_share',
				),
			),
			array(
				'id' => 'social_share_position',
				'transport' => 'partialRefresh',
				'control' => array(
					'label' => esc_html__( 'Position', 'scwd' ),
					'type' => 'select',
					'choices' => array(
						'' => esc_html__( 'Default', 'scwd' ),
						'horizontal' => esc_html__( 'Horizontal', 'scwd' ),
						'vertical' => esc_html__( 'Vertical (Fixed)', 'scwd' ),
					),
					'active_callback' => 'scwd_cac_has_theme_social_share_sites',
				),
			),
			array(
				'id' => 'social_share_style',
				'transport' => 'partialRefresh',
				'default' => 'flat',
				'control' => array(
					'label' => esc_html__( 'Style', 'scwd' ),
					'type'  => 'select',
					'choices' => array(
						'flat' => esc_html__( 'Flat', 'scwd' ),
						'minimal' => esc_html__( 'Minimal', 'scwd' ),
						'three-d' => esc_html__( '3D', 'scwd' ),
						'rounded' => esc_html__( 'Rounded', 'scwd' ),
						'custom' => esc_html__( 'Custom', 'scwd' ),
					),
					'active_callback' => 'scwd_cac_has_theme_social_share_sites',
				),
			),
			array(
				'id' => 'social_share_label',
				'transport' => 'partialRefresh',
				'default' => true,
				'control' => array(
					'label' => esc_html__( 'Display Horizontal Style Label', 'scwd' ),
					'type' => 'checkbox',
					'active_callback' => 'scwd_cac_has_theme_social_share_sites',
				),
			),
			array(
				'id' => 'social_share_font_size',
				'description' => esc_html__( 'Value in px or em.', 'scwd' ),
				'transport' => 'postMessage',
				'control' => array(
					'type' => 'text',
					'label' => esc_html__( 'Custom Font Size', 'scwd' ),
					'active_callback' => 'scwd_cac_has_theme_social_share_sites',
				),
				'inline_css' => array(
					'target' => '.scwd-social-share li a',
					'alter' => 'font-size',
				),
			),
			array(
				'id' => 'social_share_heading',
				'transport' => 'partialRefresh',
				'default' => esc_html__( 'Share This', 'scwd' ),
				'control' => array(
					'label' => esc_html__( 'Horizontal Position Heading', 'scwd' ),
					'type'  => 'text',
					'active_callback' => 'scwd_cac_has_theme_social_share_sites',
					'description' => $leave_blank_desc,
				),
			),
			array(
				'id' => 'social_share_twitter_handle',
				'transport' => 'postMessage',
				'control' => array(
					'label' => esc_html__( 'Twitter Handle', 'scwd' ),
					'type' => 'text',
					'active_callback' => 'scwd_cac_has_theme_social_share_sites',
				),
			),
			array(
				'id' => 'social_share_shortcode',
				'transport' => 'partialRefresh',
				'control' => array(
					'label' => esc_html__( 'Alternative Shortcode', 'scwd' ),
					'type' => 'text',
					'description' => esc_html__( 'Override the theme default social share with your custom social sharing shortcode.', 'scwd' ),
				),
			),
		)
	);

}*/

// Breadcrumbs
// $this->sections['scwd_breadcrumbs'] = array(
// 	'title' => esc_html__( 'Breadcrumbs', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'breadcrumbs',
// 			'transport' => 'partialRefresh',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Enable', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_visibility',
// 			'transport' => 'postMessage',
// 			'default' => 'hidden-phone',
// 			'control' => array(
// 				'label' => esc_html__( 'Visibility', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => $choices_visibility,
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_position',
// 			'transport' => 'partialRefresh',
// 			'default' => 'absolute',
// 			'control' => array(
// 				'label' => esc_html__( 'Position', 'scwd' ),
// 				'type'  => 'select',
// 				'choices' => array(
// 					'absolute'  => esc_html__( 'Absolute Right', 'scwd' ),
// 					'under-title' => esc_html__( 'Under Title', 'scwd' ),
// 					'custom' => esc_html__( 'Custom', 'scwd' ),
// 				),
// 				'active_callback' => 'scwd_cac_has_breadcrumbs',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_home_title',
// 			'transport' => 'partialRefresh',
// 			'control' => array(
// 				'label' => esc_html__( 'Custom Home Title', 'scwd' ),
// 				'type'  => 'text',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_disable_taxonomies',
// 			'transport' => 'partialRefresh',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Remove categories and other taxonomies from breadcrumbs?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_first_cat_only',
// 			'transport' => 'partialRefresh',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'First Category Only', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 			'control_display' => array(
// 				'check' => 'breadcrumbs_disable_taxonomies',
// 				'value' => 'false',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_title_trim',
// 			'transport' => 'partialRefresh',
// 			'control' => array(
// 				'label' => esc_html__( 'Title Trim Length', 'scwd' ),
// 				'type'  => 'text',
// 				'desc'  => esc_html__( 'Enter the max number of words to display for your breadcrumbs post title', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_text_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Text Color', 'scwd' ),
// 				'active_callback' => 'scwd_cac_has_breadcrumbs',
// 			),
// 			'inline_css' => array(
// 				'target' => '.site-breadcrumbs',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_seperator_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Separator Color', 'scwd' ),
// 				'active_callback' => 'scwd_cac_has_breadcrumbs',
// 			),
// 			'inline_css' => array(
// 				'target' => '.site-breadcrumbs .sep',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_link_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Link Color', 'scwd' ),
// 				'active_callback' => 'scwd_cac_has_breadcrumbs',
// 			),
// 			'inline_css' => array(
// 				'target' => '.site-breadcrumbs a',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'breadcrumbs_link_color_hover',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Link Color: Hover', 'scwd' ),
// 				'active_callback' => 'scwd_cac_has_breadcrumbs',
// 			),
// 			'inline_css' => array(
// 				'target' => '.site-breadcrumbs a:hover',
// 				'alter' => 'color',
// 			),
// 		),
// 	),
// );

// Page Title
$this->sections['scwd_page_header'] = array(
	'title' => esc_html__( 'Page Header Title', 'scwd' ),
	'panel' => 'scwd_general',
	'settings' => array(
		array(
			'id' => 'page_header_style',
			//'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Style', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'' => esc_html__( 'Default','scwd' ),
					'centered' => esc_html__( 'Centered', 'scwd' ),
					'centered-minimal' => esc_html__( 'Centered Minimal', 'scwd' ),
					'hidden' => esc_html__( 'Hidden', 'scwd' ), // @todo rename to "Disabled" and update docs
				),
			),
		),
		array(
			'id' => 'page_header_hidden_main_top_padding',
			'transport' => 'postMessage',
			'control_display' => array(
				'check' => 'page_header_style',
				'value' => 'hidden',
			),
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Hidden Page Header Title Spacing', 'scwd' ),
				'desc' => esc_html__( 'When the page header title is set to hidden there will not be any space between the header and the main content. If you want to add a default space between the header and your main content you can enter the px value here.', 'scwd' ) .'<br /><br />'. $pixel_desc,
			),
			'inline_css' => array(
				'target' => 'body.page-header-disabled #content-wrap',
				'alter' => 'padding-top',
				'sanitize' => 'px',
			),
		),
		array(
			'id' => 'page_header_top_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Top Padding', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods',
				'alter' => 'padding-top',
			),
		),
		array(
			'id' => 'page_header_bottom_padding',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Bottom Padding', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods',
				'alter' => 'padding-bottom',
			),
		),
		array(
			'id' => 'page_header_bottom_margin',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Bottom Margin', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '.page-header',
				'alter' => 'margin-bottom',
			),
		),
		array(
			'id' => 'page_header_background',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'page_header_title_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Text Color', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods .page-header-title',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'page_header_top_border',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Top Border Color', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods',
				'alter' => 'border-top-color',
			),
		),
		array(
			'id' => 'page_header_bottom_border',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Bottom Border Color', 'scwd' ),
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods',
				'alter' => 'border-bottom-color',
			),
		),
		array(
			'id' => 'page_header_border_width',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Border Width', 'scwd' ),
				'description' => $pixel_desc,
			),
			'inline_css' => array(
				'target' => '.page-header.scwd-supports-mods',
				'alter' => array( 'border-width' ),
			),
		),
		array(
			'id' => 'page_header_background_img',
			'transport' => 'refresh',
			'control' => array(
				'type' => 'media',
				'mime_type' => 'image',
				'label' => esc_html__( 'Background Image', 'scwd' ),
			),
		),
		array(
			'id' => 'page_header_background_img_style',
			'transport' => 'postMessage',
			'default' => 'fixed',
			'control' => array(
				'label' => esc_html__( 'Background Image Style', 'scwd' ),
				'type'  => 'select',
				'active_callback' => 'scwd_cac_has_page_header_title_background',
				'choices' => $bg_styles,
			),
		),
		array(
			'id' => 'page_header_background_fetch_thumbnail',
			'control' => array(
				'type' => 'multiple-select',
				'label' => esc_html__( 'Fetch Background From Featured Image', 'scwd' ),
				'description' => esc_html__( 'Check the box next to any post type where you want to display the featured image as the page header title background.', 'scwd' ),
				'active_callback' => 'scwd_cac_has_page_header_title_background',
				'choices' => $post_types,
			),
		),
	),
);

// Pages
// $blocks = apply_filters( 'scwd_page_single_blocks', array(
// 	'title'    => esc_html__( 'Title', 'scwd' ),
// 	'media'    => esc_html__( 'Media', 'scwd' ),
// 	'content'  => esc_html__( 'Content', 'scwd' ),
// 	'share'    => esc_html__( 'Social Share', 'scwd' ),
// 	'comments' => esc_html__( 'Comments', 'scwd' ),
// ), 'customizer' );

// $this->sections['scwd_pages'] = array(
// 	'title'  => esc_html__( 'Pages', 'scwd' ),
// 	'panel'  => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'page_single_layout',
// 			'control' => array(
// 				'label' => esc_html__( 'Layout', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => $post_layouts,
// 			),
// 		),
// 		array(
// 			'id' => 'pages_custom_sidebar',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Custom Sidebar', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'page_singular_template',
// 			'default' => '',
// 			'control' => array(
// 				'label' => esc_html__( 'Dynamic Template (Advanced)', 'scwd' ),
// 				'type' => 'scwd-dropdown-templates',
// 				'desc' => $template_desc,
// 			),
// 		),
// 		array(
// 			'id' => 'page_composer',
// 			'default' => 'content',
// 			'control' => array(
// 				'label' => esc_html__( 'Post Layout Elements', 'scwd' ),
// 				'type' => 'scwd-sortable',
// 				'choices' => $blocks,
// 				'desc' => esc_html__( 'Click and drag and drop elements to re-order them.', 'scwd' ),
// 				'active_callback' => 'scwd_cac_page_single_hasnt_custom_template',
// 			),
// 		),
// 	),
// );

// Search
// $this->sections['scwd_search'] = array(
// 	'title'  => esc_html__( 'Search', 'scwd' ),
// 	'panel'  => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'search_standard_posts_only',
// 			'default' => false,
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Show Standard Posts Only', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'search_style',
// 			'default' => 'default',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Style', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => array(
// 					'default' => esc_html__( 'Left Thumbnail', 'scwd' ),
// 					'blog' => esc_html__( 'Inherit From Blog','scwd' ),
// 				),
// 			),
// 		),
// 		array(
// 			'id' => 'search_layout',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Layout', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => $post_layouts,
// 			),
// 		),
// 		array(
// 			'id' => 'search_posts_per_page',
// 			'default' => '10',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Posts Per Page', 'scwd' ),
// 				'type' => 'text',
// 			),
// 		),
// 		array(
// 			'id' => 'search_custom_sidebar',
// 			'default' => true,
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'label' => esc_html__( 'Custom Sidebar', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 	),
// );

// Scroll to top
$this->sections['scwd_scroll_top'] = array(
	'title' => esc_html__( 'Scroll To Top', 'scwd' ),
	'panel' => 'scwd_general',
	'settings' => array(
		array(
			'id' => 'scroll_top',
			'default' => true,
			'transport' => 'refresh',
			'control' => array(
				'label' => esc_html__( 'Scroll Up Button', 'scwd' ),
				'type' => 'checkbox',
			),
		),
		array(
			'id' => 'scroll_top_arrow',
			'default' => 'chevron-up',
			'transport' => 'postMessage',
			'control' => array(
				'label' => esc_html__( 'Arrow', 'scwd' ),
				'type' => 'select',
				'choices' => array(
					'chevron-up' => 'chevron-up',
					'caret-up' => 'caret-up',
					'angle-up' => 'angle-up',
					'angle-double-up' => 'angle-double-up',
					'long-arrow-up' => 'long-arrow-up',
					'arrow-circle-o-up' => 'arrow-circle-o-up',
					'arrow-up' => 'arrow-up',
					'caret-square-o-up' => 'caret-square-o-up',
					'level-up' => 'level-up',
					'sort-up' => 'sort-up',
					'toggle-up' => 'toggle-up',
				),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
		),
		array(
			'id' => 'scroll_top_size',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Button Size', 'scwd' ),
				'description' => $pixel_desc,
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'sanitize' => 'px',
				'alter' => array(
					'width',
					'height',
					'line-height',
				),
			),
		),
		array(
			'id' => 'scroll_top_icon_size',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Icon Size', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'alter' => 'font-size',
			),
		),
		array(
			'id' => 'scroll_top_border_radius',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Border Radius', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'alter' => 'border-radius',
			),
		),
		array(
			'id' => 'scroll_top_right_position',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Right Position', 'scwd' ),
				'description' => esc_html__( 'Default:', 'scwd' ) .' 40px',
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'alter' => 'right',
			),
		),
		array(
			'id' => 'scroll_top_bottom_position',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'text',
				'label' => esc_html__( 'Bottom Position', 'scwd' ),
				'description' => esc_html__( 'Default:', 'scwd' ) .' 40px',
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'alter' => 'bottom',
			),
		),
		array(
			'id' => 'scroll_top_color',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'scroll_top_color_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Color: Hover', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top:hover',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'scroll_top_bg',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top',
				'alter' => 'background-color',
			),
		),
		array(
			'id' => 'scroll_top_bg_hover',
			'transport' => 'postMessage',
			'control' => array(
				'type' => 'color',
				'label' => esc_html__( 'Background: Hover', 'scwd' ),
			),
			'control_display' => array(
				'check' => 'scroll_top',
				'value' => 'true',
			),
			'inline_css' => array(
				'target' => '#site-scroll-top:hover',
				'alter' => 'background-color',
			),
		),
	),
);

// Pagination
// $this->sections['scwd_pagination'] = array(
// 	'title' => esc_html__( 'Pagination', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'pagination_align',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'select',
// 				'default' => 'left',
// 				'label' => esc_html__( 'Align', 'scwd' ),
// 				'choices' => array(
// 					'' => esc_html__( 'Default', 'scwd' ),
// 					'left' => esc_html__( 'Left', 'scwd' ),
// 					'center' => esc_html__( 'Center', 'scwd' ),
// 					'right' => esc_html__( 'Right', 'scwd' ),
// 				),
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_arrow',
// 			'transport' => 'postMessage',
// 			'default' => 'angle',
// 			'control' => array(
// 				'type' => 'select',
// 				'label' => esc_html__( 'Arrow Style', 'scwd' ),
// 				'choices' => array(
// 					'angle' => esc_html__( 'Angle', 'scwd' ),
// 					'arrow' => esc_html__( 'Arrow', 'scwd' ),
// 					'caret' => esc_html__( 'Caret', 'scwd' ),
// 					'chevron' => esc_html__( 'Chevron', 'scwd' ),
// 				),
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_padding',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Padding', 'scwd' ),
// 				'description' => $padding_desc,
// 			),
// 			'inline_css' => array(
// 				'target' => 'ul.page-numbers a,span.page-numbers,.page-links span,.page-links a > span',
// 				'alter' => 'padding',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_font_size',
// 			'description' => esc_html__( 'Value in px or em.', 'scwd' ),
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Font Size', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'ul.page-numbers, .page-links',
// 				'alter' => 'font-size',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_border_width',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Border Width', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'ul.page-numbers, .page-links, ul.page-numbers li, .page-links li',
// 				'alter' => 'border-width',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_border_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Border Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'ul.page-numbers, .page-links, ul.page-numbers li, .page-links li',
// 				'alter' => 'border-color',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'ul .page-numbers a, a.page-numbers, span.page-numbers, .page-links span, .page-links a > span',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_hover_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Color: Hover', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.page-numbers a:hover, .page-numbers.current, .page-numbers.current:hover, .page-links span, .page-links a > span:hover, .woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_hover_active',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Color: Active', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.page-numbers.current, .page-numbers.current:hover',
// 				'alter' => 'color',
// 				'important' => true,
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_bg',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Background', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'ul.page-numbers a,span.page-numbers,.page-links span,.page-links a>span,.bbp-pagination-links span.page-numbers, .bbp-pagination-links .page-numbers',
// 				'alter' => 'background',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_hover_bg',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Background: Hover', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.page-numbers a:hover,.page-numbers.current,.page-numbers.current:hover,.page-links span,.page-links a > span:hover, .woocommerce nav.woocommerce-pagination ul li a:focus,.woocommerce nav.woocommerce-pagination ul li a:hover',
// 				'alter' => 'background',
// 			),
// 		),
// 		array(
// 			'id' => 'pagination_active_bg',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Background: Active', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.page-numbers.current, .page-numbers.current:hover',
// 				'alter' => 'background',
// 				'important' => true,
// 			),
// 		),
// 		array(
// 			'id' => 'loadmore_text',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Load More Text', 'scwd' ),
// 			),
// 		),
// 	),
// );

// Next/Prev
// $this->sections['scwd_next_prev'] = array(
// 	'title' => esc_html__( 'Next & Previous Links', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'next_prev_in_same_term',
// 			'default' => true,
// 			'control' => array(
// 				'type' => 'checkbox',
// 				'label' => esc_html__( 'From Same Category', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_reverse_order',
// 			'default' => false,
// 			'control' => array(
// 				'type' => 'checkbox',
// 				'label' => esc_html__( 'Reverse Order', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_link_bg_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Background', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.post-pagination-wrap',
// 				'alter' => 'background-color',
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_link_border_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Border Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.post-pagination-wrap',
// 				'alter' => 'border-color',
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_link_padding',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Padding', 'scwd' ),
// 				'description' => $padding_desc,
// 			),
// 			'inline_css' => array(
// 				'target' => '.post-pagination-wrap',
// 				'alter' => 'padding',
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_link_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Link Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.post-pagination a',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_link_font_size',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Font Size', 'scwd' ),
// 				'description' => esc_html__( 'Value in px or em.', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.post-pagination',
// 				'alter' => 'font-size',
// 				'sanitize' => 'font-size',
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_next_text',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Custom Next Text', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'next_prev_prev_text',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Custom Prev Text', 'scwd' ),
// 			),
// 		),
// 	),
// );

// Theme Heading
// $this->sections['scwd_theme_heading'] = array(
// 	'title' => esc_html__( 'Theme Heading', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'desc' => esc_html__( 'Heading used in various places such as the related and comments heading.', 'scwd' ),
// 	'settings' => array(
// 		array(
// 			'id' => 'theme_heading_style',
// 			'control' => array(
// 				'type' => 'select',
// 				'default' => '',
// 				'label' => esc_html__( 'Style', 'scwd' ),
// 				'choices' => array(
// 					'' => esc_html__( 'Default', 'scwd' ),
// 					'plain' => esc_html__( 'Plain', 'scwd' ),
// 					'border-w-color' => esc_html__( 'Bottom Border With Color', 'scwd' ),
// 				),
// 			),
// 		),
// 		array(
// 			'id' => 'theme_heading_tag',
// 			'default' => 'div',
// 			'control' => array(
// 				'label' => esc_html__( 'Tag', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => array(
// 					'div' => 'div',
// 					'h2' => 'h2',
// 					'h3' => 'h3',
// 					'h4' => 'h4',
// 					'h5' => 'h5',
// 					'h6' => 'h6',
// 				),
// 			),
// 		),
// 	),
// );

$inputs_target = array('.site-content input[type="date"],.site-content input[type="time"],.site-content input[type="datetime-local"],.site-content input[type="week"],.site-content input[type="month"],.site-content input[type="text"],.site-content input[type="email"],.site-content input[type="url"],.site-content input[type="password"],.site-content input[type="search"],.site-content input[type="tel"],.site-content input[type="number"],.site-content textarea' );
$inputs_target_focus = array('.site-content input[type="date"]:focus,.site-content input[type="time"]:focus,.site-content input[type="datetime-local"],.site-content input[type="week"],.site-content input[type="month"]:focus,.site-content input[type="text"]:focus,.site-content input[type="email"]:focus,.site-content input[type="url"]:focus,.site-content input[type="password"]:focus,.site-content input[type="search"]:focus,.site-content input[type="tel"]:focus,.site-content input[type="number"]:focus,.site-content textarea:focus' );

// Forms
// $this->sections['scwd_general_forms'] = array(
// 	'title' => esc_html__( 'Forms', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'label_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Label Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'label,#comments #commentform label',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'forms_inputs_heading',
// 			'control' => array(
// 				'type' => 'scwd-heading',
// 				'label' => esc_html__( 'Inputs', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'input_padding',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Padding', 'scwd' ),
// 				'description' => $padding_desc,
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => 'padding',
// 			),
// 		),
// 		array(
// 			'id' => 'input_border_radius',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Border Radius', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => array( 'border-radius', '-webkit-border-radius' ),
// 			),
// 		),
// 		array(
// 			'id' => 'input_font_size',
// 			'description' => esc_html__( 'Value in px or em.', 'scwd' ),
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Font-Size', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => 'font-size',
// 			),
// 		),
// 		array(
// 			'id' => 'input_background',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Background', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => 'background-color',
// 			),
// 		),
// 		array(
// 			'id' => 'input_background_focus',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Focus: Background', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target_focus,
// 				'alter' => 'background-color',
// 			),
// 		),
// 		array(
// 			'id' => 'input_border',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Border Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => 'border-color',
// 			),
// 		),
// 		array(
// 			'id' => 'input_border_focus',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Focus: Border Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target_focus,
// 				'alter' => 'border-color',
// 			),
// 		),
// 		array(
// 			'id' => 'input_border_width',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Border Width', 'scwd' ),
// 				'description' => $pixel_desc,
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => 'border-width',
// 			),
// 		),
// 		array(
// 			'id' => 'input_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target,
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'input_color_focus',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Focus: Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => $inputs_target_focus,
// 				'alter' => 'color',
// 			),
// 		),
// 	),
// );

// Tables
// $this->sections['scwd_general_tables'] = array(
// 	'title' => esc_html__( 'Tables', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'thead_background',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Table Header Background', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'thead',
// 				'alter' => 'background',
// 			),
// 		),
// 		array(
// 			'id' => 'thead_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Table Header Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'table thead, table thead th',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'tables_th_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Th Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'table th',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'tables_border_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Cells Border Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'table th, table td',
// 				'alter' => 'border-color',
// 			),
// 		),
// 		array(
// 			'id' => 'tables_cell_padding',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Cell Padding', 'scwd' ),
// 				'description' => $padding_desc,
// 			),
// 			'inline_css' => array(
// 				'target' => 'table th, table td',
// 				'alter' => 'padding',
// 			),
// 		),
// 	),
// );


// Links & Buttons
// $this->sections['scwd_general_links_buttons'] = array(
// 	'title' => esc_html__( 'Links & Buttons', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'link_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Links Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .entry-title a:hover,.meta a:hover',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'link_color_hover',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Links Color: Hover', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => 'a:hover',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'theme_button_padding',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Theme Button Padding', 'scwd' ),
// 				'description' => $padding_desc,
// 			),
// 			'inline_css' => array(
// 				'target' => '.theme-button,input[type="submit"],button,.button,.added_to_cart',
// 				'alter' => 'padding',
// 			),
// 		),
// 		array(
// 			'id' => 'theme_button_border_radius',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'text',
// 				'label' => esc_html__( 'Theme Button Border Radius', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.theme-button,input[type="submit"],button,#site-navigation .menu-button > a > span.link-inner,.button,.added_to_cart',
// 				'alter' => 'border-radius',
// 			),
// 		),
// 		array(
// 			'id' => 'theme_button_bg',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Theme Button Background', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.theme-button,input[type="submit"],button,#site-navigation .menu-button > a > span.link-inner,.button,.added_to_cart',
// 				'alter' => 'background-color',
// 			),
// 		),
// 		array(
// 			'id' => 'theme_button_hover_bg',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Theme Button Background: Hover', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.theme-button:hover,input[type="submit"]:hover,button:hover,#site-navigation .menu-button > a:hover > span.link-inner,.button:hover,.added_to_cart:hover',
// 				'alter' => 'background-color',
// 			),
// 		),
// 		array(
// 			'id' => 'theme_button_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Theme Button Color', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.theme-button,input[type="submit"],button,#site-navigation .menu-button > a > span.link-inner,.button,.added_to_cart',
// 				'alter' => 'color',
// 			),
// 		),
// 		array(
// 			'id' => 'theme_button_hover_color',
// 			'transport' => 'postMessage',
// 			'control' => array(
// 				'type' => 'color',
// 				'label' => esc_html__( 'Theme Button Color: Hover', 'scwd' ),
// 			),
// 			'inline_css' => array(
// 				'target' => '.theme-button:hover,input[type="submit"]:hover,button:hover,#site-navigation .menu-button > a:hover > span.link-inner,.button:hover,.added_to_cart:hover',
// 				'alter' => 'color',
// 			),
// 		),
// 	),
// );

// Post Sliders
// $this->sections['scwd_post_slider'] = array(
// 	'title'  => esc_html__( 'Post Gallery Slider', 'scwd' ),
// 	'panel'  => 'scwd_general',
// 	'settings' => array(
// 		array(
// 			'id' => 'post_slider_animation',
// 			'default' => 'slide',
// 			'control' => array(
// 				'label' => esc_html__( 'Animation', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => array(
// 					'slide' => esc_html__( 'Slide', 'scwd' ),
// 					'fade' => esc_html__( 'Fade','scwd' ),
// 				),
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_animation_speed',
// 			'default' => '600',
// 			'control' => array(
// 				'label' => esc_html__( 'Custom Animation Speed', 'scwd' ),
// 				'type' => 'textfield',
// 				'description' => esc_html__( 'Enter a value in milliseconds.', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_autoplay',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Auto Play?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_loop',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Loop?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_thumbnails',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Thumbnails?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_arrows',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Arrows?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_arrows_on_hover',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Arrows on Hover?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'post_slider_dots',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Dots?', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 	),
// );

// Lightbox
// $this->sections['scwd_lightbox'] = array(
// 	'title' => esc_html__( 'Lightbox', 'scwd' ),
// 	'panel' => 'scwd_general',
// 	'settings' => array(
// 		/*array(
// 			'id' => 'lightbox_width',
// 			'default' => 1500,
// 			'control' => array(
// 				'label' => esc_html__( 'Image Max Width', 'scwd' ),
// 				'type' => 'number',
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_titles',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Titles', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		*/
// 		array(
// 			'id' => 'lightbox_load_style_globally',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Load Scripts Globally', 'scwd' ),
// 				'type' => 'checkbox',
// 				'desc' => esc_html__( 'By default the lightbox scripts will only load as needed by the theme. You can enable this option to load the scripts globally on the whole site if needed or you can use the [scwd_lightbox_scripts] shortcode anywhere to load the scripts as needed.', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_auto',
// 			'control' => array(
// 				'label' => esc_html__( 'Auto Lightbox', 'scwd' ),
// 				'type' => 'checkbox',
// 				'desc' => esc_html__( 'Automatically add Lightbox to images inserted into the post editor.', 'scwd' ),
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_transition_effect',
// 			'default' => 'fade',
// 			'control' => array(
// 				'label' => esc_html__( 'Transition Effect', 'scwd' ),
// 				'type' => 'select',
// 				'choices' => array(
// 					'fade' => esc_html__( 'Fade', 'scwd' ),
// 					'slide' => esc_html__( 'Slide', 'scwd' ),
// 					'circular' => esc_html__( 'Circular', 'scwd' ),
// 					'tube' => esc_html__( 'Tube', 'scwd' ),
// 					'zoom-in-out' => esc_html__( 'Zoom-In-Out', 'scwd' ),
// 				),
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_transition_duration',
// 			'default' => 366,
// 			'control' => array(
// 				'label' => esc_html__( 'Duration in ms for transition animation', 'scwd' ),
// 				'type' => 'text',
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_thumbnails',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Enable Gallery Thumbnails Panel', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_thumbnails_auto_start',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Auto Open Gallery Thumbnails Panel', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_loop',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Gallery Loop', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_arrows',
// 			'default' => true,
// 			'control' => array(
// 				'label' => esc_html__( 'Gallery Arrows', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 		array(
// 			'id' => 'lightbox_fullscreen',
// 			'default' => false,
// 			'control' => array(
// 				'label' => esc_html__( 'Fullscreen Button', 'scwd' ),
// 				'type' => 'checkbox',
// 			),
// 		),
// 	),
// );