<?php
/**
 * Customizer => Footer Bottom
 *
 * @package SCWD WordPress Theme
 * @subpackage Customizer
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// General
$this->sections['scwd_footer_bottom'] = array(
	'title' => esc_html__( 'General', 'scwd' ),
	'settings' => array(
		array(
			'id' => 'footer_bottom',
			'default' => true,
			'control' => array(
				'label' => esc_html__( 'Bottom Footer Area', 'scwd' ),
				'type' => 'checkbox',
				'desc' => esc_html__( 'If you disable this option we recommend you go to the Customizer Manager and disable the section as well so the next time you work with the Customizer it will load faster.', 'scwd' ),
			),
		),
		array(
			'id' => 'footer_copyright_text',
			'transport' => 'partialRefresh',
			'default' => 'Copyright <a href="#">Your Business LLC.</a> [current_year] - All Rights Reserved',
			'control' => array(
				'label' => esc_html__( 'Copyright', 'scwd' ),
				'type' => 'textarea',
				'active_callback' => 'scwd_cac_has_footer_bottom',
			),
		),
		array(
			'id' => 'bottom_footer_text_align',
			'transport' => 'partialRefresh',
			'control' =>  array(
				'type' => 'select',
				'label' => esc_html__( 'Text Align', 'scwd' ),
				'choices' => array(
					'' => esc_html__( 'Default','scwd' ),
					'left' => esc_html__( 'Left','scwd' ),
					'right' => esc_html__( 'Right','scwd' ),
					'center' => esc_html__( 'Center','scwd' ),
				),
				'active_callback'=> 'scwd_cac_has_footer_bottom',
			),
		),
		array(
			'id' => 'bottom_footer_padding',
			'transport' => 'postMessage',
			'control' =>  array(
				'type' => 'text',
				'label' => esc_html__( 'Padding', 'scwd' ),
				'description' => $padding_desc,
				'active_callback'=> 'scwd_cac_has_footer_bottom',
			),
			'inline_css' => array(
				'target' => '#footer-bottom-inner',
				'alter' => 'padding',
			),
		),
		array(
			'id' => 'bottom_footer_background',
			'transport' => 'postMessage',
			'control' =>  array(
				'type' => 'color',
				'label' => esc_html__( 'Background', 'scwd' ),
				'active_callback'=> 'scwd_cac_has_footer_bottom',
			),
			'inline_css' => array(
				'target' => '#footer-bottom',
				'alter' => 'background',
			),
		),
		array(
			'id' => 'bottom_footer_color',
			'transport' => 'postMessage',
			'control' =>  array(
				'type' => 'color',
				'label' => esc_html__( 'Color', 'scwd' ),
				'active_callback'=> 'scwd_cac_has_footer_bottom',
			),
			'inline_css' => array(
				'target' => array(
					'#footer-bottom',
					'#footer-bottom p',
				),
				'alter' => 'color',
			),
		),
		array(
			'id' => 'bottom_footer_link_color',
			'transport' => 'postMessage',
			'control' =>  array(
				'type' => 'color',
				'label' => esc_html__( 'Links', 'scwd' ),
				'active_callback'=> 'scwd_cac_has_footer_bottom',
			),
			'inline_css' => array(
				'target' => '#footer-bottom a',
				'alter' => 'color',
			),
		),
		array(
			'id' => 'bottom_footer_link_color_hover',
			'transport' => 'postMessage',
			'control' =>  array(
				'type' => 'color',
				'label' => esc_html__( 'Links: Hover', 'scwd' ),
				'active_callback'=> 'scwd_cac_has_footer_bottom',
			),
			'inline_css' => array(
				'target' => '#footer-bottom a:hover',
				'alter' => 'color',
			),
		),
	),
);