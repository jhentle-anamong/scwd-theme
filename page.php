<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SCWD WordPress Theme
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
?>

	<div id="content-wrap" class="container clr">

		<?php scwd_hook_primary_before(); ?>

		<div id="primary" class="content-area clr">

			<?php scwd_hook_content_before(); ?>

			<div id="content" class="site-content">

				<?php scwd_hook_content_top(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php scwd_get_template_part( 'page_single_blocks' ); ?>

				<?php endwhile; ?>

				<?php scwd_hook_content_bottom(); ?>

			</div><!-- #content -->

			<?php scwd_hook_content_after(); ?>

		</div><!-- #primary -->

		<?php scwd_hook_primary_after(); ?>

	</div><!-- #content-wrap -->

<?php get_footer(); ?>
