<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package SCWD WordPress Theme
 * @subpackage Templates
 * @version 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?><?php scwd_schema_markup( 'html' ); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php
	if ( function_exists( 'wp_body_open' ) ) {
		wp_body_open();
	} else {
		do_action( 'wp_body_open' );
	} ?>

	<?php scwd_outer_wrap_before(); ?>

	<div id="outer-wrap" class="clr">

		<?php scwd_hook_wrap_before(); ?>

		<div id="wrap" class="clr">

			<?php scwd_hook_wrap_top(); ?>

			<?php scwd_hook_main_before(); ?>

			<main id="main" class="site-main clr"<?php scwd_schema_markup( 'main' ); ?><?php scwd_aria_landmark( 'main' ); ?>>
				<?php scwd_hook_main_top(); ?>